package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class APIcall {
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSES FOR API CALLS.//");
		
		String messageAPIwelcomemail = "https://registry.jcpenney.com/skavamarketing/marketing/customservice/jcpenney/message?campaignId=732&listName=Registry&type=0&channel=mobile";

		String Response  = requestResponse(messageAPIwelcomemail);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("responseMessage:"+responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("responseCode:"+responseCode);
		
	}
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
