package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class getnotification {
	public static void main(String[] args) throws JSONException{
		
		System.out.println("//RESPONSES FOR GETNOTIFICATION.//");
		
		String getnotification  = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/getNotifications?offset=0&limit=10&campaignId=732&value={\"value\":null}&value={\"value\":\"99005458\"}&t=1471967332329";
		String Response  = requestResponse(getnotification);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("isowner")){
			System.out.println("isowner is present");
		}
		else
		{
			System.out.println("isowner not present");
		}
		
		if(jsonObj.has("results")){
			System.out.println("results is present");
		}
		else
		{
			System.out.println("results not present");
		}
		
		if(jsonObj.has("resultsCount")){
			System.out.println("resultsCount is present and resultsCount is:"+jsonObj.getString("resultsCount"));
		}
		else
		{
			System.out.println("resultsCount not present");
		}
		
		JSONArray resultsArr = jsonObj.getJSONArray("results");
		for(int i =0; i < resultsArr.length();i++)
		{
			JSONObject resultsObj = resultsArr.getJSONObject(i);
			
			
			if(resultsObj.has("campaignid")){
				System.out.println("campaignid is present for Index : " + i +"and campaignid is:"+resultsObj.getString("campaignid"));
			}
			else
			{
				System.out.println("campaignid not present for index : " + i);
			}
			
			if(resultsObj.has("device")){
				System.out.println("device is present for Index : " + i +"and device is:"+resultsObj.getString("device"));
			}
			else
			{
				System.out.println("device not present for index : " + i);
			}
			
			if(resultsObj.has("eventDate")){
				System.out.println("eventDate is present for Index : " + i +"and eventDate is:"+resultsObj.getString("eventDate"));
			}
			else
			{
				System.out.println("eventDate not present for index : " + i);
			}
			
			if(resultsObj.has("eventName")){
				System.out.println("eventName is present for Index : " + i +"and eventName is:"+resultsObj.getString("eventName"));
			}
			else
			{
				System.out.println("eventName not present for index : " + i);
			}
			
			if(resultsObj.has("eventType")){
				System.out.println("eventType is present for Index : " + i +"and eventType is:"+resultsObj.getString("eventType"));
			}
			else
			{
				System.out.println("eventType not present for index : " + i);
			}
			
			if(resultsObj.has("location")){
				System.out.println("location is present for Index : " + i +"and location is:"+resultsObj.getString("location"));
			}
			else
			{
				System.out.println("location not present for index : " + i);
			}
			
			if(resultsObj.has("subject")){
				System.out.println("subject is present for Index : " + i +"and subject is:"+resultsObj.getString("subject"));
			}
			else
			{
				System.out.println("subject not present for index : " + i);
			}
			
			if(resultsObj.has("type")){
				System.out.println("type is present for Index : " + i +"and type is:"+resultsObj.getString("type"));
			}
			else
			{
				System.out.println("type not present for index : " + i);
			}
		}
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
