package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class updatelistchecklist {
	public static void main(String[] args) throws Exception{
		System.out.println("//RESPONSES FOR UPDATELIST CHECKLIST.//");
		
		String URL  = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/updateList/Registry?ispublic=true&properties={\"zipCode\":\"435345\",\"country\":\"US\",\"JCPProfileId\":\"L118826265471\",\"property_eventDate\":\"1477852200000\",\"firstname\":\"ttert\",\"extn\":\"\",\"property_lastname\":\"et\",\"acls\":1,\"city\":\"Syracuse\",\"campaignid\":732,\"property_coregfullname\":\"ert ertsdfsdf\",\"coregfname\":\"ert\",\"userid\":62587132,\"isdeleted\":false,\"property_regType\":\"wedding\",\"property_regfullname\":\"ttert et\",\"profilePhoto\":\"https://d1on4naqzauyk9.cloudfront.net/uploadedImages/sc_5961471965077284_244042.jpg?t=1471965190480\",\"regfullname\":\"ttert et\",\"property_location\":\"\",\"ispublic\":true,\"coreglname\":\"ertsdfsdf\",\"listguid\":\"17282d14-1510-4003-b2f3-3e98c4a84532\",\"state\":\"NY\",\"tag\":\"\",\"id\":\"99005458\",\"regType\":\"wedding\",\"property_coreglname\":\"ertsdfsdf\",\"apt\":\"\",\"isEmailOptIn\":\"yes\",\"property_listname\":\"dfgdfg\",\"listname\":\"dfgdfg\",\"sort_eventDate_732\":\"1477852200000\",\"eventType\":\"W\",\"msgToGuest\":\"dfgdfg\",\"createdtime\":1471965085000,\"lastname\":\"et\",\"coregfullname\":\"ert ertsdfsdf\",\"property_firstname\":\"ttert\",\"streetAddress\":\"Erie Boulevard East\",\"phone\":\"345-345-3453\",\"property_coregfname\":\"ert\",\"name\":\"Registry\",\"location\":\"\",\"eventDate_data\":\"10-31-2016\",\"eventDate\":\"1477852200000\",\"checklist\":\"dinner plates (12)\"}&campaignId=732";
		String postURL = URL.replaceAll("\\&.*","");
		String postParam = URL.replaceAll(".*\\&","");
		String Response  = sendPost(postURL,postParam);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("ResponseMessage:"+responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("ResponseCode:"+responseCode);
		
	
}

static String requestResponse(String url)
{
	try
	{
		URL Url = new URL(url);
		URLConnection conn = Url.openConnection();
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		String line = "";
		StringBuffer strBuff = new StringBuffer();
		while((line = br.readLine()) != null)
		{
			strBuff.append(line);
		}

		return strBuff.toString();
	}
	catch(Throwable tr)
	{

	}

	return "";


}

private static String sendPost(String url,String urlParameters) throws Exception {

	//String url = "https://selfsolve.apple.com/wcResults.do";
	URL obj = new URL(url);
	HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
	//System.out.println(url +" and Param : " + urlParameters);

	//add reuqest header
	con.setRequestMethod("POST");
	//con.setRequestProperty("User-Agent", USER_AGENT);
	con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

	

	// Send post request
	con.setDoOutput(true);
	DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	wr.writeBytes(urlParameters);
	wr.flush();
	wr.close();

	//int responseCode = con.getResponseCode();
	/*System.out.println("\nSending 'POST' request to URL : " + url);
	System.out.println("Post parameters : " + urlParameters);
	System.out.println("Response Code : " + responseCode);*/

	BufferedReader in = new BufferedReader(
	        new InputStreamReader(con.getInputStream()));
	String inputLine;
	StringBuffer response = new StringBuffer();

	while ((inputLine = in.readLine()) != null) {
		response.append(inputLine);
	}
	in.close();

	//print result
	//System.out.println(response.toString());
	return response.toString();

}

}
