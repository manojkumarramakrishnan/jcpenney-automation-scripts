package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class geteventfullfillment {
	public static void main(String[] args) throws JSONException{
		
		System.out.println("//RESPONSES FOR GET EVENT FULLFILLMENT.//");
		String getevent  = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/getEvent?value={\"value\":\"99005458\"}&type=fulfillment&offset=0&limit=10&campaignId=732";
		String Response  = requestResponse(getevent);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("isowner")){
			System.out.println("isowner is present");
		}
		else
		{
			System.out.println("isowner not present");
		}
		
		if(jsonObj.has("results")){
			System.out.println("results is present");
		}
		else
		{
			System.out.println("results not present");
		}
		
		if(jsonObj.has("resultsCount")){
			System.out.println("resultsCount is present and resultsCount is:"+jsonObj.getString("resultsCount"));
		}
		else
		{
			System.out.println("resultsCount not present");
		}
		
     JSONArray resultsArr = jsonObj.getJSONArray("results");
		
		for(int i =0; i < resultsArr.length();i++)
		{
			JSONObject resultsObj = resultsArr.getJSONObject(i);
			
			
			if(resultsObj.has("campaignid")){
				System.out.println("campaignid is present for Index : " + i +"and campaignid is:"+resultsObj.getString("campaignid"));
			}
			else
			{
				System.out.println("campaignid not present for index : " + i);
			}
			
			if(resultsObj.has("createdtime")){
				System.out.println("createdtime is present for Index : " + i +"and createdtime is:"+resultsObj.getString("createdtime"));
			}
			else
			{
				System.out.println("createdtime not present for index : " + i);
			}
			
			if(resultsObj.has("device")){
				System.out.println("device is present for Index : " + i +"and device is:"+resultsObj.getString("device"));
			}
			else
			{
				System.out.println("device not present for index : " + i);
			}
			
			if(resultsObj.has("gifterEmail")){
				System.out.println("gifterEmail is present for Index : " + i +"and gifterEmail is:"+resultsObj.getString("gifterEmail"));
			}
			else
			{
				System.out.println("gifterEmail not present for index : " + i);
			}
			
			if(resultsObj.has("gifterName")){
				System.out.println("gifterName is present for Index : " + i +"and gifterName is:"+resultsObj.getString("gifterName"));
			}
			else
			{
				System.out.println("gifterName not present for index : " + i);
			}
			
			if(resultsObj.has("image")){
				System.out.println("imageurl is present for Index : " + i +"and imageurl is:"+resultsObj.getString("image"));
			}
			else
			{
				System.out.println("image not present for index : " + i);
			}
			
			if(resultsObj.has("location")){
				System.out.println("location is present for Index : " + i +"and location is:"+resultsObj.getString("location"));
			}
			else
			{
				System.out.println("location not present for index : " + i);
			}
			
			if(resultsObj.has("productName")){
				System.out.println("productName is present for Index : " + i +"and productName is:"+resultsObj.getString("productName"));
			}
			else
			{
				System.out.println("productName not present for index : " + i);
			}
			
			if(resultsObj.has("qty")){
				System.out.println("qty is present for Index : " + i +"and qty is:"+resultsObj.getString("qty"));
			}
			else
			{
				System.out.println("qty not present for index : " + i);
			}
			
			if(resultsObj.has("type")){
				System.out.println("type is present for Index : " + i +"and type is:"+resultsObj.getString("type"));
			}
			else
			{
				System.out.println("type not present for index : " + i);
			}
			
			if(resultsObj.has("sku")){
				System.out.println("sku is present for Index : " + i +"and sku is:"+resultsObj.getString("sku"));
			}
			else
			{
				System.out.println("sku not present for index : " + i);
			}
		}
		
	}
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
