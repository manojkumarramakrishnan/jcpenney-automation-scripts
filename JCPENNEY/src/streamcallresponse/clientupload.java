package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class clientupload {
	
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSE FOR CLIENTUPLOAD//");
		
		String clientupload = "http://registry.jcpenney.com/skavastream/upload/v5/jcpenney/clientUpload?redirectUrl=http://assets.jcpenney.atfingertips.com.s3.amazonaws.com/uploadedImages/sc_5951471963721526_545226.jpg&bucket=assets.jcpenney.atfingertips.com&key=uploadedImages/sc_5951471963721526_545226.jpg&etag=\"4687e6095e7e10de64753b1548dfccc6\"";
		
		String Response  = requestResponse(clientupload);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
