package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class productcall {
	public static void main(String[] args) throws JSONException
	{
		
		System.out.println("//RESPONSES FOR PRODUCTCALL.//");
		String Productcall = "https://registry.jcpenney.com/skavastream/core/v5/jcpenney/product/pp5005030323?page=1&channel=mobile&campaignId=732";
		
		String Response  = requestResponse(Productcall);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("image")){
			System.out.println("imageurl is present and imageurl is:"+jsonObj.getString("image"));
		}
		else
		{
			System.out.println("imageurl not present");
		}
		
		if(jsonObj.has("name")){
			System.out.println("name is present and name is:"+jsonObj.getString("name"));
		}
		else
		{
			System.out.println("name not present ");
		}
		
		if(jsonObj.has("type")){
			System.out.println("type is present and type is:"+jsonObj.getString("type"));
		}
		else
		{
			System.out.println("type not present");
		}
		
		if(jsonObj.has("properties")){
			System.out.println("properties is present." );
		}
		else
		{
			System.out.println("properties not present.");
		}
		
		JSONObject propertiesObj = jsonObj.getJSONObject("properties");
		
		if(propertiesObj.has("buyinfo")){
			System.out.println("buyinfo is present." );
		}
		else
		{
			System.out.println("buyinfo not present.");
		}
		
		if(propertiesObj.has("iteminfo")){
			System.out.println("iteminfo is present." );
		}
		else
		{
			System.out.println("iteminfo not present.");
		}
		if(propertiesObj.has("state")){
			System.out.println("state is present." );
		}
		else
		{
			System.out.println("state not present.");
		}
		
		
		if(propertiesObj.has("skuprops")){
			System.out.println("skuprops is present." );
		}
		else
		{
			System.out.println("skuprops not present.");
		}
		
		JSONObject stateObj = propertiesObj.getJSONObject("state");
		if(stateObj.has("catalogid")){
			System.out.println("catalogid is present and catalogid is:"+ stateObj.getString("catalogid"));
		}
		else
		{
			System.out.println("catalogid not present.");
		}
		JSONObject buyinfoObj = propertiesObj.getJSONObject("buyinfo");
		if(buyinfoObj.has("pricing")){
			System.out.println("pricing is present." );
		}
		else
		{
			System.out.println("pricing not present.");
		}
		
		JSONObject pricingObj = buyinfoObj.getJSONObject("pricing");
		
		if(pricingObj.has("prices")){
			System.out.println("prices is present." );
		}
		else
		{
			System.out.println("prices not present.");
		}
		
		JSONArray priceArr = pricingObj.getJSONArray("prices");
		for(int i =0; i < priceArr.length();i++)
		{
			JSONObject pricesObj = priceArr.getJSONObject(i);
			
			
			if(pricesObj.has("label")){
				System.out.println("label is present for Index : " + i + "and label is:"+pricesObj.getString("label"));
			}
			else
			{
				System.out.println("label not present for index : " + i);
			}
			
			if(pricesObj.has("type")){
				System.out.println("type is present for Index : " + i + "and type is:"+pricesObj.getString("type"));
			}
			else
			{
				System.out.println("type not present for index : " + i);
			}
			
			if(pricesObj.has("value")){
				System.out.println("value is present for Index : " + i + "and value is:"+pricesObj.getString("value"));
			}
			else
			{
				System.out.println("value not present for index : " + i);
			}
		}
		
		
		JSONObject skupropsObj = propertiesObj.getJSONObject("skuprops");
		if(skupropsObj.has("color")){
			System.out.println("color is present." );
		}
		else
		{
			System.out.println("color not present.");
		}
		
		JSONArray colorArr = skupropsObj.getJSONArray("color");
		for(int i =0; i < colorArr.length();i++)
		{
			JSONObject colorObj = colorArr.getJSONObject(i);
			
			
			if(colorObj.has("identifier")){
				System.out.println("identifier is present for Index : " + i +"and identifier is:"+colorObj.getString("identifier"));
			}
			else
			{
				System.out.println("identifier not present for index : " + i);
			}
			
			if(colorObj.has("name")){
				System.out.println("name is present for Index : " + i +"and name is:"+colorObj.getString("name"));
			}
			else
			{
				System.out.println("name not present for index : " + i);
			}
			
			if(colorObj.has("swatchimage")){
				System.out.println("swatchimageurl is present for Index : " + i +"and swatchimageurl is:"+colorObj.getString("swatchimage"));
			}
			else
			{
				System.out.println("swatchimageurl not present for index : " + i);
			}
		}
	}
	
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
