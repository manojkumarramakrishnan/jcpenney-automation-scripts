package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

public class getuploadprops {
	
	public static void main(String[] args) throws Exception {
		System.out.println("//RESPONSE FOR GETUPLOADPROPS//");
		
		String getuploadprops = "https://registry.jcpenney.com/skavastream/upload/v5/jcpenney/getUploadProps?campaignId=732&filename=SC_595.jpg&redirectUrl=//registry.jcpenney.com/skavastream/studio/reader/stg/JCP_Registry/createreg?returnUrl=//m.jcpenney.com/giftregistry&domainName=registry.jcpenney.com&foldername=uploadedImages";
		
		String Response  = requestResponse(getuploadprops);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("key")){
			System.out.println("key is present and key is:"+jsonObj.getString("key"));
		}
		else
		{
			System.out.println("key not present");
		}
		
		if(jsonObj.has("action")){
			System.out.println("action is present and action is:"+jsonObj.getString("action"));
		}
		else
		{
			System.out.println("action not present");
		}
		
		
		
		if(jsonObj.has("contentType")){
			System.out.println("contentType is present and contentType is:"+jsonObj.has("contentType"));
		}
		else
		{
			System.out.println("contentType not present");
		}
		
		if(jsonObj.has("successActionRedirect")){
			System.out.println("successActionRedirect is present and successActionRedirect is:"+jsonObj.has("successActionRedirect"));
		}
		else
		{
			System.out.println("successActionRedirect not present");
		}
	
	}

	
	
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	
	/*private static String sendPost(String url,String urlParameters) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}
*/
}
