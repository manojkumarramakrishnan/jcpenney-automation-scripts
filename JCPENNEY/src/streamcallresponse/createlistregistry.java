package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class createlistregistry {
	public static void main(String[] args) throws Exception{
		System.out.println("//RESPONSE FOR CREATELIST REGISTRY.//");
		
		String URL = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/createList/Registry?campaignId=732&ispublic=true&properties={\"firstname\":\"ttert\",\"lastname\":\"et\",\"regfullname\":\"ttert et\",\"coregfname\":\"ert\",\"coreglname\":\"ert\",\"coregfullname\":\"ert ert\",\"profilePhoto\":\"https://d1on4naqzauyk9.cloudfront.net/uploadedImages/sc_5961471965077284_244042.jpg?t=1471965190480\",\"regType\":\"wedding\",\"location\":\"\",\"eventDate\":\"1477852200000\",\"eventDate_data\":\"10-31-2016\",\"eventType\":\"W\",\"streetAddress\":\"Erie Boulevard East\",\"apt\":\"\",\"city\":\"Syracuse\",\"zipCode\":\"435345\",\"state\":\"NY\",\"country\":\"US\",\"phone\":\"345-345-3453\",\"extn\":\"\",\"msgToGuest\":\"dfgdfg\",\"listname\":\"dfgdfg\",\"ispublic\":true,\"isEmailOptIn\":\"yes\",\"JCPProfileId\":\"L118826265471\"}";
		//String Response  = requestResponse(createlistRegistry);
		String postURL = URL.replaceAll("\\&.*","");
		String postParam = URL.replaceAll(".*\\&","");
		String Response  = sendPost(postURL,postParam);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println(responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println(responseCode);
		
		
	}
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	
	private static String sendPost(String url,String urlParameters) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		/*System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);*/

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}

}
