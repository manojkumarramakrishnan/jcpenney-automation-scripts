package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class addtobag {
	public static void main(String[] args) throws Exception{
		System.out.println("//RESPONSES FOR ADD TO BAG.//");
		
		String URL = "https://registry.jcpenney.com/skavastream/xact/v5/jcpenney/addtobag?channel=mobile&campaignId=732&item={\"skuId\":\"78061070018\",\"itemid\":\"pp5005140282\",\"quantity\":1,\"giftDetails\":{\"id\":\"\"}}";
		
		/*String postURL = URL.replaceAll("\\&.*","");
		String postParam = URL.replaceAll(".*\\&","");*/
		String Response  = sendPost(URL);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		/*String responseMessage = jsonObj.getString("responseMessage");
		System.out.println(responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println(responseCode);*/
	}

	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	
	private static String sendPost(String url) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		//wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		/*System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);*/

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}
	
}


