package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class searchlist {

	public static void main(String[] args) throws Exception{
		
		System.out.println("//RESPONSES FOR SEARCH LIST.//");
		String URL  = "https://registry.jcpenney.com/skavasearch/search/v5/jcpenney/searchLists?campaignId=732&offset=0&limit=5&query={%22conditions%22:%20[{%22and%22:%22true%22,%20%22fieldName%22:%22campaignid%22,%22value%22:%22732%22},{%22and%22:%22true%22,%22fieldName%22:%22id%22,%22value%22:99005541},{%22and%22:%22true%22,%22fieldName%22:%22property_regType%22,%22value%22:%22Wedding%22}]}";
		/*String Response  = requestResponse(searchlist);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);*/
		/*
		String postURL = URL.replaceAll("\\&.*","");
		String postParam = URL.replaceAll(".*\\&","");*/
		String Response  = sendPost(URL);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		if(jsonObj.has("isowner")){
			System.out.println("isowner is present and isowner is:"+jsonObj.getString("isowner"));
		}
		else
		{
			System.out.println("isowner not present");
		}
		
		if(jsonObj.has("results")){
			System.out.println("results is present.");
		}
		else
		{
			System.out.println("results not present");
		}
		
		if(jsonObj.has("resultsCount")){
			System.out.println("resultsCount is present and resultsCount is:"+jsonObj.getString("resultsCount"));
		}
		else
		{
			System.out.println("resultsCount not present");
		}
JSONArray resultsArr = jsonObj.getJSONArray("results");
		
		for(int i =0; i < resultsArr.length();i++)
		{
			JSONObject childObj = resultsArr.getJSONObject(i);
			
			
			if(childObj.has("campaignid")){
				System.out.println("campaignid is present for Index : " + i+" and campaignid is : " + childObj.getString("campaignid"));
			}
			else
			{
				System.out.println("campaignid not present for index : " + i);
			}
			
			if(childObj.has("checklist")){
				System.out.println("checklist is present for Index : " + i+" and checklist is : " + childObj.getString("checklist"));
			}
			else
			{
				System.out.println("checklist not present for index : " + i);
			}
			
			if(childObj.has("city")){
				System.out.println("city is present for Index : " + i+" and city is : " + childObj.getString("city"));
			}
			else
			{
				System.out.println("city not present for index : " + i);
			}
			
			if(childObj.has("coregfname")){
				System.out.println("coregfname is present for Index : " + i+" and coregfname is : " + childObj.getString("coregfname"));
			}
			else
			{
				System.out.println("coregfname not present for index : " + i);
			}
			
			if(childObj.has("coregfullname")){
				System.out.println("coregfullname is present for Index : " + i+" and coregfullname is : " + childObj.getString("coregfullname"));
			}
			else
			{
				System.out.println("coregfullname not present for index : " + i);
			}
			
			if(childObj.has("country")){
				System.out.println("country is present for Index : " + i+" and country is : " + childObj.getString("country"));
			}
			else
			{
				System.out.println("country not present for index : " + i);
			}
			
			if(childObj.has("eventDate_data")){
				System.out.println("eventDate_data is present for Index : " + i+" and eventDate_data is : " + childObj.getString("eventDate_data"));
			}
			else
			{
				System.out.println("eventDate_data not present for index : " + i);
			}
			
			if(childObj.has("eventType")){
				System.out.println("eventType is present for Index : " + i+" and eventType is : " + childObj.getString("eventType"));
			}
			else
			{
				System.out.println("eventType not present for index : " + i);
			}
		
			
			if(childObj.has("firstname")){
				System.out.println("firstname is present for Index : " + i+" and firstname is : " + childObj.getString("firstname"));
			}
			else
			{
				System.out.println("firstname not present for index : " + i);
			}
			
			if(childObj.has("id")){
				System.out.println("id is present for Index : " + i+" and id is : " + childObj.getString("id"));
			}
			else
			{
				System.out.println("id not present for index : " + i);
			}
			
			if(childObj.has("isdeleted")){
				System.out.println("isdeleted is present for Index : " + i+" and isdeleted is : " + childObj.getString("isdeleted"));
			}
			else
			{
				System.out.println("isdeleted not present for index : " + i);
			}
			
			if(childObj.has("isEmailOptIn")){
				System.out.println("isEmailOptIn is present for Index : " + i+" and isEmailOptIn is : " + childObj.getString("isEmailOptIn"));
			}
			else
			{
				System.out.println("isEmailOptIn not present for index : " + i);
			}
			
			if(childObj.has("ispublic")){
				System.out.println("ispublic is present for Index : " + i+" and ispublic is : " + childObj.getString("ispublic"));
			}
			else
			{
				System.out.println("ispublic not present for index : " + i);
			}
			
			if(childObj.has("JCPProfileId")){
				System.out.println("JCPProfileId is present for Index : " + i+" and JCPProfileId is : " + childObj.getString("JCPProfileId"));
			}
			else
			{
				System.out.println("JCPProfileId not present for index : " + i);
			}
			
			if(childObj.has("lastname")){
				System.out.println("lastname is present for Index : " + i+" and lastname is : " + childObj.getString("lastname"));
			}
			else
			{
				System.out.println("lastname not present for index : " + i);
			}
			
			if(childObj.has("listname")){
				System.out.println("listname is present for Index : " + i+" and listname is : " + childObj.getString("listname"));
			}
			else
			{
				System.out.println("listname not present for index : " + i);
			}
			
			if(childObj.has("msgToGuest")){
				System.out.println("msgToGuest is present for Index : " + i+" and msgToGuest is : " + childObj.getString("msgToGuest"));
			}
			else
			{
				System.out.println("msgToGuest not present for index : " + i);
			}
			
			if(childObj.has("name")){
				System.out.println("name is present for Index : " + i+" and name is : " + childObj.getString("name"));
			}
			else
			{
				System.out.println("name not present for index : " + i);
			}
			
			if(childObj.has("phone")){
				System.out.println("phone is present for Index : " + i+" and phone is : " + childObj.getString("phone"));
			}
			else
			{
				System.out.println("phone not present for index : " + i);
			}
			
			if(childObj.has("profilePhoto")){
				System.out.println("profilePhoto url is present for Index : " + i+" and profilePhoto url is : " + childObj.getString("profilePhoto"));
			}
			else
			{
				System.out.println("profilePhoto url not present for index : " + i);
			}
			
			if(childObj.has("property_coregfname")){
				System.out.println("property_coregfname is present for Index : " + i+" and property_coregfname is : " + childObj.getString("property_coregfname"));
			}
			else
			{
				System.out.println("property_coregfname not present for index : " + i);
			}
			
			if(childObj.has("property_coregfullname")){
				System.out.println("property_coregfullname is present for Index : " + i+" and property_coregfullname is : " + childObj.getString("property_coregfullname"));
			}
			else
			{
				System.out.println("property_coregfullname not present for index : " + i);
			}
			
			if(childObj.has("property_firstname")){
				System.out.println("property_firstname is present for Index : " + i+" and property_firstname is : " + childObj.getString("property_firstname"));
			}
			else
			{
				System.out.println("property_firstname not present for index : " + i);
			}
			
			if(childObj.has("property_lastname")){
				System.out.println("property_lastname is present for Index : " + i+" and property_lastname is : " + childObj.getString("property_lastname"));
			}
			else
			{
				System.out.println("property_lastname not present for index : " + i);
			}
			
			if(childObj.has("property_listname")){
				System.out.println("property_listname is present for Index : " + i+" and property_listname is : " + childObj.getString("property_listname"));
			}
			else
			{
				System.out.println("property_listname not present for index : " + i);
			}
			
			if(childObj.has("property_regfullname")){
				System.out.println("property_regfullname is present for Index : " + i+" and property_regfullname is : " + childObj.getString("property_regfullname"));
			}
			else
			{
				System.out.println("property_regfullname not present for index : " + i);
			}
			
			if(childObj.has("property_regType")){
				System.out.println("property_regType is present for Index : " + i+" and property_regType is : " + childObj.getString("property_regType"));
			}
			else
			{
				System.out.println("property_regType not present for index : " + i);
			}
			
			if(childObj.has("regfullname")){
				System.out.println("regfullname is present for Index : " + i+" and regfullname is : " + childObj.getString("regfullname"));
			}
			else
			{
				System.out.println("regfullname not present for index : " + i);
			}
			
			if(childObj.has("regType")){
				System.out.println("regType is present for Index : " + i+" and regType is : " + childObj.getString("regType"));
			}
			else
			{
				System.out.println("regType not present for index : " + i);
			}
			
			if(childObj.has("state")){
				System.out.println("state is present for Index : " + i+" and state is : " + childObj.getString("state"));
			}
			else
			{
				System.out.println("state not present for index : " + i);
			}
			
			if(childObj.has("streetAddress")){
				System.out.println("streetAddress is present for Index : " + i+" and streetAddress is : " + childObj.getString("streetAddress"));
			}
			else
			{
				System.out.println("streetAddress not present for index : " + i);
			}
			
			if(childObj.has("userid")){
				System.out.println("userid is present for Index : " + i+" and userid is : " + childObj.getString("userid"));
			}
			else
			{
				System.out.println("userid not present for index : " + i);
			}
			
			if(childObj.has("")){
				System.out.println("zipCode is present for Index : " + i+" and zipCode is : " + childObj.getString("zipCode"));
			}
			else
			{
				System.out.println("zipCode not present for index : " + i);
			}
			
			
		}
	}
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	
	private static String sendPost(String url) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		//wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		/*System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);*/

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}
	}

