package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class addeventfullfillment {
	public static void main(String[] args) throws JSONException{
		
		System.out.println("//RESPONSES FOR ADD EVENT FULLFILLMENT.//");
		
		String addevent  = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/addEvent?properties={\"sku\":\"79703710018\",\"qty\":0,\"msgSent\":1471967765424}&type=fulfillment&channel=&location=&campaignId=732&value=99005458&value=79703710018";
		String Response  = requestResponse(addevent);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("responseMessage:"+responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("responseCode:"+responseCode);
		
	}
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
