package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class getlistregistry {
	public static void main(String[] args) throws JSONException{
		
		System.out.println("//RESPONSES FOR GETLIST REGISTRY.//");
		String getlistitemsregistry = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/getListItems/99005458?offset=0&limit=200&campaignId=732";
		
		String Response  = requestResponse(getlistitemsregistry);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("isowner")){
			System.out.println("isowner is present");
		}
		else
		{
			System.out.println("isowner not present");
		}
		
		if(jsonObj.has("resultsCount")){
			System.out.println("resultcount is present and resultcount is:"+jsonObj.getString("resultsCount"));
		}
		else
		{
			System.out.println("resultcount not present");
		}
		JSONArray resultArr = jsonObj.getJSONArray("results");
		for(int i =0; i < resultArr.length();i++)
		{
			JSONObject childObj = resultArr.getJSONObject(i);
			
			
			if(childObj.has("campaignid")){
				System.out.println("campaignid is present for Index : " + i + "and campaignid is:"+childObj.getString("campaignid"));
			}
			else
			{
				System.out.println("campaignid not present for index : " + i);
			}
			
			if(childObj.has("facet_brand_732")){
				System.out.println("facet_brand_732 is present for Index : " + i + "and facet_brand_732 is:"+childObj.getString("facet_brand_732"));
			}
			else
			{
				System.out.println("facet_brand_732 not present for index : " + i);
			}
			
			if(childObj.has("facet_category_732")){
				System.out.println("facet_category_732 is present for Index : " + i + "and facet_category_732 is:"+childObj.getString("facet_category_732"));
			}
			else
			{
				System.out.println("facet_category_732 not present for index : " + i);
			}
			
			if(childObj.has("facet_color_732")){
				System.out.println("facet_color_732 is present for Index : " + i + "and facet_color_732 is:"+childObj.getString("facet_color_732"));
			}
			else
			{
				System.out.println("facet_color_732 not present for index : " + i);
			}
			
			if(childObj.has("facet_fulfilledqty_732")){
				System.out.println("facet_fulfilledqty_732 is present for Index : " + i + "and facet_fulfilledqty_732 is:"+childObj.getString("facet_fulfilledqty_732"));
			}
			else
			{
				System.out.println("facet_fulfilledqty_732 not present for index : " + i);
			}
			
			if(childObj.has("facet_fulfilled_732")){
				System.out.println("facet_fulfilled_732 is present for Index : " + i + "and facet_fulfilled_732 is:"+childObj.getString("facet_fulfilled_732"));
			}
			else
			{
				System.out.println("facet_fulfilled_732 not present for index : " + i);
			}
			
			if(childObj.has("facet_price_732")){
				System.out.println("facet_price_732 is present for Index : " + i + "and facet_price_732 is:"+childObj.getString("facet_price_732"));
			}
			else
			{
				System.out.println("facet_price_732 not present for index : " + i);
			}
			
			if(childObj.has("img")){
				System.out.println("imgurl is present for Index : " + i + "and imgurl is:"+childObj.getString("img"));
			}
			else
			{
				System.out.println("img not present for index : " + i);
			}
			
			if(childObj.has("inStore")){
				System.out.println("inStore is present for Index : " + i + "and inStore is:"+childObj.getString("inStore"));
			}
			else
			{
				System.out.println("inStore not present for index : " + i);
			}
			
			if(childObj.has("name")){
				System.out.println("name is present for Index : " + i + "and name is:"+childObj.getString("name"));
			}
			else
			{
				System.out.println("name not present for index : " + i);
			}
			
			if(childObj.has("profileId")){
				System.out.println("profileId is present for Index : " + i + "and profileId is:"+childObj.getString("profileId"));
			}
			else
			{
				System.out.println("profileId not present for index : " + i);
			}
			
			if(childObj.has("regPrice")){
				System.out.println("regPrice is present for Index : " + i + "and regPrice is:"+childObj.getString("regPrice"));
			}
			else
			{
				System.out.println("regPrice not present for index : " + i);
			}
			
			if(childObj.has("salePrice")){
				System.out.println("salePrice is present for Index : " + i + "and salePrice is:"+childObj.getString("salePrice"));
			}
			else
			{
				System.out.println("salePrice not present for index : " + i);
			}
			
			if(childObj.has("skuid")){
				System.out.println("skuid is present for Index : " + i + "and skuid is:"+childObj.getString("skuid"));
			}
			else
			{
				System.out.println("skuid not present for index : " + i);
			}
			
			if(childObj.has("sortstr_brand_732")){
				System.out.println("sortstr_brand_732 is present for Index : " + i + "and sortstr_brand_732 is:"+childObj.getString("sortstr_brand_732"));
			}
			else
			{
				System.out.println("sortstr_brand_732 not present for index : " + i);
			}
			
			if(childObj.has("sortstr_category_732")){
				System.out.println("sortstr_category_732 is present for Index : " + i + "and sortstr_category_732 is:"+childObj.getString("sortstr_category_732"));
			}
			else
			{
				System.out.println("sortstr_category_732 not present for index : " + i);
			}
			
			if(childObj.has("sortstr_fulfilled_732")){
				System.out.println("sortstr_fulfilled_732 is present for Index : " + i + "and sortstr_fulfilled_732 is:"+childObj.getString("sortstr_fulfilled_732"));
			}
			else
			{
				System.out.println("sortstr_fulfilled_732 not present for index : " + i);
			}
			
			if(childObj.has("sort_fulfilledqty_732")){
				System.out.println("sort_fulfilledqty_732 is present for Index : " + i + "and sort_fulfilledqty_732 is:"+childObj.getString("sort_fulfilledqty_732"));
			}
			else
			{
				System.out.println("sort_fulfilledqty_732 not present for index : " + i);
			}
			
			if(childObj.has("sort_price_732")){
				System.out.println("sort_price_732 is present for Index : " + i + "and sort_price_732 is:"+childObj.getString("sort_price_732"));
			}
			else
			{
				System.out.println("sort_price_732 not present for index : " + i);
			}
			
			if(childObj.has("wantedqty")){
				System.out.println("wantedqty is present for Index : " + i + "and wantedqty is:"+childObj.getString("wantedqty"));
			}
			else
			{
				System.out.println("wantedqty not present for index : " + i);
			}
			
			
			
		}
		
	}

	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}


	
}
