package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

public class addnotificationevent {

	public static void main(String[] args) throws Exception{
		System.out.println("//RESPONSES FOR ADD NOTIFICATION.//");
		
		String URL  = "https://registry.jcpenney.com/skavalist/list/v2/jcpenney/addNotification?campaignId=732&properties={%22callerid%22:%221%22,%22subject%22:%22Find%20the%20dress%20Notification%22,%22body%22:%22dfgdfg%22,%22fromname%22:%22ttert%22,%22eventDate%22:1472581800000,%22eventName%22:%22%22,%22eventType%22:%22Find%20the%20dress%22,%22eventTime%22:%2212:0:AM%22}&channel=a&doAddHostName=false&location=,Erie%20Boulevard%20East,Syracuse-435345&value=08312016&value=99005458&value=Find%20the%20dress";
		
		 /*String postURL = URL.replaceAll("\\&.*","");
			String postParam = URL.replaceAll(".*\\&","");*/
			String Response  = sendPost(URL);

			System.out.println(Response);
			JSONObject jsonObj = new JSONObject(Response);
			
			/*String responseMessage = jsonObj.getString("responseMessage");
			System.out.println("ResponseMessage:"+responseMessage);*/
			String responseCode = jsonObj.getString("responseCode");
			System.out.println("ResponseCode:"+responseCode);
			
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	
	private static String sendPost(String url) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		//wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		/*System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);*/

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}
}
