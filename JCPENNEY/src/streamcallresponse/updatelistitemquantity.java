package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

public class updatelistitemquantity {
	public static void main(String[] args) throws Exception{
		
		System.out.println("//RESPONSES FOR UPDATELIST ITEM QUANTITY.//");
		String URL  = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/updateListItem/Registry?item={%22name%22:%22Nike%C2%AE%20Brasilia%20Gym%20Sack%22,%22skuid%22:%2257690050117%22,%22properties%22:{%22identifier%22:%22pp5005730141%22,%22img%22:%22http://s7d9.scene7.com/is/image/JCPenney/DP0505201617042136M%22,%22color%22:%22Omega%20Blue%22,%22regPrice%22:%2214.00%22,%22salePrice%22:%22%22,%22itemquantity%22:2},%22facets%22:{%22price%22:%22%22,%22color%22:%22Omega%20Blue%22,%22category%22:%22Miscellaneous%22},%22sort%22:{%22price%22:%22%22}}&campaignId=732";
		 String postURL = URL.replaceAll("\\&.*","");
			String postParam = URL.replaceAll(".*\\&","");
			String Response  = sendPost(postURL,postParam);

			System.out.println(Response);
			JSONObject jsonObj = new JSONObject(Response);
			
			String responseMessage = jsonObj.getString("responseMessage");
			System.out.println("ResponseMessage:"+responseMessage);
			String responseCode = jsonObj.getString("responseCode");
			System.out.println("ResponseCode:"+responseCode);
			
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	
	private static String sendPost(String url,String urlParameters) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		/*System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);*/

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}

}
