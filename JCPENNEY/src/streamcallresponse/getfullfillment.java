package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class getfullfillment {
	public static void main(String[] args) throws JSONException{
		
		System.out.println("//RESPONSES FOR GET FULLFILLMENT.//");
		String getfulfillment  = "https://registry.jcpenney.com/skavalist/list/v2/skava/createFulfillment?campaignId=732&listId=99005458&itemId=79703710018&properties={%27qty%27:1,%27productName%27:%27Signature%20Design%20by%20Ashley%C2%AE...%20-%20Black%20-%20$11000%27,%27image%27:%27//s7d9.scene7.com/is/image/JCPenney/DP0502201617035565M.TIF?wid=180%27,%27gifterName%27:%27meenakshi%27,%27gifterEmail%27:%27meenakshi@skava.com%27}&type=fulfillment&channel=a&location=a";
		String Response  = requestResponse(getfulfillment);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("responseMessage:"+responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("responseCode:"+responseCode);
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
