package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class firstresponse {
	
	//String response  = prev.getResponseDataAsString();
	public static void main(String[] args) throws Exception {
		System.out.println("//RESPONSE FOR GETLIST//");
		
		//String URL = "https://registry.jcpenney.com/skavauser/user/v5/jcpenney/createOrGetUser?campaignId=732&userdesc={\"createIfNecessary\":true,\"useFP\":false,\"identities\":[{\"type\":1,\"value\":\"jcp\",\"properties\":{\"user_profile_id\":\"L118826265471\"}}]}";
		String URL = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/getLists?offset=0&limit=20&getProductInfo=true&campaignId=732";
		
		
		/*String postURL = URL.replaceAll("\\&.*","");
		String postParam = URL.replaceAll(".*\\&","");
		String Response  = sendPost(postURL,postParam);*/
		
		String Response  = requestResponse(URL);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("isowner")){
			System.out.println("isowner is present");
		}
		else
		{
			System.out.println("isowner not present");
		}
		
		JSONArray resultArr = jsonObj.getJSONArray("results");
		
		for(int i =0; i < resultArr.length();i++)
		{
			JSONObject childObj = resultArr.getJSONObject(i);
			
			
			if(childObj.has("zipCode")){
				System.out.println("zipCode is present for Index : " + i+" and ZipCode is : " + childObj.getString("zipCode"));
			}
			else
			{
				System.out.println("zipCode not present for index : " + i);
			}
			
			if(childObj.has("country")){
				System.out.println("country is present for Index : " + i + "and country is :"+childObj.getString("country"));
			}
			else
			{
				System.out.println("country not present for index : " + i);
			}
			
			if(childObj.has("city")){
				System.out.println("city is present for Index : " + i+"and city is :"+childObj.getString("city"));
			}
			else
			{
				System.out.println("city not present for index : " + i);
			}
			
			if(childObj.has("campaignid")){
				System.out.println("campaignid is present for Index : " + i+ "and campaignid is:"+childObj.getString("campaignid"));
			}
			else
			{
				System.out.println("campaignid not present for index : " + i);
			}
			
			if(childObj.has("msgToGuest")){
				System.out.println("msgToGuest is present for Index : " + i+ "and msgToGuest is:"+childObj.getString("msgToGuest"));
			}
			else
			{
				System.out.println("msgToGuest not present for index : " + i);
			}
			
			if(childObj.has("firstname")){
				System.out.println("firstname is present for Index : " + i+ "and firstname is:"+childObj.getString("firstname"));
			}
			else
			{
				System.out.println("firstname not present for index : " + i);
			}
			
			if(childObj.has("lastname")){
				System.out.println("lastname is present for Index : " + i+ "and lastname is:"+childObj.getString("lastname"));
			}
			else
			{
				System.out.println("lastname is not present for index : " + i);
			}
			
			if(childObj.has("regType")){
				System.out.println("regType is present for Index : " + i+ "and regType is:"+childObj.getString("regType"));
			}
			else
			{
				System.out.println("regType is not present for index : " + i);
			}
			
			if(childObj.has("streetAddress")){
				System.out.println("streetAddress is present for Index : " + i+ "and streetAddress is:"+childObj.getString("streetAddress"));
			}
			else
			{
				System.out.println("streetAddress is not present for index : " + i);
			}
			
			if(childObj.has("phone")){
				System.out.println("phone is present for Index : " + i+ "and phone is:"+childObj.getString("phone"));
			}
			else
			{
				System.out.println("phone is not present for index : " + i);
			}
		}
		/*String isowner = jsonObj.getString("responseMessage");
		System.out.println(isowner);
		String resultsCount = jsonObj.getString("responseCode");
		System.out.println(resultsCount);*/

		/*JSONArray jsonArr  = jsonObj.getJSONArray("results");

		for(int i = 0; i < jsonArr.length() ; i++)
		{
			JSONObject childObj = jsonArr.getJSONObject(i);
			String zipCode  = childObj.getString("zipCode");
		}*/
		
	}
	
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	
	private static String sendPost(String url,String urlParameters) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		/*System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);*/

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}
	

}
