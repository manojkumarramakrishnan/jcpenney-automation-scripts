package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class productdetails {
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSES FOR PRODUCT DETAILS.//");
		
		String productdetails1 = "https://registry.jcpenney.com/skavastream/core/v5/jcpenney/productdetails/NzgwNDAyMzAwMjZ8RkN8MjAxMw?channel=mobile&campaignId=732";
		String Response  = requestResponse(productdetails1);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("children")){
			System.out.println("children is present");
		}
		else
		{
			System.out.println("children not present");
		}
		
		if(jsonObj.has("type")){
			System.out.println("type is present and type is:"+jsonObj.getString("type"));
		}
		else
		{
			System.out.println("type not present");
		}
		
		JSONObject childrenObj = jsonObj.getJSONObject("children");
		if(childrenObj.has("categories")){
			System.out.println("categories is present");
		}
		else
		{
			System.out.println("categories not present");
		}
		
		JSONArray categoriesArr = childrenObj.getJSONArray("categories");
		
		for(int i =0; i < categoriesArr.length();i++)
		{
			JSONObject categoriesObj = categoriesArr.getJSONObject(i);
			
			
			if(categoriesObj.has("properties")){
				System.out.println("properties is present for Index : " + i);
			}
			else
			{
				System.out.println("properties not present for index : " + i);
			}
			JSONObject propertiesObj = categoriesObj.getJSONObject("properties");
			if(propertiesObj.has("buyinfo")){
				System.out.println("buyinfo is present.");
			}
			else
			{
				System.out.println("buyinfo not present.");
			
		}
			
			if(propertiesObj.has("iteminfo")){
				System.out.println("iteminfo is present.");
			}
			else
			{
				System.out.println("iteminfo not present.");
			}
			
			JSONObject buyinfoObj = propertiesObj.getJSONObject("buyinfo");
			if(buyinfoObj.has("deliverymsg")){
				System.out.println("deliverymsg is present and deliverymsg is:"+buyinfoObj.getString("deliverymsg"));
			}
			else
			{
				System.out.println("deliverymsg not present.");
		}
			
			if(buyinfoObj.has("inventory")){
				System.out.println("inventory is present and inventory is:"+buyinfoObj.getString("inventory"));
			}
			else
			{
				System.out.println("inventory not present.");
		}
			
			if(buyinfoObj.has("pricing")){
				System.out.println("pricing is present.");
			}
			else
			{
				System.out.println("pricing not present.");
		}
			
			JSONObject pricingObj = buyinfoObj.getJSONObject("pricing");
			
			if(pricingObj.has("prices")){
				System.out.println("prices is present.");
			}
			else
			{
				System.out.println("prices not present.");
		}
			
			JSONArray pricesArr = pricingObj.getJSONArray("prices");
			
			for(int j =0; j < pricesArr.length();j++)
			{
				JSONObject pricesObj = pricesArr.getJSONObject(i);
				
				
				if(pricesObj.has("label")){
					System.out.println("label is present for Index : " + j +"and lable is:"+pricesObj.getString("label"));
				}
				else
				{
					System.out.println("label not present for index : " + j);
				}
				if(pricesObj.has("type")){
					System.out.println("type is present for Index : " + j +"and type is:"+pricesObj.getString("type"));
				}
				else
				{
					System.out.println("type not present for index : " + j);
				}
				if(pricesObj.has("value")){
					System.out.println("value is present for Index : " + j +"and value is:"+pricesObj.getString("value"));
				}
				else
				{
					System.out.println("value not present for index : " + j);
				}
		}
		}
		
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
