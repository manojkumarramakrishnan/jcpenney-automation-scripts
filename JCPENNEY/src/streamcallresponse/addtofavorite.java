package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class addtofavorite {
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSES FOR ADD ITEM TO FAVORITES.//");
		
		String  additemtofav = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/addListItem/Favourites?item=%7B%22skuid%22%3A%22pp5004111294%22%2C%22name%22%3A%22Cuisinart%C2%AE%203-pc.%20Stainless%20Steel%20Mixing%20Bowls%20with%20Lids%20Set%22%2C%22properties%22%3A%7B%22img%22%3A%22http%3A%2F%2Fs7d9.scene7.com%2Fis%2Fimage%2FJCPenney%2FDP0618201417104383M.TIF%22%2C%22regPrice%22%3A%2270.00%22%2C%22salePrice%22%3A%2241.99%22%2C%22rangePrice%22%3A%22%22%7D%2C%22facets%22%3A%7B%22brand%22%3A%22Cuisinart%22%2C%22price%22%3A%2241.99%22%2C%22category%22%3A%22Kitchen%20%26%20Dining%22%7D%2C%22sort%22%3A%7B%22brand%22%3A%22Cuisinart%22%2C%22price%22%3A%2241.99%22%2C%22category%22%3A%22Kitchen%20%26%20Dining%22%7D%7D&customparams=%7B%7D&campaignId=732 ";
 

		String Response  = requestResponse(additemtofav);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("responseMessage:"+responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("responseCode:"+responseCode);
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
