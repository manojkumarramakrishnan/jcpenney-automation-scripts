package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class getstores {
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSES FOR GETSTORE.//");
		
		String getstores = "https://registry.jcpenney.com/skavastream/util/v5/jcpenney/getstores?skuid=78061070018&addressid1=37.79593620000001%2C-122.40000320000001&size1=25&channel=mobile&campaignId=732";
		String Response  = requestResponse(getstores);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("children")){
			System.out.println("children is present");
		}
		else
		{
			System.out.println("children not present");
		}
		
		if(jsonObj.has("type")){
			System.out.println("type is present and type is:"+jsonObj.getString("type"));
		}
		else
		{
			System.out.println("type not present");
		}
		
		if(jsonObj.has("properties")){
			System.out.println("properties is present");
		}
		else
		{
			System.out.println("properties not present");
		}
		
		JSONObject childrenObj = jsonObj.getJSONObject("children");
		if(childrenObj.has("inventory")){
			System.out.println("inventory is present." );
		}
		else
		{
			System.out.println("inventory not present.");
		}
		
		
		JSONArray inventoryArr = childrenObj.getJSONArray("inventory");
		for(int i =0; i < inventoryArr.length();i++)
		{
			JSONObject inventoryObj = inventoryArr.getJSONObject(i);
			
			
			if(inventoryObj.has("identifier")){
				System.out.println("identifier is present for Index : " + i +"and identifier is:"+inventoryObj.getString("identifier"));
			}
			else
			{
				System.out.println("identifier not present for index : " + i);
			}
			
			if(inventoryObj.has("image")){
				System.out.println("imageurl is present for Index : " + i +"and imageurl is:"+inventoryObj.getString("image"));
			}
			else
			{
				System.out.println("imageurl not present for index : " + i);
			}
			
			if(inventoryObj.has("properties")){
				System.out.println("properties is present for Index : " + i);
			}
			else
			{
				System.out.println("properties not present for index : " + i);
			}
			
			
			/*JSONArray propertiesArr = inventoryObj.getJSONArray("properties");
			for(int j =0; j < propertiesArr.length();j++)
			{
				JSONObject propertiesObj = propertiesArr.getJSONObject(i);
				
				
				if(propertiesObj.has("cartinfo")){
					System.out.println("cartinfo is present for Index : " + i);
				}
				else
				{
					System.out.println("cartinfo not present for index : " + i);
				}
				
				if(propertiesObj.has("storeinfo")){
					System.out.println("storeinfo is present for Index : " + i);
				}
				else
				{
					System.out.println("storeinfo not present for index : " + i);
				}
			}*/
			}
		
		JSONObject propertiesObj = jsonObj.getJSONObject("properties");
		if(propertiesObj.has("storeinfo")){
			System.out.println("storeinfo is present." );
		}
		else
		{
			System.out.println("storeinfo not present.");
		}
		JSONArray storeinfoArr = propertiesObj.getJSONArray("storeinfo");
		for(int i =0; i < storeinfoArr.length();i++)
		{
			JSONObject storeinfoObj = storeinfoArr.getJSONObject(i);
			
			
			if(storeinfoObj.has("address1")){
				System.out.println("address1 is present for Index : " + i +"and address1 is:"+storeinfoObj.getString("address1"));
			}
			else
			{
				System.out.println("address1 not present for index : " + i);
			}
			
			if(storeinfoObj.has("city")){
				System.out.println("city is present for Index : " + i +"and city is:"+storeinfoObj.getString("city"));
			}
			else
			{
				System.out.println("city not present for index : " + i);
			}
			
			if(storeinfoObj.has("distance")){
				System.out.println("distance is present for Index : " + i +"and distance is:"+storeinfoObj.getString("distance"));
			}
			else
			{
				System.out.println("distance not present for index : " + i);
			}
			
			if(storeinfoObj.has("name")){
				System.out.println("name is present for Index : " + i +"and name is:"+storeinfoObj.getString("name"));
			}
			else
			{
				System.out.println("name not present for index : " + i);
			}
			
			if(storeinfoObj.has("phone")){
				System.out.println("phone is present for Index : " + i +"and phone is:"+storeinfoObj.getString("phone"));
			}
			else
			{
				System.out.println("phone not present for index : " + i);
			}
			
			if(storeinfoObj.has("services")){
				System.out.println("services is present for Index : " + i );
			}
			else
			{
				System.out.println("services not present for index : " + i);
			}
			
			if(storeinfoObj.has("state")){
				System.out.println("state is present for Index : " + i +"and state is:"+storeinfoObj.getString("state"));
			}
			else
			{
				System.out.println("state not present for index : " + i);
			}
			
			if(storeinfoObj.has("zipcode")){
				System.out.println("zipcode is present for Index : " + i +"and zipcode is:"+storeinfoObj.getString("zipcode"));
			}
			else
			{
				System.out.println("zipcode not present for index : " + i);
			}
			
			JSONArray servicesArr = storeinfoObj.getJSONArray("services");
			for(int j =0; j < servicesArr.length();j++)
			{
				JSONObject servicesObj = servicesArr.getJSONObject(i);
				
				if(servicesObj.has("name")){
					System.out.println("name is present for Index : " + i +"and name is:"+storeinfoObj.getString("name"));
				}
				else
				{
					System.out.println("name not present for index : " + i);
				}
			
			}
		}
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
