package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class messagethankyou {
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSE FOR MESSAGE SEND THANK YOU.//");
		
		String messagesendthankyou  = "https://registry.jcpenney.com/skavamarketing/marketing/customservice/jcpenney/message?campaignId=732&listName=Registry&type=1&recipientEmail=meenakshi@skava.com&recipientName=meenakshi&senderMsg=Thank%20you%20for%20the%20wonderful%20gift!%20I%20can%20assure%20you%20that%20your%20gift%20will%20be%20used%20often.&channel=mobile";
		
		String Response  = requestResponse(messagesendthankyou);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("responseMessage:"+responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("responseCode:"+responseCode);
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	}


