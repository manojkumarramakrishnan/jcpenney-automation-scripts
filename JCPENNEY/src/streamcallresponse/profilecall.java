package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class profilecall {
	public static void main(String[] args) throws JSONException{

		System.out.println("//RESPONSE FOR PROFILECALL//");
		String Profilecall = "https://registry.jcpenney.com/skavastream/xact/v5/jcpenney/profile/L118826265471?channel=mobile&campaignId=732";
		
		String Response  = requestResponse(Profilecall);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("properties"))
		{
			System.out.println("properties is present.");
		}
		else
		{
			System.out.println("properties is not present.");
		}
		
		JSONObject propertiesObj = jsonObj.getJSONObject("properties");

		
		if(propertiesObj.has("state")){
			System.out.println("state is present for Index." );
		}
		else
		{
			System.out.println("state not present for index .");
		}
		JSONObject stateObj = propertiesObj.getJSONObject("state");
		if(stateObj.has("errorcode")){
			System.out.println("errorcode is present and errorcode is:"+ stateObj.has("errorcode"));
		}
		else
		{
			System.out.println("errorcode not present for index .");
		}
		
		if(stateObj.has("errormessage")){
			System.out.println("errormessage is present and errormessage is:"+ stateObj.has("errormessage"));
		}
		else
		{
			System.out.println("errormessage not present for index .");
		}
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
