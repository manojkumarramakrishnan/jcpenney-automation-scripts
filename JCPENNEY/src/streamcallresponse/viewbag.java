package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

public class viewbag {
	public static void main(String[] args) throws Exception {
		System.out.println("//RESPONSES FOR VIEWBAG.//");


		String viewbag = "https://registry.jcpenney.com/skavastream/xact/v5/jcpenney/viewbag?channel=mobile&campaignId=732";
		String Response  = requestResponse(viewbag);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);

		if(jsonObj.has("children")){
			System.out.println("children is present");
		}
		else
		{
			System.out.println("children not present");
		}

		if(jsonObj.has("type")){
			System.out.println("type is present.and type is:"+jsonObj.getString("type"));
		}
		else
		{
			System.out.println("type not present");
		}



		/*JSONArray resultArr = jsonObj.getJSONArray("properties");

		for(int i =0; i < resultArr.length();i++)
		{

		JSONObject childObj = resultArr.getJSONObject(i);*/

		if(jsonObj.has("properties")){
			System.out.println("properties is present for Index." );
		}
		else
		{
			System.out.println("properties not present for index .");
		}

		//JSONArray resultArr = jsonObj.getJSONArray("properties");
		// this is not array.. that is json object
		JSONObject propertiesObj = jsonObj.getJSONObject("properties");

	
			if(propertiesObj.has("state")){
				System.out.println("state is present for Index." );
			}
			else
			{
				System.out.println("state not present for index .");
			}
			
			JSONObject childObj = propertiesObj.getJSONObject("state");
			if(childObj.has("errormessage")){
				System.out.println("errormessage is present and errormessage is:"+childObj.getString("errormessage") );
			}
			else
			{
				System.out.println("errormessage not present for index .");
			}

			if(childObj.has("errorcode")){
				System.out.println("errorcode is present. and errorcode is:"+ childObj.getString("errorcode"));
			}
			else
			{
				System.out.println("errorcode not present for index .");
			}
		}
	




	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

	private static String sendPost(String url,String urlParameters) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");



		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		/*System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);*/

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}


}


