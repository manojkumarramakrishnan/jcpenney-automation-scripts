package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class additemtoregistry {
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSES FOR ADD ITEM TO REGISTRY.//");
		
		
		String additemtoregistry = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/addListItem/Registry?item=%7B%22skuid%22%3A%2278217280018%22%2C%22name%22%3A%22Cooks%2013-pc.%20Essential%20Aluminum%20Nonstick%20Cookware%20Set%22%2C%22properties%22%3A%7B%22profileId%22%3A%22C114611674973%22%2C%22identifier%22%3A%22pp5004890052%22%2C%22img%22%3A%22%2F%2Fs7d9.scene7.com%2Fis%2Fimage%2FJCPenney%2FDP0519201418111106C%3Fhei%3D380%26amp%3Bwid%3D380%26op_usm%3D.4%2C.8%2C0%2C0%26resmode%3Dsharp2%22%2C%22color%22%3A%22Black%22%2C%22regPrice%22%3A%22100.00%22%2C%22salePrice%22%3A%2259.99%22%2C%22itemquantity%22%3A1%2C%22wantedqty%22%3A1%2C%22inStore%22%3Afalse%2C%22encodeskuid%22%3A%22NzgyMTcyODAwMTh8RkN8MjAxMw%22%2C%22catalogId%22%3A%227821729%22%7D%2C%22facets%22%3A%7B%22brand%22%3A%22Cooks%22%2C%22price%22%3A%2259.99%22%2C%22color%22%3A%22Black%22%2C%22category%22%3A%22Kitchen%20%26%20Dining%22%7D%2C%22sort%22%3A%7B%22brand%22%3A%22Cooks%22%2C%22price%22%3A59.99%2C%22category%22%3A%22Kitchen%20%26%20Dining%22%7D%7D&customparams=%7B%7D&campaignId=732";
		
		 

		String Response  = requestResponse(additemtoregistry);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("responseMessage:"+responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("responseCode:"+responseCode);
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
