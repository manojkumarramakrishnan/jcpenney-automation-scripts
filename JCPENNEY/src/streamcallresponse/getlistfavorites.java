package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class getlistfavorites {
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSES FOR GETLIST FAVORITE.//");
		
		String getlistitemsfavorites = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/getListItems/99005459?offset=0&limit=200&campaignId=732";
		String Response  = requestResponse(getlistitemsfavorites);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("isowner")){
			System.out.println("isowner is present");
		}
		else
		{
			System.out.println("isowner not present");
		}
		
		if(jsonObj.has("resultsCount")){
			System.out.println("resultcount is present and resultcount is:"+jsonObj.getString("resultsCount"));
		}
		else
		{
			System.out.println("resultcount not present");
		}
		JSONArray resultArr = jsonObj.getJSONArray("results");
		for(int i =0; i < resultArr.length();i++)
		{
			JSONObject childObj = resultArr.getJSONObject(i);
			
			
			if(childObj.has("campaignid")){
				System.out.println("campaignid is present for Index : " + i+ "and campaignid is:"+childObj.getString("campaignid"));
			}
			else
			{
				System.out.println("campaignid not present for index : " + i);
			}
			
			if(childObj.has("facet_brand_732")){
				System.out.println("facet_brand_732 is present for Index : " + i+ "and facet_brand_732 is:"+childObj.getString("facet_brand_732"));
			}
			else
			{
				System.out.println("facet_brand_732 not present for index : " + i);
			}
			
			if(childObj.has("facet_category_732")){
				System.out.println("facet_category_732 is present for Index : " + i + "and facet_category_732 is:"+childObj.getString("facet_category_732"));
			}
			else
			{
				System.out.println("facet_category_732 not present for index : " + i);
			}
			
			if(childObj.has("facet_price_732")){
				System.out.println("facet_price_732 is present for Index : " + i+ "and facet_price_732 is:"+childObj.getString("facet_price_732"));
			}
			else
			{
				System.out.println("facet_price_732 not present for index : " + i);
			}
			
			if(childObj.has("img")){
				System.out.println("img url is present for Index : " + i + "and img url is:"+childObj.getString("img"));
			}
			else
			{
				System.out.println("img not present for index : " + i);
			}
			
			if(childObj.has("name")){
				System.out.println("name is present for Index : " + i+ "and name is:"+childObj.getString("name"));
			}
			else
			{
				System.out.println("name not present for index : " + i);
			}
			
			if(childObj.has("regPrice")){
				System.out.println("regPrice is present for Index : " + i+ "and regPrice is:"+childObj.getString("regPrice"));
			}
			else
			{
				System.out.println("regPrice not present for index : " + i);
			}
			
			if(childObj.has("salePrice")){
				System.out.println("salePrice is present for Index : " + i+ "and salePrice is:"+childObj.getString("salePrice"));
			}
			else
			{
				System.out.println("salePrice not present for index : " + i);
			}
			
			if(childObj.has("skuid")){
				System.out.println("skuid is present for Index : " + i+ "and skuid is:"+childObj.getString("skuid"));
			}
			else
			{
				System.out.println("skuid not present for index : " + i);
			}
			
			if(childObj.has("sortstr_brand_732")){
				System.out.println("sortstr_brand_732 is present for Index : " + i+ "and sortstr_brand_732 is:"+childObj.getString("sortstr_brand_732"));
			}
			else
			{
				System.out.println("sortstr_brand_732 not present for index : " + i);
			}
			
			if(childObj.has("sortstr_category_732")){
				System.out.println("sortstr_category_732 is present for Index : " + i+ "and sortstr_category_732 is:"+childObj.getString("sortstr_category_732"));
			}
			else
			{
				System.out.println("sortstr_category_732 not present for index : " + i);
			}
			
			if(childObj.has("sort_price_732")){
				System.out.println("sort_price_732 is present for Index : " + i+ "and sort_price_732 is:"+childObj.getString("sort_price_732"));
			}
			else
			{
				System.out.println("sort_price_732 not present for index : " + i);
			}
		}
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
