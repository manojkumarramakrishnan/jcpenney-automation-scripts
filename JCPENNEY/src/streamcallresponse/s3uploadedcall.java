package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class s3uploadedcall {
	public static void main(String[] args) throws JSONException{
		
		String S3uploadcall = "https://s3.amazonaws.com/assets.jcpenney.atfingertips.com/";
		String Response  = requestResponse(S3uploadcall);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
