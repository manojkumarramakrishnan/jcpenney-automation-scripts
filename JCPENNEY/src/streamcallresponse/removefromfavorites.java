package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class removefromfavorites {
	public static void main(String[] args) throws JSONException{
		System.out.println("//RESPONSES FOR REMOVE FROM FAVORITES.//");
		
		String removefromfav  = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/removeListItem/Favourites?skuid=pp5007420624&campaignId=732";
		String Response  = requestResponse(removefromfav);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("responseMessage:"+responseMessage);
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("responseCode:"+responseCode);
		
	}
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
