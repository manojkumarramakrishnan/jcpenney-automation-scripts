package streamcallresponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class productlist {
	public static void main(String[] args) throws JSONException{
		
		System.out.println("//RESPONSES FOR PRODUCTLIST.//");
		
		String ProductList  = "https://registry.jcpenney.com/skavastream/core/v5/jcpenney/productlist/1100009?campaignId=732";
		
		String Response  = requestResponse(ProductList);

		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		if(jsonObj.has("children")){
			System.out.println("children is present");
		}
		else
		{
			System.out.println("children not present");
		}
		
		if(jsonObj.has("image")){
			System.out.println("imageurl is present and imageurl is:"+jsonObj.getString("image"));
		}
		else
		{
			System.out.println("imageurl not present");
		}
		
		if(jsonObj.has("link")){
			System.out.println("link is present and link is:"+jsonObj.getString("link"));
		}
		else
		{
			System.out.println("link not present");
		}
		
		if(jsonObj.has("name")){
			System.out.println("name is present and name is:"+jsonObj.getString("name"));
		}
		else
		{
			System.out.println("name not present");
		}
		
		if(jsonObj.has("properties")){
			System.out.println("properties is present");
		}
		else
		{
			System.out.println("properties not present");
		}
		
		if(jsonObj.has("type")){
			System.out.println("type is present and type is:"+jsonObj.getString("type"));
		}
		else
		{
			System.out.println("type not present");
		}
		
		if(jsonObj.has("_name")){
			System.out.println("_name is present and name is:"+jsonObj.getString("_name"));
		}
		else
		{
			System.out.println("_name not present");
		}
		
		JSONObject childrenObj = jsonObj.getJSONObject("children");
		if(childrenObj.has("products")){
			System.out.println("products is present");
		}
		else
		{
			System.out.println("products not present");
		}
		
JSONArray productsArr = childrenObj.getJSONArray("products");
		
		for(int i =0; i < productsArr.length();i++)
		{
			JSONObject productsObj = productsArr.getJSONObject(i);
			
			
			if(productsObj.has("identifier")){
				System.out.println("identifier is present for Index : " + i +"and identifier is:"+productsObj.getString("identifier"));
			}
			else
			{
				System.out.println("identifier not present for index : " + i);
			}
			
			if(productsObj.has("image")){
				System.out.println("imageurl is present for Index : " + i +"and imageurl is:"+productsObj.getString("image"));
			}
			else
			{
				System.out.println("imageurl not present for index : " + i);
			}
			
			if(productsObj.has("link")){
				System.out.println("link is present for Index : " + i +"and link is:"+productsObj.getString("link"));
			}
			else
			{
				System.out.println("link not present for index : " + i);
			}
			
			if(productsObj.has("name")){
				System.out.println("name is present for Index : " + i +"and name is:"+productsObj.getString("name"));
			}
			else
			{
				System.out.println("name not present for index : " + i);
			}
			
			if(productsObj.has("properties")){
				System.out.println("properties is present for Index : " + i);
			}
			else
			{
				System.out.println("properties not present for index : " + i);
			}
			
			if(productsObj.has("type")){
				System.out.println("type is present for Index : " + i +"and type is:"+productsObj.getString("type"));
			}
			else
			{
				System.out.println("type not present for index : " + i);
			}
			
			JSONObject propertiesObj = productsObj.getJSONObject("properties");
			if(propertiesObj.has("buyinfo")){
				System.out.println("buyinfo is present");
			}
			else
			{
				System.out.println("buyinfo not present");
			}
			
			if(propertiesObj.has("iteminfo")){
				System.out.println("iteminfo is present");
			}
			else
			{
				System.out.println("iteminfo not present");
			}
			
			if(propertiesObj.has("reviewrating")){
				System.out.println("reviewrating is present");
			}
			else
			{
				System.out.println("reviewrating not present");
			}
			
			if(propertiesObj.has("state")){
				System.out.println("state is present");
			}
			else
			{
				System.out.println("state not present");
			}
			
			JSONObject buyinfoObj = propertiesObj.getJSONObject("buyinfo");
			if(buyinfoObj.has("isnew")){
				System.out.println("isnew is present and isnew is:"+buyinfoObj.getString("isnew"));
			}
			else
			{
				System.out.println("isnew not present");
			}
			
			if(buyinfoObj.has("preorder")){
				System.out.println("preorder is present and preorder is:"+buyinfoObj.getString("preorder"));
			}
			else
			{
				System.out.println("preorder not present");
			}
			
			if(buyinfoObj.has("pricing")){
				System.out.println("pricing is present");
			}
			else
			{
				System.out.println("pricing not present");
			}
			
			if(buyinfoObj.has("promomessages")){
				System.out.println("promomessages is present");
			}
			else
			{
				System.out.println("promomessages not present");
			}
			
			
		}
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}

}
