package streamcallresponse;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class addnotificationcreate {
	/*public static WebDriver driver = null;
	public static String LogFile = "E:\\Logs\\JCPPennyTestCasesERRORLOG.log";*/
	public static void main(String[] args) throws Exception{
		System.out.println("//RESPONSES FOR ADD NOTIFICATION (CREATE ACCOUNT).//");
		
		String URL = "https://registry.jcpenney.com/skavalist/list/v5/jcpenney/addNotification?campaignId=732&properties={%22callerid%22:%221%22,%22subject%22:%22Big%20day%20Notification%22,%22body%22:%22dfgdfg%22,%22fromname%22:%22ttert%22,%22eventDate%22:1477852200000,%22eventName%22:%22Registry%22,%22eventType%22:%22wedding%22,%22isFromCreateReg%22:true}&channel=a&doAddHostName=false&location=Erie%20Boulevard%20East&value=10312016&value=99005458";
		/*String postURL = URL.replaceAll("\\&.*","");
		String postParam = URL.replaceAll(".*\\&","");*/
		String Response  = sendPost(URL);

		/*String Response  ="";
		System.setProperty("webdriver.chrome.driver","E:\\Jars\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
		//driver = new FirefoxDriver();
		driver.get("https://registry.jcpenney.com/skavalist/list/v5/jcpenney/addNotification?campaignId=732&properties={\"callerid\":\"1\",\"subject\":\"Big day Notification\",\"body\":\"dfgdfg\",\"fromname\":\"ttert\",\"eventDate\":1477852200000,\"eventName\":\"Registry\",\"eventType\":\"wedding\",\"isFromCreateReg\":true}&channel=a&doAddHostName=false&location=Erie Boulevard East&value=10312016&value=99005458");
		Thread.sleep(10000);*/
		
		System.out.println(Response);
		JSONObject jsonObj = new JSONObject(Response);
		
		/*String responseMessage = jsonObj.getString("responseMessage");
		System.out.println("ResponseMessage:"+responseMessage);*/
		String responseCode = jsonObj.getString("responseCode");
		System.out.println("ResponseCode:"+responseCode);
	}
	
	static String requestResponse(String url)
	{
		try
		{
			URL Url = new URL(url);
			URLConnection conn = Url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line = "";
			StringBuffer strBuff = new StringBuffer();
			while((line = br.readLine()) != null)
			{
				strBuff.append(line);
			}

			return strBuff.toString();
		}
		catch(Throwable tr)
		{

		}

		return "";


	}
	
	private static String sendPost(String url) throws Exception {

		//String url = "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		//System.out.println(url +" and Param : " + urlParameters);

		//add reuqest header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		//wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		//int responseCode = con.getResponseCode();
		/*System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);*/

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());
		return response.toString();

	}

}
