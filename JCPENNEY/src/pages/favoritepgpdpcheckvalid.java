package pages;

import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import JCPpack.srcmain;

public class favoritepgpdpcheckvalid extends srcmain{
	
	public static void FavoritePgPdpCheck() throws IOException {
		// TODO Auto-generated method stub
		try
		{  System.out.println("//FAVORITE PAGE PRODUCT CHECK IS STARTED.//");

		logMessage = "ERROR :Favorite Icon is not clicked.\n";
		driver.findElement(By.xpath("//*[@pagename='favorites']")).click();
		System.out.println("Favorite Icon is  clicked..");
		Thread.sleep(10000);


		for(int i  = 0; i < 2 ;i++)
		{
			int pdtSize = driver.findElements(By.xpath("//*[@class='skr_registryItemsCont']")).size();
			int rvPdt = new Random().nextInt(pdtSize);

			//*[@class='skr_bcPdtCont'][17]//*[itemprop='name']
			String favoritepageprodcutname1;
			String favoritepageprodcutsaleprice1=""; 
			String favoritepageprodcutregprice1;


			favoritepageprodcutname1 = driver.findElement(By.xpath("(//*[@class='skr_registryItemsCont'])["+rvPdt+"]//*[@itemprop='name']")).getText();
			if(driver.findElements(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Sale']")).size() != 0)
			{
				favoritepageprodcutsaleprice1 = driver.findElement(By.xpath("(//*[@class='skr_registryItemsCont'])["+rvPdt+"]//*[@class='skr_registryItemsPrice skr_registryItemsSalePrice']")).getText();
			}
			favoritepageprodcutregprice1 = driver.findElement(By.xpath("(//*[@class='skr_registryItemsCont'])["+rvPdt+"]//*[@class='skr_registryItemsPrice skr_registryItemsRegPrice']")).getText(); 

			System.out.println(favoritepageprodcutname1 +"," + favoritepageprodcutsaleprice1+","+favoritepageprodcutregprice1);



			driver.findElement(By.xpath("(//*[@class='skr_registryItemsCont'])["+rvPdt+"]//*[@class='skr_registryAddToBagBtn']")).click();
			Thread.sleep(4000);

			String favoritepageprodcutname1popup = driver.findElement(By.xpath("//*[@class='skr_pdtGridItemDesc']")).getText();
			String favoritepageprodcutSaleprice1InPopUP = driver.findElement(By.xpath("(//*[@class='skr_addPrice sale'])[1]")).getText();
			String favoritepageprodcutRegprice1InPopUP = driver.findElement(By.xpath("//*[contains(@class,'skr_orgPrice ')]")).getText();
			//(//*[contains(@class,'skr_pdpFavIcn skr_isFavItem')])[5]

			if(favoritepageprodcutname1.equals(favoritepageprodcutname1popup))
			{
				System.out.println("Product Name  : "+favoritepageprodcutname1 +" is Matched in both side.");
			}
			else
			{
				System.out.println("Product Name  is Mismatch. In BrowsePage : "+favoritepageprodcutname1 +"  and In PopUpPage : " + favoritepageprodcutname1popup );
			}
			if(favoritepageprodcutsaleprice1.equals(favoritepageprodcutSaleprice1InPopUP))
			{
				System.out.println("Product Sale Price  : "+favoritepageprodcutsaleprice1 +" is Matched in both side.");
			}
			else
			{
				System.out.println("Product Sale Price  is Mismatch. In BrowsePage : "+favoritepageprodcutsaleprice1 +"  and In PopUpPage : " +favoritepageprodcutSaleprice1InPopUP );
			}
			if(favoritepageprodcutregprice1.equals(favoritepageprodcutRegprice1InPopUP))
			{
				System.out.println("Product Reg Price  : "+favoritepageprodcutregprice1 +" is Matched in both side.");
			}
			else
			{
				System.out.println("Product Reg Price  is Mismatch. In BrowsePage : "+favoritepageprodcutregprice1 +"  and In PopUpPage : " +favoritepageprodcutRegprice1InPopUP );
			}

			for(int j =0 ; j <= 1;j++)
			{
				logMessage = "ERROR:ProductQuantity 2 Times  is not clicked .\n";
				driver.findElement(By.xpath("//*[@class='skr_registryQtyIcon skr_registryItemsPlusIcon']")).click();
			}
			Thread.sleep(3000);
			try
			{
				logMessage = "ERROR:Product Colour is not Selected .\n";
				driver.findElement(By.xpath("(//*[contains(@class,'skr_color_items ')])[1]")).click();
				Thread.sleep(3000);
			}
			catch(Throwable color)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

			try{
				logMessage = "ERROR:Style is not Selected .\n";
				new Select(driver.findElement(By.xpath("//*[contains(@class,'skr_styleSelector')]"))).selectByIndex(1);
			}
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}
			try
			{
				logMessage = "ERROR:Add To Bag  is not clicked .\n";
				driver.findElement(By.xpath("//*[@aria-label='add to cart']")).click();
				System.out.println("Add To Shopping Bag.");
				Thread.sleep(3000);
			}
			catch(Throwable addregis)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;	
			}

			logMessage = "ERROR:Continue Shopping is not clicked .\n";
			driver.findElement(By.xpath("(//*[contains(@class,'skr_ATBPopupBtn ')])[2]")).click();
			System.out.println("Continue Shopping is clicked.");
			Thread.sleep(8000);

			System.out.println("//BROWSE PAGE ADD TO Bag PRODUCT 1 IS COMPLETED.// ");
		}

		logMessage = "ERROR:Favorite Icon is not clicked .\n";
		driver.findElement(By.xpath("(//*[@class='skr_registryItemsCont'])["+rvPdt+"]//*[@class='skr_pdpFavIcn skr_isFavItem']")).click();
		System.out.println("Favorite Icon is clicked.");
		Thread.sleep(4000);

		logMessage = "ERROR:Favorite Remove Popup  is not clicked .\n";
		driver.findElement(By.xpath("//*[@class='skr_exitRegBtn']")).click();
		System.out.println("Favorite Remove Popup is clicked.");
		Thread.sleep(4000);

		if(driver.findElements(By.xpath("//*[@class='skMob_Errors']")).size() !=0)
		{
			driver.findElement(By.xpath("//*[@class='skMob_ErrorsOK']")).click();
			Thread.sleep(2000);
		}

		System.out.println("//FAVORITE PAGE PRODUCT CHECK IS COMPLETED.//");
		}
		catch(Throwable favoritepgpdp)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}

}
