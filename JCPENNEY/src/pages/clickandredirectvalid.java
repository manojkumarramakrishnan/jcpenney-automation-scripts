package pages;

import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.By;

import JCPpack.srcmain;

public class clickandredirectvalid extends srcmain{
	
	
	public static void ClickAndRedirectionValidation() throws IOException,
	InterruptedException {
		// TODO Auto-generated method stub
		try {

			try {
				System.out.println("ClickAndRedirectionValidation is started to validate");
				logMessage = "ERROR:Dashboard is not displayed .\n";
				driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[1]")).isDisplayed();
				System.out.println("Dashboard is  displayed .");
				logMessage = "ERROR:Dashboard is not clicked .\n";
				driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[1]")).click();
				System.out.println("Dashboard is  clicked .");
				Thread.sleep(10000);
				logMessage = "ERROR:Checklist is not clicked .\n";
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-1']/div/div/div/div[2]/div/div/div[4]/div")).click();
				System.out.println("Checklist is  clicked .");
				Thread.sleep(5000);
			}
			catch (Throwable dash)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			DashboardchecklistValidation();


			try {
				logMessage = "ERROR:Browsecat is not displayed .\n";
				driver.findElement(By.xpath("//*[@pagename='browsecat']")).isDisplayed();
				System.out.println("Browsecat is not displayed ");
				logMessage = "ERROR:Browsecat is not clicked .\n";
				driver.findElement(By.xpath("//*[@pagename='browsecat']")).click();
				Thread.sleep(10000);
				String url3 = driver.getCurrentUrl();
				if (url3.contains("browsecat"))
				{
					System.out.println("Successfully moved to browsecat page");
				} 
				else 
				{
					System.out.println("Failed moved to browsecat page");
				}
			}
			catch (Throwable brows) 
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}

			try {
				logMessage = "ERROR:Tips is not displayed .\n";
				driver.findElement(By.xpath("//*[@pagename='tips']")).isDisplayed();
				logMessage = "ERROR:Tips is not clicked .\n";
				driver.findElement(By.xpath("//*[@pagename='tips']")).click();
				Thread.sleep(10000);
				String url5 = driver.getCurrentUrl();
				if (url5.contains("tips"))
				{
					System.out.println("Successfully moved to Tips page");
				}
				else
				{
					System.out.println("Failed moved to Tips page");
				}

			}
			catch (Throwable tp)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}

			System.out.println("ClickAndRedirectionValidation is completed");

		} catch (Throwable dsh) {
			System.out.println(logMessage);
			logInfo(logMessage);
		}
	}
	
	
	public static void DashboardchecklistValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			// checklist printicon dialogbox open close

			// Need to check the below lines
			/*String currentWindowName = driver.getWindowHandle();
			  try {

				logMessage = "ERROR:SharePrinting Button is not clicked .\n";
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-6']/div/div/div/div[1]/div[1]")).click();
				Thread.sleep(6000);

				Set<String> allWindowName = driver.getWindowHandles();
				for (String windowName : allWindowName) {
					if (!windowName.equals(currentWindowName))
					{
						driver.switchTo().window(windowName);

						// PrintMirror Page case289
						logMessage = "ERROR:PrintMirrorImage is not displayed .\n";
						driver.findElement(By.xpath("//*[@id='plugin']")).isDisplayed();
						logMessage = "ERROR:SharePrinting Cancel Button is not clicked .\n";
						driver.findElement(By.xpath("//*[@id='print-header']/div/button[2]")).click();
						Thread.sleep(7000);         
						break;
					}
				}


			} catch (Throwable et) {
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			driver.switchTo().window(currentWindowName);*/


			// CHECKBOX CLICKCABLE
			try {
				int sizeOfCategoryTitle = driver.findElements(By.xpath("//*[@class='skr_checklistCategoryTitle']")).size();
				System.out.println("Number of CategoryTitle: "+ sizeOfCategoryTitle);
				/*for (int i = 1; i <= sizeOfCategoryTitle; i++) 
				{
					logMessage = "ERROR:CategoryTitle is not displayed .\n";
					driver.findElement(By.xpath("//*[@class='skr_checklistCategoryTitle']")).isDisplayed();
					String CategoryName = driver.findElement(By.xpath("(//*[@class='skr_checklistCategoryTitle'])["+ i + "]")).getText();
					System.out.println("CategoryTitle : " + CategoryName);

				}*/
				int sizeOfSubMenus = driver.findElements(By.xpath("//*[@class='skr_chklistItemName']")).size();
				System.out.println("Number of Submenus: " + sizeOfSubMenus);
				/*for (int i = 1; i <= sizeOfSubMenus; i++) 
				{
					logMessage = "ERROR:ChecklistItemName is not displayed .\n";
					driver.findElement(By.xpath("(//*[@class='skr_chklistItemName'])[" + i+ "]")).isDisplayed();
					String SubmenuName = driver.findElement(By.xpath("(//*[@class='skr_chklistItemName'])[" + i+ "]")).getText();
					System.out.println("SubMenu Name : " + SubmenuName);
				}
*/
				int sizeOfCheckBox = driver.findElements(By.xpath("//*[@class='skr_checklistCategoryItemCont ']/div")).size();
				System.out.println("Number of Checkbox: " + sizeOfCheckBox);
				for (int i = 1; i <= sizeOfCheckBox; i++)
				{
					logMessage = "ERROR:ChecklistCategory is not clicked .\n";
					driver.findElement(By.xpath("(//*[@class='skr_checklistCategoryItemCont ']/div)["+ i + "]")).click();
					String className = driver.findElement(By.xpath("(//*[@class='skr_checklistCategoryItemCont ']/div)["+ i + "]")).getAttribute("class");

					if (className.contains("selected"))
					{
						System.out.println("Selected the checkbox for : " + i);
					}
					else
					{
						System.out.println("Not Selected the checkbox for : "+ i);
					}
				}

			} catch (Throwable cl) {
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			try { // CHECKLIST SHAREICON AND PRINTICON PRESENT

				logMessage = "ERROR:Checklist PrintIcon is not displayed .\n";
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-6']/div/div/div/div[1]/div[1]")).isDisplayed();
				System.out.println("Checklist PrintIcon is  displayed .");

			} catch (Throwable ico)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}

			String url1 = driver.getCurrentUrl();
			if (url1.contains("dashboard")) {
				System.out.println("Successfully moved to dashboard page");
			}
			else
			{
				System.out.println("Failed moved to dashboard page");
			}

		} catch (Throwable db)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}

}
