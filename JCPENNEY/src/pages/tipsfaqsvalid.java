package pages;

import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.By;

import JCPpack.srcmain;

public class tipsfaqsvalid extends srcmain{
	
	public static void TipsFaqValitation() throws IOException {
		// TODO Auto-generated method stub
		try {
			TipsValidation();
			FavoritesValidation();
			CommonBannersValidation();
			PrintButtonValidation();
			QuestionAnswerValidation();
		} 
		catch (Throwable tr)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	
	public static void TipsValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			try {
				logMessage = "ERROR:Tips header is not clicked .\n";
				driver.findElement(By.xpath("//*[@pagename='tips']")).click();
				Thread.sleep(10000);
				logMessage = "ERROR:Registry Faqs See More button is not clicked .\n";
				driver.findElement(By.xpath("//*[@id='skr_catGridContWrapper_id']/div/div[1]/div[1]/div[3]/div/div[1]")).click();
				Thread.sleep(10000);
				String faqurl = driver.getCurrentUrl();

				if (faqurl.contains("Registry/faq"))
				{
					System.out.println("Successfully moved to Registry Faqs page");
				} 
				else 
				{
					System.out.println("Failed moved to Registry Faqs page");
				}
			} catch (Throwable faq) 
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}
			try {
				logMessage = "ERROR:Tips header is not clicked .\n";
				driver.findElement(By.xpath("//*[@pagename='tips']")).click();
				Thread.sleep(10000);
				logMessage = "ERROR:Announcement See More button is not clicked .\n";
				driver.findElement(By.xpath("//*[@id='skr_catGridContWrapper_id']/div/div[2]/div[1]/div[3]/div/div[2]")).click();
				Thread.sleep(10000);
				String announurl = driver.getCurrentUrl();

				if (announurl.contains("Registry/announcements")) {
					System.out.println("Successfully moved to Announcement page");
				} 
				else 
				{
					System.out.println("Failed moved to Announcement page");
				}
			} catch (Throwable annou)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

			try {
				logMessage = "ERROR:Tips header is not clicked .\n";
				driver.findElement(By.xpath("//*[@pagename='tips']")).click();
				Thread.sleep(10000);
				logMessage = "ERROR:Completion Offer See More button is not clicked .\n";
				driver.findElement(By.xpath("//*[@id='skr_catGridContWrapper_id']/div/div[3]/div[1]/div[3]/div")).click();
				Thread.sleep(10000);
				String compurl = driver.getCurrentUrl();

				if (compurl.contains("Registry/completion_offer")) {
					System.out.println("Successfully moved to Completion Offer page");
				} 
				else 
				{
					System.out.println("Failed moved to Completion Offer page");
				}
			} catch (Throwable compl) 
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

			try {
				logMessage = "ERROR:Tips header is not clicked .\n";
				driver.findElement(By.xpath("//*[@pagename='tips']")).click();
				Thread.sleep(10000);
				logMessage = "ERROR:Guest Offer See More button is not clicked .\n";
				driver.findElement(By.xpath("//*[@id='skr_catGridContWrapper_id']/div/div[4]/div[1]/div[3]/div/div[2]")).click();
				Thread.sleep(10000);
				String gusurl = driver.getCurrentUrl();

				if (gusurl.contains("Registry/guest_offer")) {
					System.out.println("Successfully moved to Guest Offer page");
				}
				else 
				{
					System.out.println("Failed moved to Guest Offer page");
				}
			} catch (Throwable gust) 
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

			logMessage = "ERROR:Tips header is not clicked .\n";
			driver.findElement(By.xpath("//*[@pagename='tips']")).click();
			Thread.sleep(10000);
		} catch (Throwable tv) 
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void FavoritesValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			// case283

			logMessage = "ERROR: Favorite icon is not clicked .\n";
			driver.findElement(By.xpath("//*[@pagename='favorites']")).click();
			System.out.println("Favorite icon is  clicked .");
			Thread.sleep(5000);

			String favorite = driver.getCurrentUrl();

			if (favorite.contains("favorites"))
			{
				System.out.println("Successfully moved to Favorite page.");
			} 
			else 
			{
				System.out.println("Failed moved to Favorite page");
			}

			logMessage = "ERROR:Tips header is not clicked .\n";
			driver.findElement(By.xpath("//*[@pagename='tips']")).click();
			Thread.sleep(10000);

		} catch (Throwable fv)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void CommonBannersValidation() throws IOException {
		// TODO Auto-generated method stub
		try { // case290

			if (driver.findElements(By.xpath("//*[@id='skr_jcpCustomCatg_id']/div/div/div/div[1]/div/div/div[1]")).size() != 0)
			{
				System.out.println("Inspiration Gallery Banner Is displayed");
			} 
			else
			{
				System.out.println("Inspiration Gallery Banner is not displayed");
			}

			if (driver.findElements(By.xpath("//*[@id='skr_jcpCustomCatg_id']/div/div/div/div[2]/div/div/div[1]")).size() != 0)
			{
				System.out.println("Brands you love Banner Is displayed");
			}
			else 
			{
				System.out.println("Brands you love Banner is not displayed");
			}
			if (driver.findElements(By.xpath("//*[@id='skr_jcpCustomCatg_id']/div/div/div/div[3]/div/div/div[1]")).size() != 0)
			{
				System.out.println("Registry Checklist Banner Is displayed");
			} 
			else
			{
				System.out.println("Registry Checklist Banner is not displayed");
			}
		}
		catch (Throwable cbv) 
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void PrintButtonValidation() throws IOException {
		// TODO Auto-generated method stub
		try {

			//CHECKLIST  

			logMessage = "ERROR:Checklist is not clicked .\n";
			driver.findElement(By.xpath("//*[@pagename='checklist']")).click();
			System.out.println("Checklist is  clicked .");
			Thread.sleep(10000);

			if (driver.findElements(By.xpath("//*[@class='skr_checklistPrintIcon']")).size() != 0)
			{
				System.out.println("SharePrinting Button Is displayed");
			}
			else
			{
				System.out.println("SharePrinting Button is not displayed");
			}

			//PrintValidation();
		}
		catch (Throwable spb)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void QuestionAnswerValidation() throws IOException
	{
		// TODO Auto-generated method stub
		try { // case286


			logMessage = "ERROR:Tips is not clicked .\n";
			driver.findElement(By.xpath("//*[@pagename='tips']")).click();
			System.out.println("Tips is  clicked .");
			Thread.sleep(10000);

			logMessage = "ERROR:Registry Faqs is not clicked .\n";
			driver.findElement(By.xpath("(//*[@class='skr_readMoreText'])[1]")).click();
			System.out.println("Registry Faqs is  clicked .");
			Thread.sleep(10000);

			int sizeOfquestion = driver.findElements(By.xpath("//*[@class='cls-question']")).size();
			System.out.println("Number of Question: " + sizeOfquestion);
			for (int i = 1; i <= sizeOfquestion; i++) {
				driver.findElement(By.xpath("(//*[@class='cls-question'])[" + i + "]")).isDisplayed();
				String ques = driver.findElement(By.xpath("(//*[@class='cls-question'])[" + i + "]")).getText();
				//System.out.println("Questions are displayed" + ques);
				System.out.println("Questions all are displayed");
			}
			try {
				int sizeOfanswer = driver.findElements(By.xpath("//*[@class='cls-answer']")).size();
				System.out.println("Number of Answer: " + sizeOfanswer);
				for (int i = 1; i <= sizeOfanswer; i++)
				{
					driver.findElement(By.xpath("(//*[@class='cls-answer'])[" + i + "]")).isDisplayed();
					String ans = driver.findElement(By.xpath("(//*[@class='cls-answer'])[" + i + "]")).getText();
					//System.out.println("Answers are displayed" + ans);
					System.out.println("Answers all are displayed");
				}
			} catch (Throwable ans)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

		} catch (Throwable qav) 
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}

}
