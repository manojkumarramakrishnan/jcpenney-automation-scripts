package pages;

import java.io.IOException;
import java.util.Random;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import JCPpack.srcmain;

public class viewregistryaddprod extends srcmain{
	
	
	
	
	
	public static void ViewRegistryAddProductValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			
				
				ViewRegistryAddPdpRandom();
				RemoveProduct();
			    ViewAsGuestValidation();



		}
		catch(Throwable addproduct)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void ViewRegistryAddPdpRandom() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//VIEW REGISTRY PAGE:ADD PRODUCT IN RANDOM IS STARTED.//");
			if(driver.findElements(By.xpath("//*[@class='skr_viewRegHeaderEmpty']")).size() !=0)
			{
				System.out.println("The requested registry is either public static or does not exist.");
			}
			else
			{
				for(int i  = 0; i < 3 ;i++)
				{
					int categorySize = driver.findElements(By.xpath("//*[@class='skr_registryAddItemsTxt']")).size();
					int rvcat = new Random().nextInt(categorySize);
					logMessage = "ERROR:Registry page: Category  is not clicked .\n";
					driver.findElement(By.xpath("(//*[@class='skr_registryAddItemsTxt'])["+rvcat+"]")).click();
					System.out.println("Registry page: Category  is  clicked .");
					Thread.sleep(7000);


					int pdtSize = driver.findElements(By.xpath("//*[@class='skr_bcPdtCont']")).size();
					int rvPdt = new Random().nextInt(pdtSize);

					//*[@class='skr_bcPdtCont'][17]//*[itemprop='name']
					String registrypageprodcutname1;
					String registrypageprodcutsaleprice1=""; 
					String registrypageprodcutregprice1;


					registrypageprodcutname1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@itemprop='name']")).getText();
					if(driver.findElements(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Sale']")).size() != 0)
					{
						registrypageprodcutsaleprice1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Sale']")).getText();
					}
					registrypageprodcutregprice1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Reg']")).getText(); 

					System.out.println(registrypageprodcutname1 +"," + registrypageprodcutsaleprice1+","+registrypageprodcutregprice1);

					logMessage = "ERROR:Favorite Icon  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@class='skFavWrap grid']/div")).click();
					System.out.println("Favorite Icon  is  clicked .");
					Thread.sleep(4000);

					logMessage = "ERROR:Browse Page Product  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@aria-label='buy-manage']")).click();
					System.out.println("Browse Page Product  is  clicked .");
					Thread.sleep(7000);
					try{
						if(driver.findElements(By.xpath("//*['@class='skMob_Errors']")).size() !=0);
						{
							driver.findElement(By.xpath("//*['@id='skMob_ErrorsOK_id']")).click();
						}
							
					}
					catch(Throwable errormsg)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}

					//*[@class='skMob_Errors']
					//*[@class='skMob_ErrorsOK']

					String registrypageprodcutName1InPopUP = driver.findElement(By.xpath("//*[@class='skr_pdtGridItemDesc']")).getText();
					String registrypageprodcutSaleprice1InPopUP = driver.findElement(By.xpath("(//*[@class='skr_addPrice sale'])[1]")).getText();
					String registrypageprodcutRegprice1InPopUP = driver.findElement(By.xpath("//*[contains(@class,'skr_orgPrice ')]")).getText();
					//(//*[contains(@class,'skr_pdpFavIcn skr_isFavItem')])[5]

					if(registrypageprodcutname1.equals(registrypageprodcutName1InPopUP))
					{
						System.out.println("Product Name  : "+registrypageprodcutname1 +" is Matched in both side.");
					}
					else
					{
						System.out.println("Product Name  is Mismatch. In BrowsePage : "+registrypageprodcutname1 +"  and In PopUpPage : " + registrypageprodcutName1InPopUP );
					}
					if(registrypageprodcutsaleprice1.equals(registrypageprodcutSaleprice1InPopUP))
					{
						System.out.println("Product Sale Price  : "+registrypageprodcutsaleprice1 +" is Matched in both side.");
					}
					else
					{
						System.out.println("Product Sale Price  is Mismatch. In BrowsePage : "+registrypageprodcutsaleprice1 +"  and In PopUpPage : " +registrypageprodcutSaleprice1InPopUP );
					}
					if(registrypageprodcutregprice1.equals(registrypageprodcutRegprice1InPopUP))
					{
						System.out.println("Product Reg Price  : "+registrypageprodcutregprice1 +" is Matched in both side.");
					}
					else
					{
						System.out.println("Product Reg Price  is Mismatch. In BrowsePage : "+registrypageprodcutregprice1 +"  and In PopUpPage : " +registrypageprodcutRegprice1InPopUP );
					}

					for(int j =0 ; j <= 1;j++)
					{
						logMessage = "ERROR:ProductQuantity 2 Times  is not clicked .\n";
						driver.findElement(By.xpath("//*[@class='skr_registryQtyIcon skr_registryItemsPlusIcon']")).click();
						//System.out.p
					}
					Thread.sleep(3000);
					try
					{
						logMessage = "ERROR:Product Colour is not Selected .\n";
						driver.findElement(By.xpath("(//*[contains(@class,'skr_color_items ')])[1]")).click();
						Thread.sleep(3000);
					}
					catch(Throwable color)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}

					try{
						logMessage = "ERROR:Style is not Selected .\n";
						new Select(driver.findElement(By.xpath("//*[contains(@class,'skr_styleSelector')]"))).selectByIndex(1);
					}
					catch(Throwable st)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}
					try
					{
						logMessage = "ERROR:Add To Bag  is not clicked .\n";
						driver.findElement(By.xpath("//*[@aria-label='add to cart']")).click();
						System.out.println("Add To Shopping Bag.");
						Thread.sleep(3000);
					}
					catch(Throwable addregis)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;	
					}

					logMessage = "ERROR:Continue Shopping is not clicked .\n";
					driver.findElement(By.xpath("(//*[contains(@class,'skr_ATBPopupBtn ')])[2]")).click();
					System.out.println("Continue Shopping is clicked.");
					Thread.sleep(8000);

					System.out.println("//VIEW REGISTRY PAGE:ADD PRODUCT IN RANDOM IS COMPLETED.//");
				}	
			}
		}
		catch(Throwable viewregistryaddpdprandom)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;	
		}

	}
	
	
	public static void RemoveProduct() throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		//REMOVE PRODUCTS
		try
		{
			logMessage = "ERROR:First Product Remove Button is not clicked .\n";
			driver.findElement(By.xpath("(//*[@class='skr_registryremoveCont']/div)[2]")).click();
			Thread.sleep(3000);
			logMessage = "ERROR:Product menu pop menu YES  is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='skr_exitRegBtn']")).click();
			Thread.sleep(3000);
			//*[@class='skReg_regCloseButton']
			logMessage = "ERROR:Product menu pop menu YES  is not clicked .\n";
			driver.findElement(By.xpath("/html/body/div[8]/div/div[1]/div")).click();
			Thread.sleep(5000);
		}
		catch(Throwable rp)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	
	
	public static void ViewAsGuestValidation() throws IOException 
	{
		// TODO Auto-generated method stub
		try
		{
			System.out.println("VIEW AS GUEST VALIDATION STARTED");
			logMessage = "ERROR:View As Guest Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_27_id-1']/div/div/div/div[2]/div[1]/div[1]/div[2]")).click();
			System.out.println("View As Guest Button is  clicked .");
			Thread.sleep(8000);

			//*[@id='skr_ListView_id']
			logMessage = "ERROR:List View Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skr_ListView_id']")).click();
			Thread.sleep(8000);

			String registrydate = driver.findElement(By.xpath("//*[@id='skPageLayoutCell_27_id-1']/div/div/div/div[1]/div[2]/div[2]")).getText();
			System.out.println("Regisry Date:"+registrydate);

			logMessage = "ERROR:Product Name are  not displayed .\n";
			if(driver.findElements(By.xpath("//*[@class='skr_registryItemsNameCont']")).size() != 0)
			{
				int sizeofpdpnameguest = driver.findElements(By.xpath("//*[@class='skr_registryItemsNameCont']")).size();
				System.out.println("Number of product name in view as a guest:"+sizeofpdpnameguest);
				for(int i=0 ; i<=sizeofpdpnameguest ; i++)
				{
					String productnameguest = driver.findElement(By.xpath("(//*[@class='skr_registryItemsNameCont'])["+i+"]")).getText();
					System.out.println("Product Name:"+productnameguest);
				}
			}


			logMessage = "ERROR:Product Images are  not displayed .\n";
			if(driver.findElements(By.xpath("//*[@class='skr_registryItemsImg']")).size() != 0)
			{
				System.out.println("Product Images Are Displayed.");
			}
			else
			{
				System.out.println("Product Images Are Not Displayed.");
			}
			int sizeofsalepriceguest = driver.findElements(By.xpath("//*[contains(@class,'skr_registryItemsSalePrice')]")).size();
			System.out.println("Number of sale price in guest:"+sizeofsalepriceguest);
			for(int j=0 ; j<=sizeofsalepriceguest ; j++)
			{
				String productsalepriceguest = driver.findElement(By.xpath("(//*[contains(@class,'skr_registryItemsSalePrice')])["+j+"]")).getText();
				System.out.println("Product sale Price in guest:"+productsalepriceguest);
			}


			int sizeoforiginalpriceguest = driver.findElements(By.xpath("(//*[contains(@class,'skr_registryItemsRegPrice')])")).size();
			System.out.println("Number of original price in guest:"+sizeoforiginalpriceguest);
			for(int k=0 ; k<=sizeoforiginalpriceguest ; k++)
			{
				String productoriginalpriceguest = driver.findElement(By.xpath("(//*[contains(@class,'skr_registryItemsRegPrice')])["+k+"]")).getText();
				System.out.println("Product original Price in guest:"+productoriginalpriceguest);
			}
			logMessage = "ERROR:Back To Managing Registry is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_27_id-1']/div/div/div/div[2]/div[1]/div")).click();
			Thread.sleep(8000);

			logMessage = "ERROR:Back Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='skr_bcViewBtnIcon")).click();
			System.out.println("Back Button is clicked");
			Thread.sleep(10000);
			System.out.println("VIEW AS GUEST VALIDATION COMPLETED.");


		}
		catch(Throwable guest)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}


}
