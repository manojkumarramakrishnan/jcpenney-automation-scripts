package pages;

import java.io.IOException;
import java.util.Random;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import JCPpack.srcmain;

public class browsepagevalid extends srcmain{


	public static void BrowsePageValidation() throws IOException 
	{
		// TODO Auto-generated method stub
		try
		{
			BrowseCategorypdpvalidation();
			BrowseSubCategorypdpValidation();
			BrowsePageItemPresentValidation();
			BrowsePageProductVlidation();
			BrowsePageHighToLowValidation();
			BrowsePageLowToHighValidation();
			BrowsePageGridListValidation();
			FavoriteIconValidation();
			BrowsePageAddToBagPdpRandom();
			BrowsePageAddToRegPdpRandom();

		}
		catch(Throwable browsepage)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}

	

	



	private static void BrowseCategorypdpvalidation() {
		// TODO Auto-generated method stub
			try
			{
				System.out.println("//BROWSE ITEM PRESENT VALIDATION STARTED.//");
				logMessage = "ERROR :Browse Item is not clicked.\n";
				driver.findElement(By.xpath("//*[@name='browse items']")).click();
				System.out.println("Browse Item is  clicked.");
				Thread.sleep(8000);
				
				System.out.println("//BROWSEPAGE CATEGORY  VALIDATION STARTED.//");

				int sizeofbrowsepagecatimage = driver.findElements(By.xpath("(//*[contains(@itemprop,'image')])")).size();
				System.out.println("Number of BrowsePage Category Image: " + sizeofbrowsepagecatimage );
				for(int i = 1; i <= sizeofbrowsepagecatimage ; i++)
				{     
					try
					{
						if(driver.findElements(By.xpath("(//*[contains(@itemprop,'image')])["+i+"]")).size() != 0)
						{
							System.out.println("BrowsePage Category  Image "+i+" is displayed");
						}
						else
						{
							System.out.println("BrowsePage Category  Image "+i+" is  not displayed");
						}
					}
					catch(Throwable browsepagecatimage)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}
				}


				int sizeofbrowsepagecatname = driver.findElements(By.xpath("(//*[contains(@itemprop,'name')])")).size();
				System.out.println("Number of BrowsePage Product Name: " + sizeofbrowsepagecatname );
				for(int j = 1; j <= sizeofbrowsepagecatname ; j++)
				{     
					try
					{
						String	 browsepagecatname = "";
						if(driver.findElements(By.xpath("(//*[contains(@itemprop,'name')])["+j+"]")).size() != 0)
						{

							browsepagecatname = driver.findElement(By.xpath("(//*[contains(@itemprop,'name')])["+j+"]")).getText();
							System.out.println("BrowsePage Product Name "+j+":"+ browsepagecatname +" is displayed");
						}
						else
						{
							System.out.println("BrowsePage Product Name "+j+":"+browsepagecatname +" is not displayed");
						}
					}
					catch(Throwable browsepagecatname)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}
				}
				
				try
				{
					int pdtSize = driver.findElements(By.xpath("//*[contains(@itemprop,'image')]")).size();
					int rvPdt = new Random().nextInt(pdtSize);
					
					logMessage = "ERROR:Browse Page Category  is not clicked .\n";
					driver.findElement(By.xpath("(//*[contains(@itemprop,'image')])["+rvPdt+"]")).click();
					System.out.println("Browse Page Category  is  clicked .");
					Thread.sleep(12000);
				}
				catch(Throwable categoryclick)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}

				System.out.println("//BROWSEPAGE CATEGORY VALIDATION COMPLETED.//");
			}
			catch(Throwable browsepageproduct)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

		}
		
		
	private static void BrowseSubCategorypdpValidation() {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//BROWSEPAGE SUB CATEGORY  VALIDATION STARTED.//");
			
			logMessage = "ERROR:Browse Page Sub Category Banner  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skr_bannerHotspot_id']")).isDisplayed();
			System.out.println("Browse Page Category Banner is  displayed .");
			

			int sizeofbrowsepagesubcatimage = driver.findElements(By.xpath("(//*[contains(@itemprop,'image')])")).size();
			System.out.println("Number of BrowsePage Sub Category Image: " + sizeofbrowsepagesubcatimage );
			for(int i = 1; i <= sizeofbrowsepagesubcatimage ; i++)
			{     
				try
				{
					if(driver.findElements(By.xpath("(//*[contains(@itemprop,'image')])["+i+"]")).size() != 0)
					{
						System.out.println("BrowsePage Sub Category  Image "+i+" is displayed");
					}
					else
					{
						System.out.println("BrowsePage Sub Category  Image "+i+" is  not displayed");
					}
				}
				catch(Throwable browsepagecatimage)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
			}


			int sizeofbrowsepagesubcatname = driver.findElements(By.xpath("(//*[contains(@itemprop,'name')])")).size();
			System.out.println("Number of BrowsePage Sub Category Name: " + sizeofbrowsepagesubcatname );
			for(int j = 1; j <= sizeofbrowsepagesubcatname ; j++)
			{     
				try
				{
					String	 browsepagesubcatname = "";
					if(driver.findElements(By.xpath("(//*[contains(@itemprop,'name')])["+j+"]")).size() != 0)
					{

						browsepagesubcatname = driver.findElement(By.xpath("(//*[contains(@itemprop,'name')])["+j+"]")).getText();
						System.out.println("BrowsePage Sub Category Name "+j+":"+ browsepagesubcatname +" is displayed");
					}
					else
					{
						System.out.println("BrowsePage Sub Category Name "+j+":"+browsepagesubcatname +" is not displayed");
					}
				}
				catch(Throwable browsepagesubcatname)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
			}
			
			try
			{
				int pdtSize = driver.findElements(By.xpath("//*[contains(@itemprop,'image')]")).size();
				int rvPdt = new Random().nextInt(pdtSize);
				
				logMessage = "ERROR:Browse Page Sub Category  is not clicked .\n";
				driver.findElement(By.xpath("(//*[contains(@itemprop,'image')])["+rvPdt+"]")).click();
				System.out.println("Browse Page Sub Category  is  clicked .");
				Thread.sleep(12000);
			}
			catch(Throwable subcategoryclick)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

			System.out.println("//BROWSEPAGE CATEGORY VALIDATION COMPLETED.//");
		}
		catch(Throwable browsepageproduct)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
		
	}
	

	public static void BrowsePageItemPresentValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{

			

			logMessage = "ERROR :Featured DropDownBox is not displayed.\n";
			driver.findElement(By.xpath("//*[@aria-label='featured']")).isDisplayed();
			System.out.println("Featured DropDownBox is displayed.");

			logMessage = "ERROR :Filter DropDownBox is not displayed.\n";
			driver.findElement(By.xpath("//*[@class='skr_bcFilterLabel']")).isDisplayed();
			System.out.println("Filter DropDownBox is displayed.");

			logMessage = "ERROR :Item Count is not displayed.\n";
			String browsepageitemcount = driver.findElement(By.xpath("//*[@class='skr_bcCount']")).getText();
			System.out.println("Item Count is displayed.");
			System.out.println("BrowsePage Item Count :"+browsepageitemcount);

			logMessage = "ERROR :Grid View Icon is not displayed.\n";
			driver.findElement(By.xpath("//*[@aria-label='grid view icon']")).isDisplayed();
			System.out.println("Grid View Icon is displayed.");

			logMessage = "ERROR :List View Icon is not displayed.\n";
			driver.findElement(By.xpath("//*[@aria-label='list view icon']")).isDisplayed();
			System.out.println("List View Icon is displayed.");

			logMessage = "ERROR :BrowsePage Product All Image is not displayed.\n";
			driver.findElement(By.xpath("(//*[contains(@itemprop,'image')])")).isDisplayed();
			System.out.println("BrowsePage Product All Image is not displayed.");

			System.out.println("//BROWSE ITEM PRESENT VALIDATION COMPLETED.//");
		}
		catch(Throwable itempresent)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}


	public static void BrowsePageProductVlidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//BROWSEPAGE PRODUCT VALIDATION STARTED.//");

			int sizeofbrowsepageproductimage = driver.findElements(By.xpath("(//*[contains(@itemprop,'image')])")).size();
			System.out.println("Number of BrowsePage Product Image: " + sizeofbrowsepageproductimage );
			for(int i = 1; i <= sizeofbrowsepageproductimage ; i++)
			{     
				try
				{
					if(driver.findElements(By.xpath("(//*[contains(@itemprop,'image')])["+i+"]")).size() != 0)
					{
						System.out.println("BrowsePage Product Image "+i+" is displayed");
					}
					else
					{
						System.out.println("BrowsePage Product Image "+i+" is  not displayed");
					}
				}
				catch(Throwable browsepageimage)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
			}


			int sizeofbrowsepageproductname = driver.findElements(By.xpath("(//*[contains(@itemprop,'name')])")).size();
			System.out.println("Number of BrowsePage Product Name: " + sizeofbrowsepageproductname );
			for(int j = 1; j <= sizeofbrowsepageproductname ; j++)
			{     
				try
				{
					String	 browsepageitemname = "";
					if(driver.findElements(By.xpath("(//*[contains(@itemprop,'name')])["+j+"]")).size() != 0)
					{

						browsepageitemname = driver.findElement(By.xpath("(//*[contains(@itemprop,'name')])["+j+"]")).getText();
						System.out.println("BrowsePage Product Name "+j+":"+ browsepageitemname +" is displayed");
					}
					else
					{
						System.out.println("BrowsePage Product Name "+j+":"+browsepageitemname +" is not displayed");
					}
				}
				catch(Throwable browsepageimage)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
			}

			System.out.println("//BROWSEPAGE PRODUCT VALIDATION COMPLETED.//");
		}
		catch(Throwable browsepageproduct)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}


	public static void BrowsePageHighToLowValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{

			System.out.println("//BROWSE PAGE HIGH TO LOW VALIDATION STARTED.//");
			logMessage = "ERROR :Featured is not clicked.\n";
			driver.findElement(By.xpath("//*[@aria-label='featured']")).click();
			System.out.println("Featured is  clicked.");
			Thread.sleep(4000);


			try{
				logMessage = "ERROR:ShortBy High To Low  is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[contains(@aria-label,'Sort By')])"))).selectByIndex(8);
				driver.findElement(By.xpath("//*[@sortvalue='pricehightolow']")).click();
				Thread.sleep(4000);
				System.out.println("Short High To Low is  Selected .");

				//*[@class='skr_sortDone']
				logMessage = "ERROR :Apply is not clicked.\n";
				driver.findElement(By.xpath("//*[@class='skr_sortDone']")).click();
				System.out.println("Apply is  clicked.");
				Thread.sleep(8000);

			}
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}
			//(//*[@itemprop='price'])[2]
			try
			{
				int sizeofhightolowprice= driver.findElements(By.xpath("(//*[@pricetype='Reg'])")).size();
				System.out.println("Number of High to Low Price:"+sizeofhightolowprice);

				//*[@id="skr_id_regItemsPrice_78811680018"]
				Float prev= 0.0f;
				Float next = 0.0f;


				for(int i=1;i<=sizeofhightolowprice ; i++)
				{
					logMessage = "ERROR:Registry Item High to Low Price  is not displayed .\n";
					String registryitemhightolow = driver.findElement(By.xpath("(//*[@pricetype='Reg'])["+i+"]")).getText();

					//$29.99 \\+ \\. \\\ -

					registryitemhightolow = registryitemhightolow.replaceAll(" Original", "").replaceAll("\\$", "");


					prev = Float.parseFloat(registryitemhightolow);

					if(i != 1)
					{

						//if(prev.compareTo(next) < - 1)
						if(prev.compareTo(next) < 0)
						{
							System.out.println("Sorting order is wrong...");
							break;
						}
						else
						{
							System.out.println("Product Order is correct.. First Value : " + next +" Second Value : " + prev+" for Product : " + i);
						}
					}
					next = prev;

				}


			}
			catch(Throwable hightolow)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;	
			}
			System.out.println("//BROWSE PAGE HIGH TO LOW VALIDATION COMPLETED.//");
		}
		catch(Throwable brosepagehightolow)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}


	}


	public static void BrowsePageLowToHighValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{

			System.out.println("//BROWSE PAGE LOW TO HIGH VALIDATION STARTED.//");
			logMessage = "ERROR :SelectedSortValue dropdownBox is not clicked.\n";
			driver.findElement(By.xpath("//*[@class='skMob_selectedSortValue']")).click();
			System.out.println("SelectedSortValue dropdownBox is  clicked.");
			Thread.sleep(4000);


			try{
				logMessage = "ERROR:ShortBy Low To High  is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[contains(@aria-label,'Sort By')])"))).selectByIndex(8);
				driver.findElement(By.xpath("//*[@sortvalue='pricelowtohigh']")).click();
				Thread.sleep(4000);
				System.out.println("Short Low To High is  Selected .");

				//*[@class='skr_sortDone']
				logMessage = "ERROR :Apply is not clicked.\n";
				driver.findElement(By.xpath("//*[@class='skr_sortDone']")).click();
				System.out.println("Apply is  clicked.");
				Thread.sleep(8000);

			}
			catch(Throwable lowtohigh)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}
			//(//*[@itemprop='price'])[2]
			try
			{
				int sizeoflowtohighprice= driver.findElements(By.xpath("(//*[@pricetype='Reg'])")).size();
				System.out.println("Number of Low To High Price:"+sizeoflowtohighprice);

				//*[@id="skr_id_regItemsPrice_78811680018"]
				Float prev= 0.0f;
				Float next = 0.0f;


				for(int i=1;i<=sizeoflowtohighprice ; i++)
				{
					logMessage = "ERROR:Registry Item Low To High Price  is not displayed .\n";
					String registryitemlowtohigh = driver.findElement(By.xpath("(//*[@pricetype='Reg'])["+i+"]")).getText();

					//$29.99 \\+ \\. \\\ -

					registryitemlowtohigh = registryitemlowtohigh.replaceAll(" Original", "").replaceAll("\\$", "");


					prev = Float.parseFloat(registryitemlowtohigh);

					if(i != 1)
					{
						//prev.compareTo(next) < 0
						if(prev.compareTo(next) > 0)
						{
							System.out.println("Sorting order is wrong...");
							break;
						}
						else
						{
							System.out.println("Product Order is correct.. First Value : " + next +" Second Value : " + prev+" for Product : " + i);
						}
					}
					next = prev;

				}


			}
			catch(Throwable hightolow)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;	
			}
			System.out.println("//BROWSE PAGE LOW TO HIGH VALIDATION COMPLETED.//");
		}
		catch(Throwable brosepagehightolow)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}


	}


	public static void BrowsePageGridListValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//GRID LIST VALIDATION STARTED.// ");

			logMessage = "ERROR :List view icon is not clicked.\n";
			driver.findElement(By.xpath("//*[@view='listView']")).click();
			System.out.println("List view icon is  clicked.");
			Thread.sleep(2000);

			logMessage = "ERROR :Grid view icon is not clicked.\n";
			driver.findElement(By.xpath("//*[@view='gridView']")).click();
			System.out.println("Grid view icon is  clicked.");
			Thread.sleep(6000);

			System.out.println("//GRID LIST VALIDATION COMPLETED.// ");


		}
		catch(Throwable gridlist)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}


	public static void FavoriteIconValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//FAVORITE ICON PRESENT VALIDATION STARTED.//");

			int sizeoffavoriteicon = driver.findElements(By.xpath("//*[@class='skFavWrap grid']/div")).size();
			System.out.println("Number of favorite icon in browse page:"+sizeoffavoriteicon);
			if(driver.findElements(By.xpath("//*[@class='skFavWrap grid']/div")).size() != 0)
			{
				System.out.println("All Favorite Icons Are Displayed.");
			}
			else
			{
				System.out.println("All Favorite Icons Are Not Displayed.");
			}

			System.out.println("//FAVORITE ICON PRESENT VALIDATION COMPLETED.//");

		}
		catch(Throwable favoriteicon)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}


	public static void BrowsePageAddToBagPdpRandom() throws IOException {
		// TODO Auto-generated method stub
		try
		{

			System.out.println("//BROWSE PAGE:ADD PRODUCT IN RANDOM IS STARTED.//");
			for(int i  = 0; i < 2 ;i++)
			{
				int pdtSize = driver.findElements(By.xpath("//*[@class='skr_bcPdtCont']")).size();
				int rvPdt = new Random().nextInt(pdtSize);

				//*[@class='skr_bcPdtCont'][17]//*[itemprop='name']
				String browsepageprodcutname1;
				String browsepageprodcutsaleprice1=""; 
				String browsepageprodcutregprice1;


				browsepageprodcutname1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@itemprop='name']")).getText();
				if(driver.findElements(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Sale']")).size() != 0)
				{
					browsepageprodcutsaleprice1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Sale']")).getText();
				}
				browsepageprodcutregprice1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Reg']")).getText(); 

				System.out.println(browsepageprodcutname1 +"," + browsepageprodcutsaleprice1+","+browsepageprodcutregprice1);
				try
				{
					logMessage = "ERROR:Favorite Icon  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@class='skFavWrap grid']/div")).click();
					System.out.println("Favorite Icon  is  clicked .");
					Thread.sleep(4000);

					if(driver.findElements(By.xpath("//*[@class='skMob_Errors']")).size() !=0)
					{
						logMessage = "ERROR:Error Ok Button  is not clicked .\n";
						driver.findElement(By.xpath("//*[@class='skMob_ErrorsOK']")).click();
						System.out.println("Error Ok Button  is not clicked .");
					}

				}
				catch(Throwable browsepaaddtoproduct)


				{
					System.out.println(logMessage);
					logInfo(logMessage);  
				}
				try
				{
					logMessage = "ERROR:Browse Page Product  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@aria-label='buy-manage']")).click();
					System.out.println("Browse Page Product  is  clicked .");
					Thread.sleep(12000);
				}
				catch(Throwable pdpclick)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
				
				boolean flag = true;
				try{
					if(driver.findElements(By.xpath("//*[contains(@class,'skMob_Errors ')]")).size() !=0)
					{    //*[@class='skMob_Errors']
						flag = false;
						driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']")).click();
						Thread.sleep(3000);	
						BrowsePageAddToBagPdpRandom();
						
					}
					
						
				}
				catch(Throwable errormsg)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
				if(flag)
				{
				

				//*[@class='skMob_Errors']
				//*[@class='skMob_ErrorsOK']

				String browsepageprodcutName1InPopUP = driver.findElement(By.xpath("//*[@class='skr_pdtGridItemDesc']")).getText();
				String browsepageprodcutSaleprice1InPopUP = driver.findElement(By.xpath("(//*[@class='skr_addPrice sale'])[1]")).getText();
				String browsepageprodcutRegprice1InPopUP = driver.findElement(By.xpath("//*[contains(@class,'skr_orgPrice ')]")).getText();
				//(//*[contains(@class,'skr_pdpFavIcn skr_isFavItem')])[5]

				if(browsepageprodcutname1.equals(browsepageprodcutName1InPopUP))
				{
					System.out.println("Product Name  : "+browsepageprodcutname1 +" is Matched in both side.");
				}
				else
				{
					System.out.println("Product Name  is Mismatch. In BrowsePage : "+browsepageprodcutname1 +"  and In PopUpPage : " + browsepageprodcutName1InPopUP );
				}
				if(browsepageprodcutsaleprice1.equals(browsepageprodcutSaleprice1InPopUP))
				{
					System.out.println("Product Sale Price  : "+browsepageprodcutsaleprice1 +" is Matched in both side.");
				}
				else
				{
					System.out.println("Product Sale Price  is Mismatch. In BrowsePage : "+browsepageprodcutsaleprice1 +"  and In PopUpPage : " +browsepageprodcutSaleprice1InPopUP );
				}
				if(browsepageprodcutregprice1.equals(browsepageprodcutRegprice1InPopUP))
				{
					System.out.println("Product Reg Price  : "+browsepageprodcutregprice1 +" is Matched in both side.");
				}
				else
				{
					System.out.println("Product Reg Price  is Mismatch. In BrowsePage : "+browsepageprodcutregprice1 +"  and In PopUpPage : " +browsepageprodcutRegprice1InPopUP );
				}

				for(int j =0 ; j <= 1;j++)
				{
					logMessage = "ERROR:ProductQuantity 2 Times  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_registryQtyIcon skr_registryItemsPlusIcon']")).click();
					//System.out.p
				}
				Thread.sleep(3000);
				try
				{
					logMessage = "ERROR:Product Colour is not Selected .\n";
					driver.findElement(By.xpath("(//*[contains(@class,'skr_color_items ')])[1]")).click();
					Thread.sleep(3000);
				}
				catch(Throwable color)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}

				try{
					logMessage = "ERROR:Style is not Selected .\n";
					new Select(driver.findElement(By.xpath("//*[contains(@class,'skr_styleSelector')]"))).selectByIndex(1);
				}
				catch(Throwable st)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
				try
				{
					logMessage = "ERROR:Add To Bag  is not clicked .\n";
					driver.findElement(By.xpath("//*[@aria-label='add to cart']")).click();
					System.out.println("Add To Shopping Bag.");
					Thread.sleep(3000);
				}
				catch(Throwable addregis)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;	
				}

				logMessage = "ERROR:Continue Shopping is not clicked .\n";
				driver.findElement(By.xpath("(//*[contains(@class,'skr_ATBPopupBtn ')])[2]")).click();
				System.out.println("Continue Shopping is clicked.");
				Thread.sleep(8000);

				System.out.println("//BROWSE PAGE:ADD PRODUCT IN RANDOM IS COMPLETED.//");
			}
			}

		}
		catch(Throwable browsepgaddrandompdp)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}


	}

	public static void BrowsePageAddToRegPdpRandom() {
		// TODO Auto-generated method stub
		
		try
		{

			System.out.println("//BROWSE PAGE:ADD TO REGISTRY PRODUCT IN RANDOM IS STARTED.//");
			for(int i  = 0; i < 2 ;i++)
			{
				int pdtSize = driver.findElements(By.xpath("//*[@class='skr_bcPdtCont']")).size();
				int rvPdt = new Random().nextInt(pdtSize);

				//*[@class='skr_bcPdtCont'][17]//*[itemprop='name']
				String browsepageprodcutname1;
				String browsepageprodcutsaleprice1=""; 
				String browsepageprodcutregprice1;


				browsepageprodcutname1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@itemprop='name']")).getText();
				if(driver.findElements(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Sale']")).size() != 0)
				{
					browsepageprodcutsaleprice1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Sale']")).getText();
				}
				browsepageprodcutregprice1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Reg']")).getText(); 

				System.out.println(browsepageprodcutname1 +"," + browsepageprodcutsaleprice1+","+browsepageprodcutregprice1);
				try
				{
					logMessage = "ERROR:Favorite Icon  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@class='skFavWrap grid']/div")).click();
					System.out.println("Favorite Icon  is  clicked .");
					Thread.sleep(4000);

					if(driver.findElements(By.xpath("//*[@class='skMob_Errors']")).size() !=0)
					{
						logMessage = "ERROR:Error Ok Button  is not clicked .\n";
						driver.findElement(By.xpath("//*[@class='skMob_ErrorsOK']")).click();
						System.out.println("Error Ok Button  is not clicked .");
					}

				}
				catch(Throwable browsepaaddtoproduct)


				{
					System.out.println(logMessage);
					logInfo(logMessage);  
				}
				try
				{
					logMessage = "ERROR:Browse Page Product  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@aria-label='add to registry']")).click();
					System.out.println("Browse Page Product  is  clicked .");
					Thread.sleep(12000);
				}
				catch(Throwable pdpclick)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
				
				boolean flag = true;
				try{
					if(driver.findElements(By.xpath("//*[contains(@class,'skMob_Errors ')]")).size() !=0)
					{    //*[@class='skMob_Errors']
						flag = false;
						driver.findElement(By.xpath("//*[@id='skMob_ErrorsOK_id']")).click();
						Thread.sleep(3000);	
						BrowsePageAddToRegPdpRandom();
						
					}
					
						
				}
				catch(Throwable errormsg)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
				if(flag)
				{
				

				//*[@class='skMob_Errors']
				//*[@class='skMob_ErrorsOK']

				String browsepageprodcutName1InPopUP = driver.findElement(By.xpath("//*[@class='skr_pdtGridItemDesc']")).getText();
				String browsepageprodcutSaleprice1InPopUP = driver.findElement(By.xpath("(//*[@class='skr_addPrice sale'])[1]")).getText();
				String browsepageprodcutRegprice1InPopUP = driver.findElement(By.xpath("//*[contains(@class,'skr_orgPrice ')]")).getText();
				//(//*[contains(@class,'skr_pdpFavIcn skr_isFavItem')])[5]

				if(browsepageprodcutname1.equals(browsepageprodcutName1InPopUP))
				{
					System.out.println("Product Name  : "+browsepageprodcutname1 +" is Matched in both side.");
				}
				else
				{
					System.out.println("Product Name  is Mismatch. In BrowsePage : "+browsepageprodcutname1 +"  and In PopUpPage : " + browsepageprodcutName1InPopUP );
				}
				if(browsepageprodcutsaleprice1.equals(browsepageprodcutSaleprice1InPopUP))
				{
					System.out.println("Product Sale Price  : "+browsepageprodcutsaleprice1 +" is Matched in both side.");
				}
				else
				{
					System.out.println("Product Sale Price  is Mismatch. In BrowsePage : "+browsepageprodcutsaleprice1 +"  and In PopUpPage : " +browsepageprodcutSaleprice1InPopUP );
				}
				if(browsepageprodcutregprice1.equals(browsepageprodcutRegprice1InPopUP))
				{
					System.out.println("Product Reg Price  : "+browsepageprodcutregprice1 +" is Matched in both side.");
				}
				else
				{
					System.out.println("Product Reg Price  is Mismatch. In BrowsePage : "+browsepageprodcutregprice1 +"  and In PopUpPage : " +browsepageprodcutRegprice1InPopUP );
				}

				for(int j =0 ; j <= 1;j++)
				{
					logMessage = "ERROR:ProductQuantity 2 Times  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_registryQtyIcon skr_registryItemsPlusIcon']")).click();
					//System.out.p
				}
				Thread.sleep(3000);
				try
				{
					logMessage = "ERROR:Product Colour is not Selected .\n";
					driver.findElement(By.xpath("(//*[contains(@class,'skr_color_items ')])[1]")).click();
					Thread.sleep(3000);
				}
				catch(Throwable color)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}

				try{
					logMessage = "ERROR:Style is not Selected .\n";
					new Select(driver.findElement(By.xpath("//*[contains(@class,'skr_styleSelector')]"))).selectByIndex(1);
				}
				catch(Throwable st)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
				try
				{
					logMessage = "ERROR:Add To Registry  is not clicked .\n";
					driver.findElement(By.xpath("//*[@class='skr_addToRegistryButton']")).click();
					System.out.println("Add To Registry is clicked.");
					Thread.sleep(3000);
				}
				catch(Throwable addregis)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;	
				}

				logMessage = "ERROR:Continue Shopping is not clicked .\n";
				driver.findElement(By.xpath("(//*[contains(@class,'skr_ATBPopupBtn ')])[2]")).click();
				System.out.println("Continue Shopping is clicked.");
				Thread.sleep(8000);

				System.out.println("//BROWSE PAGE:ADD PRODUCT IN RANDOM IS COMPLETED.//");
			}
			}

		}
		catch(Throwable browsepgaddrandompdp)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
		
	}



}
