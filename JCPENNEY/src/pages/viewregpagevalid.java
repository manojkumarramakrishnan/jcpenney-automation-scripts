package pages;

import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.By;

import JCPpack.srcmain;

public class viewregpagevalid extends srcmain {
	
	
	
	/*public void TestMethod() throws IOException
	{
		try
		{
			ViewRegistryValidation();
		}
		catch(Throwable method)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}*/
	
	
	
	public static void ViewRegistryValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			MovedToViewRegistry();
			ViewAsGuestDisplayed();
			RegistryIDNumberValidation();	
			RegistryDetailName();
			RegistryDetailDate();
			RegistryTypeDisplayed();
			CheckZeroItemPresent();	










			/*try {
				//SHAREICON CLICKED
				logMessage = "ERROR:ViewRegistry Shareicon is not clicked .\n";
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_27_id-1']/div/div/div/div[2]/div[2]/div[2]")).click();
				Thread.sleep(12000);
				logMessage = "ERROR:ViewRegistry Shareicon Copy Button is not clicked .\n";
				driver.findElement(By.xpath("/html/body/div[6]/div[2]/div[1]/div[2]/div/div")).click();
				Thread.sleep(3000);
				//driver.findElement(By.xpath("//*[@class='skReg_copyBtn']")).click();
				String afterCopy = driver.findElement(By.xpath("//*[@class='skReg_copyBtnDiv']/div")).getText();
				System.out.println("AfterCopy Displayed Name :"+afterCopy);
				if (afterCopy.equals("COPIED!"))
				{
					System.out.println("Copied is displayed");
				} 
				else
				{
					System.out.println("Copied is  not displayed");
				}
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_27_id-1']/div/div/div/div[2]/div[2]/div[2]")).click();
			}
			catch (Throwable copy) 
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}*/

			//ShareiconPresentValidation();
		}
		catch(Throwable view)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void MovedToViewRegistry() throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		try
		{
			/*logMessage = "ERROR:ViewRegistry is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[2]")).click();*/
			System.out.println("View Registry is opened");
			Thread.sleep(20000);
			String url2 = driver.getCurrentUrl();
			if (url2.contains("viewRegistry"))
			{
				System.out.println("Successfully moved to viewRegistry page.");
			} 
			else 
			{
				System.out.println("Failed moved to viewRegistry page.");
			}
		}
		catch(Throwable movedregistry)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}


	}
	
	
	public static void ViewAsGuestDisplayed() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			logMessage = "ERROR:View As Guest is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_27_id-1']/div/div/div/div[2]/div[1]/div[1]/div[2]")).isDisplayed();
			System.out.println("View as guest  is displayed");
		}
		catch(Throwable viewgest)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void RegistryIDNumberValidation() throws IOException {
		// TODO Auto-generated method stub
		//REGISTRY ID NUMBER
		try{
			logMessage = "ERROR:ViewRegistryId Number is not displayed .\n";
			String regidnum = driver.findElement(By.xpath("//*[@id='skPageLayoutCell_27_id-1']/div/div/div/div[1]/div[2]/div[4]/div")).getText();
			System.out.println("ViewRegistery ID Number :"+regidnum);
		}
		catch(Throwable id)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}


	}
	
	
	public static void RegistryDetailName() throws IOException {
		// TODO Auto-generated method stub
		//REGISTRY DETAIL NAME
		try
		{
			logMessage = "ERROR:Registry Detail Name is not displayed .\n";
			viewregistryname = driver.findElement(By.xpath("//*[@class='skr_registryDetailName']")).getText();
			System.out.println("Registry Detail Name :"+viewregistryname);
			if(perregistryname.equals(viewregistryname))
			{
				System.out.println("Both Registry names are equal");
			}
			else
			{
				System.out.println("Both Registry names are not equal");
			}
		}
		catch(Throwable comparedetailname)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void RegistryDetailDate() throws IOException {
		// TODO Auto-generated method stub
		//REGISTRY DETAIL DATE
		try
		{
			logMessage = "ERROR:Registry Detail Date is not displayed .\n";
			viewregistrydate = driver.findElement(By.xpath("//*[@class='skr_registryDetailDate']")).getText();
			System.out.println("Registry Detail Date :"+viewregistrydate);

			if(eventdate.equals(viewregistrydate))
			{
				System.out.println("Both Registry Date are equal");
			}
			else
			{
				System.out.println("Both Registry Date are not equal");
			}
		}
		catch(Throwable comparedetaildate)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void RegistryTypeDisplayed() throws IOException {
		// TODO Auto-generated method stub
		//REGISTRY DETAIL TYPE
		try
		{
			logMessage = "ERROR:Registry Detail Type is not displayed .\n";
			viewregistrytype = driver.findElement(By.xpath("//*[@class='skr_registryDetailType']")).getText();
			System.out.println("Registry Detail Type :"+viewregistrytype);
		}
		catch(Throwable comparedetailtype)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void CheckZeroItemPresent() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			logMessage = "ERROR:Registry Item Count is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_registryItemCountInRegContainer']")).isDisplayed();
			String totalitemcount = driver.findElement(By.xpath("//*[@class='skr_registryTotalItemCount']")).getText();
			System.out.println("Total Item Count:"+totalitemcount);
			/*try
			{
				logMessage = "ERROR:Back To Top is not clicked .\n";
				driver.findElement(By.xpath("//*[@class='skr_backToTop_imgCont']")).click();
				System.out.println("Back To Top is  clicked .");
				Thread.sleep(6000);
			}
			catch(Throwable backtotop)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}*/


		}
		catch(Throwable checkzero)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}

}
