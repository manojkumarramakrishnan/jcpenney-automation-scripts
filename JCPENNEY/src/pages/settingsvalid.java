package pages;

import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.By;

import JCPpack.srcmain;

public class settingsvalid extends srcmain{
	
	
	public static void SettingsValidation() throws IOException {
		// TODO Auto-generated method stub
		try 
		{
			logMessage = "ERROR :View Registry is not clicked.\n ";
			driver.findElement(By.xpath("//*[@pagename='viewRegistry']")).click();
			System.out.println("View Registry is  clicked.");
			Thread.sleep(8000);	

			logMessage = "ERROR :Settings is not clicked.\n ";
			driver.findElement(By.xpath("//*[@class='skr_registrySettings']")).click();
			System.out.println("Settings is  clicked.");
			Thread.sleep(8000);	

			//*[@labelname='first name']
			logMessage = "ERROR :First Name is not Entered.\n ";
			driver.findElement(By.xpath("//*[@labelname='first name']")).sendKeys("autotest");
			System.out.println("First Name is Entered.");

			//*[@aria-label='fiance's first name']
			logMessage = "ERROR :Fiance First Name is not Entered.\n ";
			driver.findElement(By.xpath("//*[@id='skr_input_spousefName']")).sendKeys("autotest");
			System.out.println("Fiance First Name is Entered.");

			//*[@class='skr_isPublicCheckboxVisible']
			logMessage = "ERROR :Visible to public CheckBox is not clicked.\n ";
			driver.findElement(By.xpath("//*[@class='skr_isPublicCheckboxVisible']")).click();
			System.out.println("Visible to publick Checkbox is  clicked.");
			Thread.sleep(2000);	

			//*[@id="skPageLayoutCell_45_id-1"]/div/div/div[1]/div[1]/div[1]/div/div[4]/button[1]
			logMessage = "ERROR :Save Button is not clicked.\n ";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_45_id-1']/div/div/div[1]/div[1]/div[1]/div/div[4]/button[1]")).click();
			System.out.println("Save Button is  clicked.");
			Thread.sleep(8000);	

			/*logMessage = "ERROR :Delete Registry Button is not clicked.\n ";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_45_id-1']/div/div/div[1]/div[1]/div[1]/div/div[5]/div/div")).click();
			System.out.println("Delete Registry Button is  clicked.");
			Thread.sleep(8000);
			logMessage = "ERROR :PopUp Ok Button is not clicked.\n ";
			driver.findElement(By.xpath("//*[@class='skr_exitRegBtn']")).click();
			System.out.println("PopUp Ok Button is  clicked.");
			Thread.sleep(8000);*/

			logMessage = "ERROR :Manage Registry is clicked is not clicked.\n ";
			driver.findElement(By.xpath("//*[@pagename='viewRegistry']")).click();
			System.out.println("Manage Registry is clicked is  clicked.");
			Thread.sleep(8000);	

			String changedregistryname = driver.findElement(By.xpath("//*[@class='skr_registryDetailName']")).getText();
			System.out.println("After Changed Registry Name :"+changedregistryname);

			logMessage = "ERROR :Settings is not clicked.\n ";
			driver.findElement(By.xpath("//*[@class='skr_registrySettings']")).click();
			System.out.println("Settings is  clicked.");
			Thread.sleep(8000);

			String Settingfirstname = driver.findElement(By.xpath("//*[@id='skr_input_firstName']")).getText();
			System.out.println("After Changed Registry Name :"+Settingfirstname);

		}
		catch(Throwable settings)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}

}
