package pages;

import java.io.IOException;
import java.util.Random;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import JCPpack.srcmain;

public class inspirationgallery extends srcmain{
	
	
	public static void InspirationGallerypdpaddfavorite() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//VIEW REGISTRY PAGE:INSPIRATION GALLERY:ADD FAVORITE  IN RANDOM IS STARTED.//");

			logMessage = "ERROR:Registry page:Inspiration Gallery  is not clicked .\n";
			driver.findElement(By.xpath("(//*[@class='skr_jcpCutomCtgText1'])[1]")).click();
			System.out.println("Registry page:Inspiration Gallery  is  clicked .");
			Thread.sleep(7000);

			if(driver.findElements(By.xpath("//*[@class='skr_inspGallery_img']")).size() !=0)
			{
				System.out.println("Common Banner is displayed in inspiration gallery.");
			}
			else
			{
				System.out.println("Common Banner is  not displayed in inspiration gallery.");
			}

			for(int i  = 0; i < 2 ;i++)
			{
				int categorySize = driver.findElements(By.xpath("//*[@class='skr_pdtcatItemTitle']")).size();
				int rvcat = new Random().nextInt(categorySize);
				logMessage = "ERROR:Registry page: Category  is not clicked .\n";
				driver.findElement(By.xpath("(//*[@class='skr_pdtcatItemTitle'])["+rvcat+"]")).click();
				System.out.println("Registry page: Category  is  clicked .");
				Thread.sleep(7000);


				int pdtSize = driver.findElements(By.xpath("//*[@class='skr_pdtGridListItem']")).size();
				int rvPdt = new Random().nextInt(pdtSize);

				//*[@class='skr_bcPdtCont'][17]//*[itemprop='name']
				String registrypageprodcutname1;
				String registrypageprodcutsaleprice1=""; 
				String registrypageprodcutregprice1;


				registrypageprodcutname1 = driver.findElement(By.xpath("//*[@class='skr_pdtGridListItem']["+rvPdt+"]//*[@class='skr_pdtItemName']")).getText();
				/*if(driver.findElements(By.xpath("//*[@class='skr_pdtGridListItem']["+rvPdt+"]//*[@pricetype='Sale']")).size() != 0)
				{
					registrypageprodcutsaleprice1 = driver.findElement(By.xpath("//*[@class='skr_bcPdtCont']["+rvPdt+"]//*[@pricetype='Sale']")).getText();
				}*/
				registrypageprodcutregprice1 = driver.findElement(By.xpath("//*[@class='skr_pdtGridListItem']["+rvPdt+"]//*[@class='skr_pdtItemPrice']")).getText(); 

				System.out.println(registrypageprodcutname1 +"," + registrypageprodcutsaleprice1+","+registrypageprodcutregprice1);

				logMessage = "ERROR:Favorite Icon  is not clicked .\n";
				driver.findElement(By.xpath("//*[@class='skr_pdtGridListItem']["+rvPdt+"]//*[contains(@class,'skr_pdtFavButton')]")).click();
				System.out.println("Favorite Icon  is  clicked .");
				Thread.sleep(4000);

				logMessage = "ERROR:Inspiration Gallery:Product Add to registry is not clicked .\n";
				driver.findElement(By.xpath("//*[@class='skr_pdtGridListItem']["+rvPdt+"]//*[@class='skBrowseAddtoReg']")).click();
				System.out.println("Inspiration Gallery:Product Add to registry is  clicked .");
				Thread.sleep(7000);

				//*[@class='skMob_Errors']
				//*[@class='skMob_ErrorsOK']
				if(driver.findElements(By.xpath("//*[@class='skMob_Errors']")).size() !=0)
				{
					driver.findElement(By.xpath("//*[@class='skMob_ErrorsOK']")).click();
				}
				try{


					String registrypageprodcutName1InPopUP = driver.findElement(By.xpath("//*[@class='skr_pdtGridItemDesc']")).getText();
					String registrypageprodcutSaleprice1InPopUP = driver.findElement(By.xpath("(//*[@class='skr_addPrice sale'])[1]")).getText();
					String registrypageprodcutRegprice1InPopUP = driver.findElement(By.xpath("//*[contains(@class,'skr_orgPrice ')]")).getText();
					//(//*[contains(@class,'skr_pdpFavIcn skr_isFavItem')])[5]

					if(registrypageprodcutname1.equals(registrypageprodcutName1InPopUP))
					{
						System.out.println("Product Name  : "+registrypageprodcutname1 +" is Matched in both side.");
					}
					else
					{
						System.out.println("Product Name  is Mismatch. In BrowsePage : "+registrypageprodcutname1 +"  and In PopUpPage : " + registrypageprodcutName1InPopUP );
					}
					if(registrypageprodcutsaleprice1.equals(registrypageprodcutSaleprice1InPopUP))
					{
						System.out.println("Product Sale Price  : "+registrypageprodcutsaleprice1 +" is Matched in both side.");
					}
					else
					{
						System.out.println("Product Sale Price  is Mismatch. In BrowsePage : "+registrypageprodcutsaleprice1 +"  and In PopUpPage : " +registrypageprodcutSaleprice1InPopUP );
					}
					if(registrypageprodcutregprice1.equals(registrypageprodcutRegprice1InPopUP))
					{
						System.out.println("Product Reg Price  : "+registrypageprodcutregprice1 +" is Matched in both side.");
					}
					else
					{
						System.out.println("Product Reg Price  is Mismatch. In BrowsePage : "+registrypageprodcutregprice1 +"  and In PopUpPage : " +registrypageprodcutRegprice1InPopUP );
					}

					for(int j =0 ; j <= 1;j++)
					{
						logMessage = "ERROR:ProductQuantity 2 Times  is not clicked .\n";
						driver.findElement(By.xpath("//*[@class='skr_registryQtyIcon skr_registryItemsPlusIcon']")).click();
						//System.out.p
					}
					Thread.sleep(3000);
					try
					{
						logMessage = "ERROR:Product Colour is not Selected .\n";
						driver.findElement(By.xpath("(//*[contains(@class,'skr_color_items ')])[1]")).click();
						Thread.sleep(3000);
					}
					catch(Throwable color)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}

					try{
						logMessage = "ERROR:Style is not Selected .\n";
						new Select(driver.findElement(By.xpath("//*[contains(@class,'skr_styleSelector')]"))).selectByIndex(1);
					}
					catch(Throwable st)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}
					try
					{
						logMessage = "ERROR:Add To Bag  is not clicked .\n";
						driver.findElement(By.xpath("//*[@aria-label='add to cart']")).click();
						System.out.println("Add To Shopping Bag.");
						Thread.sleep(3000);
					}
					catch(Throwable addregis)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;	
					}

					logMessage = "ERROR:Continue Shopping is not clicked .\n";
					driver.findElement(By.xpath("(//*[contains(@class,'skr_ATBPopupBtn ')])[2]")).click();
					System.out.println("Continue Shopping is clicked.");
					Thread.sleep(8000);
				}
				catch(Throwable itemnotfound)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;	
				}
				System.out.println("//VIEW REGISTRY PAGE:INSPIRATION GALLERY:ADD FAVORITE  IN RANDOM IS COMPLETED.//");
			}	
		}
		catch(Throwable insgallpdpaddfavo)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}

}
