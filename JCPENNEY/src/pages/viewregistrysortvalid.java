package pages;

import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import JCPpack.srcmain;

public class viewregistrysortvalid extends srcmain{
	
	

	
	
	
	public static void ViewRegistrySortItemCountValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			CheckItemComparison();
			SortByLowToHighValidation();
			SortByHighToLowValidation();
		}
		catch(Throwable sortitemcount)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void CheckItemComparison() throws IOException {
		// TODO Auto-generated method stub
		try{
			logMessage = "ERROR:Registry Item count  is not displayed .\n";
			registryitemcount = driver.findElement(By.xpath("//*[@class='skr_registryTotalItemCount']")).getText();
			System.out.println("Registry Item count :"+registryitemcount);

			if(registryitemcount.equals(dashboarditemcount))
			{
				System.out.println("Both Item Count are equal");
			}
			else
			{
				System.out.println("Both Item Count are not equal");
			}
		}
		catch(Throwable itemcomparision)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void SortByLowToHighValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			logMessage = "ERROR:View All Text is not displayed .\n";
			driver.findElement(By.xpath("(//*[@class='skr_registryViewBtnTxt'])[2]")).isDisplayed();
			System.out.println("View All Text is displayed .");
			logMessage = "ERROR:View All Text is not clicked .\n";
			driver.findElement(By.xpath("(//*[@class='skr_registryViewBtnTxt'])[2]")).click();
			Thread.sleep(4000);
			System.out.println("View All Text is  clicked .");

			//*[@class='skr_bcSortLabel']

			try{
				logMessage = "ERROR:ShortBy Low To High  is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[contains(@aria-label,'Sort By')])"))).selectByIndex(8);
				new Select(driver.findElement(By.xpath("(//*[contains(@class,'skr_sortByOptions')])"))).selectByVisibleText(" Price Low to High ");
				System.out.println("ShortBy Low To High is  Selected .");

			}
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}
			//(//*[@itemprop='price'])[2]
			try
			{
				int sizeoflowtohighprice= driver.findElements(By.xpath("//*[contains(@class,'skr_registryItemsRegPrice')]")).size();
				System.out.println("Number of Low To High Price:"+sizeoflowtohighprice);

				//*[@id="skr_id_regItemsPrice_78811680018"]
				Float prev= 0.0f;
				Float next = 0.0f;


				for(int i=1;i<=sizeoflowtohighprice ; i++)
				{
					logMessage = "ERROR:Registry Item Low To High Price  is not displayed .\n";
					String registryitemlowtohigh = driver.findElement(By.xpath("(//*[contains(@class,'skr_registryItemsRegPrice')])["+i+"]")).getText();

					//$29.99 \\+ \\. \\\ -

					registryitemlowtohigh = registryitemlowtohigh.replaceAll(" Original", "").replaceAll("\\$", "");


					prev = Float.parseFloat(registryitemlowtohigh);

					if(i != 1)
					{

						if(prev.compareTo(next) < - 1)
						{
							System.out.println("Sorting order is wrong...");
							break;
						}
						else
						{
							System.out.println("Product Order is correct.. Prev : " + prev +" Current Value : " + next+" for Product : " + i);
						}
					}
					next = prev;

				}

				//*[@class='skr_bcViewBtnIcon']
				logMessage = "ERROR :Back Button is not clicked.\n ";
				driver.findElement(By.xpath("//*[@class='skr_bcViewBtnIcon']")).click();
				System.out.println("Back Button is  clicked.");
				Thread.sleep(2000);	
			}
			catch(Throwable price)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}




		}
		catch(Throwable sortby)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	
	public static void SortByHighToLowValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			try{
				logMessage = "ERROR:ShortBy High To Low  is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[contains(@aria-label,'Sort By')])"))).selectByIndex(8);
				new Select(driver.findElement(By.xpath("(//*[contains(@class,'skr_sortByOptions')])"))).selectByVisibleText(" PRICE HIGH TO LOW ");
				System.out.println("Short High To Low is  Selected .");

			}
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}
			//(//*[@itemprop='price'])[2]
			try
			{
				int sizeofhightolowprice= driver.findElements(By.xpath("//*[contains(@class,'skr_registryItemsRegPrice')]")).size();
				System.out.println("Number of High to Low Price:"+sizeofhightolowprice);

				//*[@id="skr_id_regItemsPrice_78811680018"]
				Float prev= 0.0f;
				Float next = 0.0f;


				for(int i=1;i<=sizeofhightolowprice ; i++)
				{
					logMessage = "ERROR:Registry Item High to Low Price  is not displayed .\n";
					String registryitemhightolow = driver.findElement(By.xpath("(//*[contains(@class,'skr_registryItemsRegPrice')])["+i+"]")).getText();

					//$29.99 \\+ \\. \\\ -

					registryitemhightolow = registryitemhightolow.replaceAll(" Original", "").replaceAll("\\$", "");


					prev = Float.parseFloat(registryitemhightolow);

					if(i != 1)
					{

						if(prev.compareTo(next) < - 1)
						{
							System.out.println("Sorting order is wrong...");
							break;
						}
						else
						{
							System.out.println("Product Order is correct.. Prev : " + prev +" Current Value : " + next+" for Product : " + i);
						}
					}
					next = prev;

				}

				//*[@class='skr_bcViewBtnIcon']
				logMessage = "ERROR :Back Button is not clicked.\n ";
				driver.findElement(By.xpath("//*[@class='skr_bcViewBtnIcon']")).click();
				System.out.println("Back Button is  clicked.");
				Thread.sleep(5000);	
			}
			catch(Throwable price)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

		}
		catch(Throwable hightolow)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
}
