package pages;

import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import JCPpack.srcmain;

public class dashboardvalid extends srcmain{
	
	public static void DasBoardValidation() throws IOException {
		// TODO Auto-generated method stub

		try
		{
			DashBoardEventValidation();
			DashBoardItemCountValidation();
			BrowseAndAddByCategory();
			BrowseAddByBrand();
			BrowseInspirationGallery();

		}catch(Throwable dash)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void DashBoardEventValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			logMessage = "ERROR:DashBoard Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[1]")).click();
			System.out.println("DashBoard Button is clicked .");
			Thread.sleep(6000);

			logMessage = "ERROR:Add Events Button is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-1']/div/div/div/div[1]/div[2]/div[3]/div[1]")).isDisplayed();
			System.out.println("Add Events Button is  displayed .");


			logMessage = "ERROR:Add Events Text is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-1']/div/div/div/div[1]/div[2]/div[3]/div[2]")).isDisplayed();
			System.out.println("Add Events Text is  displayed .");

			logMessage = "ERROR:Add Events Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-1']/div/div/div/div[1]/div[2]/div[3]/div[1]")).click();
			System.out.println("Add Events Button is  clicked .");
			Thread.sleep(8000);


			/*logMessage = "ERROR:Add Events Heading is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skReg_emailShareTitle']")).isDisplayed();
			System.out.println("Add Events Heading is displayed .");*/


			logMessage = "ERROR:Add Events Type Input is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skReg_addEventTypeInputCont']")).isDisplayed();
			System.out.println("Add Events Type Input is displayed .");


			logMessage = "ERROR:Add Events Name Input is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skReg_addEventName']")).isDisplayed();
			System.out.println("Add Events Name Input is displayed .");


			logMessage = "ERROR:Add Events Date Input is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skReg_addEventDateInputCont']")).isDisplayed();
			System.out.println("Add Events Data Input is displayed .");


			logMessage = "ERROR:Add Events Time Input is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skReg_addEventTime']")).isDisplayed();
			System.out.println("Add Events Time Input is displayed .");

			//*[@class='skr_cancelEvent']
			logMessage = "ERROR:Add Events :Cancel Button is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_cancelEvent']")).isDisplayed();
			System.out.println("Add Events :Cancel Button is  displayed .");

			//*[@class='skReg_addEventCreateBtn ']
			logMessage = "ERROR:Add Events :Create Button is not displayed .\n";
			driver.findElement(By.xpath("//*[contains(@class,'skReg_addEventCreateBtn')]")).isDisplayed();
			System.out.println("Add Events :Create Button is displayed .");

			logMessage = "ERROR:Add Events :Create Button is not clicked .\n";
			driver.findElement(By.xpath("//*[contains(@class,'skReg_addEventCreateBtn')]")).click();
			System.out.println("Add Events :Create Button is clicked .");
			Thread.sleep(6000);
			try
			{
				if(driver.findElements(By.xpath("//*[@class='skReg_addEventTypeErrTxt']")).size() != 0)
				{
					System.out.println("Error msg displayed.");
				}
				else
				{
					System.out.println("Error msg not displayed.");
				}
			}
			catch(Throwable error)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}


			try{
				logMessage = "ERROR:Type Of Event is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpDropDown'])[1]"))).selectByIndex(2);
				//driver.findElement(By.xpath("//*[@aria-label='type of event']")).click();
				new Select(driver.findElement(By.xpath("//*[@aria-label='type of event']"))).selectByVisibleText("CAKE TASTING");
				System.out.println("Type of event is selected. ");

			}//*[contains(@class,'skReg_addEventTypeInput')]
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}


			//*[@class='skReg_addEventNameInput']
			logMessage = "ERROR:Add Events Name Input is not Entered .\n";
			driver.findElement(By.xpath("//*[@aria-label='event name (optional)']")).sendKeys("Test");
			System.out.println("Add Events Time Input is Entered.");

			logMessage = "ERROR:Add Events Date Input is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='skReg_addEventDateCont']")).click();
			System.out.println("Add Events Date Input is clicked.");
			Thread.sleep(4000);

			//*[@id="ui-datepicker-div"]/div/a[2]/span
			logMessage = "ERROR:Add Events datepicker month arrow is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='ui-datepicker-next ui-corner-all']")).click();
			System.out.println("Add Events datepicker month arrow is clicked.");
			Thread.sleep(4000);

			//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[5]/a
			logMessage = "ERROR:Add Events datepicker date is not clicked .\n";
			driver.findElement(By.xpath("(//*[@class='ui-state-default'])[4]")).click();
			System.out.println("Add Events datepicker date is clicked.");
			Thread.sleep(4000);

			logMessage = "ERROR:Add Events Time hours  is not clicked .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[1]"))).selectByIndex(9);
			System.out.println("Add Events Time hours is clicked .");
			Thread.sleep(4000);

			//*[contains(@class,'skReg_addEventTimeMin')]
			logMessage = "ERROR:Add Events Time min  is not selected .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[2]"))).selectByIndex(6);
			System.out.println("Add Events Time min is selected .");
			Thread.sleep(4000);

			//*[contains(@class,'skReg_addEventTimePM')]
			logMessage = "ERROR:Add Events Time am/Pm  is not selected .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[3]"))).selectByIndex(1);
			System.out.println("Add Events Time am/Pm is selected .");
			Thread.sleep(4000);



			//*[@class='skReg_addEventCreateBtn ']
			logMessage = "ERROR:Add Events :Create Button is not clicked .\n";
			driver.findElement(By.xpath("//*[contains(@class,'skReg_addEventCreateBtn')]")).click();
			System.out.println("Add Events :Create Button is clicked .");
			Thread.sleep(10000);



			//ADD SECOND EVENT
			//*[@class='regSummaryEventsTxt']
			logMessage = "ERROR:Add Events :Show Event Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='regSummaryEventsTxt']")).click();
			Thread.sleep(7000);
			System.out.println("Add Events :Show Event Button is clicked .");

			//*[@class='regSummaryaddEventsCalendar']
			logMessage = "ERROR:Add Events :Add new Event Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='regMarkerAddEvents']")).click();
			System.out.println("Add Events :Add new Event Button is clicked .");
			Thread.sleep(5000);

			try{
				logMessage = "ERROR:Type Of Event is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpDropDown'])[1]"))).selectByIndex(3);
				new Select(driver.findElement(By.xpath("//*[@aria-label='type of event']"))).selectByVisibleText("ENGAGEMENT PARTY");
				System.out.println("Type of event is selected. ");
				Thread.sleep(5000);
			}//*[contains(@class,'skReg_addEventTypeInput')]
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}


			//*[@class='skReg_addEventNameInput']
			logMessage = "ERROR:Add Events Name Input is not Entered .\n";
			driver.findElement(By.xpath("//*[@aria-label='event name (optional)']")).sendKeys("Test");
			Thread.sleep(3000);
			System.out.println("Add Events Time Input is Entered.");

			logMessage = "ERROR:Add Events Date Input is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='skReg_addEventDateCont']")).click();
			System.out.println("Add Events Date Input is clicked.");
			Thread.sleep(5000);

			//*[@id="ui-datepicker-div"]/div/a[2]/span
			logMessage = "ERROR:Add Events datepicker month arrow is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='ui-datepicker-next ui-corner-all']")).click();
			System.out.println("Add Events datepicker month arrow is clicked.");
			Thread.sleep(4000);

			//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[5]/a
			logMessage = "ERROR:Add Events datepicker date is not clicked .\n";
			driver.findElement(By.xpath("(//*[@class='ui-state-default'])[11]")).click();
			System.out.println("Add Events datepicker date is clicked.");
			Thread.sleep(4000);

			logMessage = "ERROR:Add Events Time hours  is not clicked .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[1]"))).selectByIndex(5);
			System.out.println("Add Events Time hours is clicked .");
			Thread.sleep(4000);

			//*[contains(@class,'skReg_addEventTimeMin')]
			logMessage = "ERROR:Add Events Time min  is not selected .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[2]"))).selectByIndex(3);
			System.out.println("Add Events Time min is selected .");
			Thread.sleep(4000);

			//*[contains(@class,'skReg_addEventTimePM')]
			logMessage = "ERROR:Add Events Time am/Pm  is not selected .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[3]"))).selectByIndex(1);
			System.out.println("Add Events Time am/Pm is selected .");
			Thread.sleep(4000);



			//*[@class='skReg_addEventCreateBtn ']
			logMessage = "ERROR:Add Events :Create Button is not clicked .\n";
			driver.findElement(By.xpath("//*[contains(@class,'skReg_addEventCreateBtn')]")).click();
			System.out.println("Add Events :Create Button is clicked .");
			Thread.sleep(10000);

			//THIRDADDEVENTS

			logMessage = "ERROR:Add Events :Hide Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='regMarkerCloseTxt']")).click();
			System.out.println("Add Events :Hide Button is clicked .");
			Thread.sleep(3000);

			logMessage = "ERROR:Add Events :Show Event Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='regSummaryEventsTxt']")).click();
			Thread.sleep(5000);
			System.out.println("Add Events :Show Event Button is clicked .");


			//*[@class='regSummaryaddEventsCalendar']
			logMessage = "ERROR:Add Events :Add new Event Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='regMarkerAddEvents']")).click();
			Thread.sleep(6000);
			System.out.println("Add Events :Add new Event Button is clicked .");

			try{
				logMessage = "ERROR:Type Of Event is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpDropDown'])[1]"))).selectByIndex(4);
				new Select(driver.findElement(By.xpath("//*[@aria-label='type of event']"))).selectByVisibleText("ENGAGEMENT PHOTOS");
				Thread.sleep(5000);
				System.out.println("Type of event is selected. ");
			}//*[contains(@class,'skReg_addEventTypeInput')]
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}


			//*[@class='skReg_addEventNameInput']
			logMessage = "ERROR:Add Events Name Input is not Entered .\n";
			driver.findElement(By.xpath("//*[@aria-label='event name (optional)']")).sendKeys("Test");
			Thread.sleep(3000);
			System.out.println("Add Events Time Input is Entered.");

			logMessage = "ERROR:Add Events Date Input is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='skReg_addEventDateCont']")).click();
			System.out.println("Add Events Date Input is clicked.");
			Thread.sleep(5000);

			//*[@id="ui-datepicker-div"]/div/a[2]/span
			logMessage = "ERROR:Add Events datepicker month arrow is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='ui-datepicker-next ui-corner-all']")).click();
			System.out.println("Add Events datepicker month arrow is clicked.");
			Thread.sleep(4000);

			//*[@id="ui-datepicker-div"]/table/tbody/tr[3]/td[5]/a
			logMessage = "ERROR:Add Events datepicker date is not clicked .\n";
			driver.findElement(By.xpath("(//*[@class='ui-state-default'])[17]")).click();
			System.out.println("Add Events datepicker date is clicked.");
			Thread.sleep(4000);

			logMessage = "ERROR:Add Events Time hours  is not clicked .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[1]"))).selectByIndex(6);
			System.out.println("Add Events Time hours is clicked .");
			Thread.sleep(4000);

			//*[contains(@class,'skReg_addEventTimeMin')]
			logMessage = "ERROR:Add Events Time min  is not selected .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[2]"))).selectByIndex(5);
			System.out.println("Add Events Time min is selected .");
			Thread.sleep(4000);

			//*[contains(@class,'skReg_addEventTimePM')]
			logMessage = "ERROR:Add Events Time am/Pm  is not selected .\n";
			new Select(driver.findElement(By.xpath("(//*[@class='skReg_jcpaddTimeActingContainer'])[3]"))).selectByIndex(1);
			System.out.println("Add Events Time am/Pm is selected .");
			Thread.sleep(4000);



			//*[@class='skReg_addEventCreateBtn ']
			logMessage = "ERROR:Add Events :Create Button is not clicked .\n";
			driver.findElement(By.xpath("//*[contains(@class,'skReg_addEventCreateBtn')]")).click();
			Thread.sleep(6000);
			System.out.println("Add Events :Create Button is clicked .");

			//*[@class='regMarkerCloseTxt']
			logMessage = "ERROR:Add Events :Hide Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='regMarkerCloseTxt']")).click();
			Thread.sleep(6000);
			System.out.println("Add Events :Hide Button is clicked .");

			//*[@class='regSummaryEventsTxt']
			logMessage = "ERROR:Add Events :Show Events Button is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='regSummaryEventsTxt']")).isDisplayed();
			System.out.println("Add Events :Show Events Button is displayed .");

			logMessage = "ERROR:Number Of Registry Item is not displayed .\n";
			String registryitem =driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-1']/div/div/div/div[1]/div[3]/div[1]/div[1]")).getText();
			System.out.println("Number of add registry item:"+registryitem);

			logMessage = "ERROR:Number Of Item Purchased is not displayed .\n";
			String itempurchased =driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-1']/div/div/div/div[1]/div[3]/div[2]/div[1]")).getText();
			System.out.println("Number of item purchased:"+itempurchased);

			logMessage = "ERROR:Registry fullfilled is not displayed .\n";
			String regfullfilled =driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-1']/div/div/div/div[1]/div[3]/div[3]/div[1]/span[1]")).getText();
			System.out.println("Number of registry fullfilled:"+regfullfilled);

		}
		catch(Throwable dashevent)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	public static void DashBoardItemCountValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			logMessage = "ERROR:DashBoard Item count  is not displayed .\n";
			dashboarditemcount = driver.findElement(By.xpath("//*[@class='regItemsCount']")).getText();
			System.out.println("Dashboard Item count :"+dashboarditemcount);

			if(registryitemcount.equals(dashboarditemcount))
			{
				System.out.println("Both Item Count are equal");
			}
			else
			{
				System.out.println("Both Item Count are not equal");
			}
		}
		catch(Throwable Dashboarditemcount)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void BrowseAndAddByCategory() throws IOException {
		// TODO Auto-generated method stub

		try
		{
			//PRODUCT IMAGE PRESENT VALIDATION
			int sizeofcategoryimages = driver.findElements(By.xpath("//*[@id='skPageLayoutCell_24_id-2']/div/div/div//*[@id='skr_catGridContWrapper_id']/div/div")).size();
			System.out.println("Number of category images: " + sizeofcategoryimages );
			for(int i = 1; i <= sizeofcategoryimages ; i++)
			{     
				try
				{
					if(driver.findElements(By.xpath("//*[@id='skPageLayoutCell_24_id-2']/div/div/div//*[@id='skr_catGridContWrapper_id']/div/div["+ i +"]/img")).size() != 0)
					{
						System.out.println("Add By Category Image "+i+" are displayed");
					}
					else
					{
						System.out.println("Add By Category Image "+i+" are  not displayed");
					}
				}
				catch(Throwable category)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
				try
				{
					logMessage = "ERROR: category"+i+" is not clicked .\n";
					driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-2']/div/div/div//*[@id='skr_catGridContWrapper_id']/div/div["+ i +"]/img")).click();
					System.out.println("Category"+i+" is clicked");
					Thread.sleep(6000);
				}
				catch(Throwable categ)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
				try
				{


					int sizeofproductimages = driver.findElements(By.xpath("//*[@id='skPageLayoutCell_44_id-1']/div/div/div/div[2]/div/div")).size();
					System.out.println("Number of category images "+i+" : " + sizeofproductimages );
					for(int j = 1; j <= sizeofproductimages ; j++)
					{       
						if(driver.findElements(By.xpath("//*[@id='skPageLayoutCell_44_id-1']/div/div/div/div[2]/div/div["+j+"]/div[1]/img")).size() != 0)
						{
							try
							{
								System.out.println("Add By  Category "+i+"( Product Image) "+j+" are displayed");
								logMessage = "ERROR:Product"+j+" is not clicked .\n";
								driver.findElement(By.xpath("//*[@id='skPageLayoutCell_44_id-1']/div/div/div/div[2]/div/div["+j+"]/div[1]/img")).click();
								System.out.println("Product"+i+" is clicked");
								Thread.sleep(6000);


							}
							catch(Throwable productimage)
							{
								System.out.println(logMessage);
								logInfo(logMessage);;
							}

							try
							{
								int sizeofproductdetailimages = driver.findElements(By.xpath("//*[@class='skr_bcPdtImgCont']")).size();
								System.out.println("Number of productdetail images "+j+" : " + sizeofproductdetailimages );
								for(int k = 1; k <= sizeofproductdetailimages ; k++)
								{       
									if(driver.findElements(By.xpath("(//*[@class='skr_bcPdtImgCont'])["+k+"]")).size() != 0)
									{
										System.out.println("Add By  Category"+i+" ( Product Image) "+j+"(Productdetail image) "+k+" are displayed");
										logMessage = "ERRORDashBoard Button is not clicked .\n";
										driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[1]")).click();
										Thread.sleep(6000);

									}
									else
									{
										System.out.println("Add By  Category"+i+" ( Product Image) "+j+"(Productdetail image) "+k+" are not displayed");
									}



								}
							}
							catch(Throwable productdetails)
							{
								System.out.println(logMessage);
								logInfo(logMessage);;
							}
						}

						else
						{
						}
					}
				}
				catch(Throwable product)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}



			}
		}catch(Throwable browse)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	
	public static void BrowseAddByBrand() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			logMessage = "ERROR:DashBoard Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[1]")).click();
			Thread.sleep(6000);
			logMessage = "ERROR:Add By Brands View all Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skr_catGridName_id']/div[2]")).click();
			Thread.sleep(6000);
			int sizeofbrandimages = driver.findElements(By.xpath("//*[@id='skPageLayoutCell_31_id-2']/div/div/div/div[2]/div/div")).size();
			System.out.println("Number of brand images: " + sizeofbrandimages );
			for(int i = 1; i <= sizeofbrandimages ; i++)
			{       
				if(driver.findElements(By.xpath("//*[@id='skPageLayoutCell_31_id-2']/div/div/div/div[2]/div/div["+i+"]/div[1]/img")).size() != 0)
				{
					System.out.println("Add By Brand Image "+i+" are displayed");


					logMessage = "ERROR: Add BY Brand Category Image "+i+" is not clicked .\n";
					driver.findElement(By.xpath("//*[@id='skPageLayoutCell_31_id-2']/div/div/div/div[2]/div/div["+i+"]/div[1]/img")).click();
					System.out.println(" Add BY Brand Category Image "+i+" is  clicked");
					Thread.sleep(6000);
					try
					{
						int sizeofbrandproductimages = driver.findElements(By.xpath("//*[@class='skr_bcPdtImgCont']")).size();
						System.out.println("Number of category images "+i+" : " + sizeofbrandproductimages );

						/*//*[@id="skr_id_colorWrapper_pp5003250340"]/div[6]/img

						int sizeOFColors = driver.findElements(By.xpath("//*[@class='skr_color_item']")).size();
						int randomColor = new Random().nextInt(sizeOFColors) + 1;

						driver.findElement(By.xpath("(//*[@class='skr_color_item'])["+randomColor+"]")).click();*/
						for(int j = 1; j <= sizeofbrandproductimages ; j++)
						{       
							if(driver.findElements(By.xpath("(//*[@class='skr_bcPdtImgCont'])["+j+"]")).size() != 0)
							{
								System.out.println("Add By  Brand "+i+"( Product Image) "+j+" are displayed");
								logMessage = "ERRORDashBoard Button is not clicked .\n";
								driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[1]")).click();
								Thread.sleep(6000);
							}
							else
							{
								System.out.println("Add By  Brand "+i+"( Product Image) "+j+" are not displayed");
							}

						}

					}
					catch(Throwable browsebrand)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}
				}
			}
		}
		catch(Throwable brand)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void BrowseInspirationGallery() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			int sizeofgalleryimages = driver.findElements(By.xpath("//*[@id='skPageLayoutCell_24_id-4']/div/div/div//*[@id='skr_catGridContWrapper_id']/div/div")).size();
			System.out.println("Number of gallery images: " + sizeofgalleryimages );
			for(int i = 1; i <= sizeofgalleryimages ; i++)
			{       
				if(driver.findElements(By.xpath("//*[@id='skPageLayoutCell_24_id-4']/div/div/div//*[@id='skr_catGridContWrapper_id']/div/div["+i+"]/img")).size() != 0)
				{
					System.out.println("Add By gallery Image "+i+"  are displayed");
					logMessage = "ERROR:Inspiration gallery Image "+i+" is not clicked .\n";
					driver.findElement(By.xpath("//*[@id='skPageLayoutCell_24_id-4']/div/div/div//*[@id='skr_catGridContWrapper_id']/div/div["+i+"]/img")).click();
					Thread.sleep(6000);
					int sizeofgallerycategory = driver.findElements(By.xpath("//*[@class='skr_pdtcatItemTitle']")).size();
					System.out.println("Number of gallery category:"+sizeofgallerycategory);
					for(int j=1 ;j<=sizeofgallerycategory ; j++)
					{
						driver.findElement(By.xpath("(//*[@class='skr_pdtcatItemTitle'])["+j+"]")).click();
						Thread.sleep(6000);

						int sizeofgallerysubcategory = driver.findElements(By.xpath("//*[@class='skr_pdtGridListItem']")).size();
						System.out.println("Number of gallery subcategory:"+sizeofgallerysubcategory);
						for(int k=1 ;k<=sizeofgallerycategory ; k++)
						{
							if(driver.findElements(By.xpath("(//*[@class='skr_pdtGridListItem'])["+k+"]")).size() != 0)
							{
								System.out.println("Gallary Product Images "+k+" are displayed");
							}
							else
							{
								System.out.println("Gallary Product Images "+k+" are not displayed");
							}
						}
					}
				}
				else
				{
					System.out.println("Add By gallery Image "+i+"  are  not displayed");
				}

			}
			logMessage = "ERRORDashBoard Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[1]")).click();
			Thread.sleep(6000);
			logMessage = "ERROR View Registry Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='id_scfHeader']/div/div[2]/div/div[2]")).click();
			Thread.sleep(6000);
		}
		catch(Throwable gallery)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
}
