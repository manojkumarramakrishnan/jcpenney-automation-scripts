package pages;

import java.io.IOException;

import org.junit.Test;
import org.openqa.selenium.By;

import JCPpack.srcmain;

public class infomationpage extends srcmain {
	
	
	public static void Informationpgvalidation() throws IOException
	{
		try
		{
			EventInformation();
			ShippingInformationPageValidation();
			PersonalizeRegistryValidation();
		}
		catch(Throwable information)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	public static void EventInformation() throws IOException {
		// TODO Auto-generated method stub
		


		try
		{
			FirstSecondNamesTextBoxPresent();
			DatePickerCase843();
			EventFirstNameValidation();
			EventLastNameValidation();
			//EventFirstLastNameComparison();
			FianceFirstNameValidation();
			FianceLastNameValidation();
			EmailMeValidation();
			ErrorMsgEntryValidation();


		

		System.out.println("//EVENT INFORMATION VALIDATION IS COMPLETED.//");
		}
		catch(Throwable eventinformation)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void ShippingInformationPageValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			ShippingPagemoved();
			ShippingPageFieldDisplayed();
			StreetAddressValidation();
			FieldAutoFilledValidation();
			PhoneNumberLimitValidation();
			AptTextBoxValidation();
			RemainderDateValidation();
			ShippingBackNextValidation();
		}
		catch(Throwable shipping)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	public static void PersonalizeRegistryValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			BesideSpecialCharFieldNameValidation();
		}catch(Throwable personalize)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;

		}
	}
	
	public static void FirstSecondNamesTextBoxPresent() throws IOException {
		try {
			// TODO Auto-generated method stub
			// case843
			System.out.println("VerifyReleventInfo is started to validate");
			logMessage = "ERROR :Date TextBox not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_eventDate']")).isDisplayed();
			logMessage = "ERROR :Event Location is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_eventlocation']")).isDisplayed();
			logMessage = "ERROR :FirstName is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_firstName']")).isDisplayed();
			logMessage = "ERROR :LastName is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_lastName']")).isDisplayed();
			logMessage = "ERROR :SpuseName is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_spousefName']")).isDisplayed();
			logMessage = "ERROR :Last SpouseName is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_spouselName']")).isDisplayed();
			System.out.println("VerifyReleventInfo is completed");
			// driver.navigate().back();
		} catch (Throwable tr)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	public static void DatePickerCase843() throws IOException {
		// TODO Auto-generated method stub

		try {
			// case134 backmonthvalidation
			System.out.println("Date picker is started to validate");
			logMessage = "ERROR : Date Calender icon is not clicked.\n ";
			driver.findElement(By.xpath("//*[@id='skr_input_eventDate']")).click();
			Thread.sleep(2000);
			logMessage = "ERROR : DateBackArrow icon is not Blocked.\n ";
			driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div/a[1]/span")).click();
			Thread.sleep(2000);
			if (driver.findElements(By.xpath("//*[@id='skr_labeleventDate']/div[2]/div[1]")).size() != 0) 
			{
				System.out.println("DatePicker Error msg is displayed");
			} 
			else 
			{
				System.out.println("DatePicker Error msg is not displayed");
			}
			// case844

			// Manual Entry
			try {
				logMessage = "ERROR : Manual Entry is Working.\n ";
				driver.findElement(By.xpath("//*[@id='skr_input_eventDate']")).sendKeys("07-14-2016");
				System.out.println("Mannul entry is not accecpt");
			} catch (Throwable tr1)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

			// Selected date Should be appear
			try {
				logMessage = "ERROR : Input for data event is not select the date from the date picker.\n ";
				driver.findElement(By.xpath("//*[@id='skr_input_eventDate']")).click();
				System.out.println("Input for data event is  select the date from the date picker.");
				Thread.sleep(3000);


				logMessage = "ERROR : In DatePicker Arrow Mark is not clicked.\n ";
				driver.findElement(By.xpath("//*[@class='ui-datepicker-next ui-corner-all']")).click();
				System.out.println(" In DatePicker Arrow Mark is  clicked.");
				Thread.sleep(3000);

				logMessage = "ERROR : Input for data event is not select the date from the date picker.\n ";
				String selectedDate = driver.findElement(By.xpath("(//*[@data-handler='selectDay'])[28]")).getText();
				{
					if (driver.findElements(By.xpath("(//*[@data-handler='selectDay'])[28]")).size() != 0)
					{
						System.out.println("Current Date Highlights in calender is displayed");
					}
					else 
					{
						System.out.println("Current Date Highlights in calender is not displayed");
					}
					driver.findElement(By.xpath("(//*[@data-handler='selectDay'])[28]")).click();
					Thread.sleep(3000);

					String selectedValueInTextBox = driver.findElement(By.xpath("//*[@id='skr_input_eventDate']")).getAttribute("min");
					if (selectedDate.equals(selectedValueInTextBox))
					{
						System.out.println("Both date are equals");
					}
					else 
					{
						System.out.println("Both date arenot equals");
						logMessage = "ERROR : Date is mismatch.\n ";
					}

				}
				eventdate = driver.findElement(By.xpath("//*[@id='skr_input_eventDate']")).getText();
				System.out.println("Event Date :"+eventdate);
			} catch (Throwable tr1) 
			{
				logInfo(logMessage);;
			}

			System.out.println("Date picker validation completed");
		} catch (Throwable tr)
		{
			logInfo(logMessage);;
		}
	}

	
	
	public static void EventFirstNameValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out.println("FirstNameValidation is started to validate");
			String[] firstname = { "test1", "  ", "@#$","test skava", "skava" };
			for (int i = 0; i < firstname.length; i++)
			{
				String input = firstname[i];
				System.out.println("First Name : " + input);
				try {
					logMessage = "ERROR : ERROR icon is not displayed. Firstname : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_firstName']")).clear();
					driver.findElement(By.xpath("//*[@id='skr_input_firstName']")).sendKeys(input);
					driver.findElement(By.xpath("//*[@id='skr_input_firstName']")).click();
					Thread.sleep(4000);
					driver.findElement(By.xpath("//*[@id='skPageLayoutCell_37_id-1']/div/div/div/div/div[3]/span[1]")).isDisplayed();
				} catch (Throwable tr) 
				{
					logInfo(logMessage);;
				}
			}
			System.out.println("FirstNameValidation is completed");

		} catch (Throwable fnv) {
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	public static void EventLastNameValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out.println("LastNameValidation is started to validate");
			String[] lastname = { "test2", "  ", "@#$","test skava", "test" };
			for (int i = 0; i < lastname.length; i++) {
				String input = lastname[i];
				System.out.println("Last Name : " + input);
				try {
					logMessage = "ERROR : ERROR icon is not displayed. Lastname : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_lastName']")).clear();
					driver.findElement(By.xpath("//*[@id='skr_input_lastName']")).sendKeys(input);
					driver.findElement(By.xpath("//*[@id='skr_input_lastName']")).click();
					Thread.sleep(4000);
					//driver.findElement(By.xpath("//*[@id='skPageLayoutCell_37_id-1']/div/div/div/div/div[3]/span[1]")).isDisplayed();
				} catch (Throwable tr)
				{
					logInfo(logMessage);;
				}
			}
			System.out.println("LastNameValidation is completed");

		} catch (Throwable lnv) {
			System.out.println(logMessage);
			logInfo(logMessage);;
		}


	}
	
	public static void EventFirstLastNameComparison() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			eventfirstname = driver.findElement(By.xpath("//*[@id='skr_input_firstName']")).getText();
			System.out.println("Event First Name :"+eventfirstname);

			eventlastname = driver.findElement(By.xpath("//*[@id='skr_input_lastName']")).getText();
			System.out.println("Event Last Name :"+eventlastname);

			if(eventfirstname.equals(myaccountfirstname))
			{
				System.out.println("Both FirstNames are equal");
			}
			else
			{
				System.out.println("Both FirstNames are not equal");
			}


			if(eventlastname.equals(myaccountlastname))
			{
				System.out.println("Both LastNames are equal.");
			}
			else
			{
				System.out.println("Both LastNames are not equal.");
			}
		}
		catch(Throwable eventfirstlastcomparison)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void FianceFirstNameValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out.println("FianceFirstNameValidation is started to validate");
			String[] fiancefirstname = { "test3", "  ", "@#$","test skava", "test" };
			for (int i = 0; i < fiancefirstname.length; i++) {
				String input = fiancefirstname[i];
				System.out.println("Fiance First Name : " + input);
				try {
					logMessage = "ERROR : ERROR icon is not displayed. Fiancefirstname : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_spousefName']")).clear();
					driver.findElement(By.xpath("//*[@id='skr_input_spousefName']")).sendKeys(input);
					driver.findElement(By.xpath("//*[@id='skr_input_spousefName']")).click();
					Thread.sleep(4000);
					//driver.findElement(By.xpath("//*[@id='skPageLayoutCell_37_id-1']/div/div/div/div/div[3]/span[1]")).isDisplayed();
				} catch (Throwable tr)
				{
					logInfo(logMessage);;
				}
			}
			System.out.println("FiancefirstnameValidation is completed");

		} catch (Throwable ffnv) {
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void FianceLastNameValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out.println("FianceLastNameValidation is started to validate");
			String[] fiancelastname = { "test4", "  ", "@#$","test skava", "test" };
			for (int i = 0; i < fiancelastname.length; i++) {
				String input = fiancelastname[i];
				System.out.println("Fiance Last Name : " + input);
				try {
					logMessage = "ERROR : ERROR icon is not displayed. Fiancelastname : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_spouselName']")).clear();
					driver.findElement(By.xpath("//*[@id='skr_input_spouselName']")).sendKeys(input);
					driver.findElement(By.xpath("//*[@id='skr_input_spouselName']")).click();
					Thread.sleep(4000);
					//driver.findElement(By.xpath("//*[@id='skPageLayoutCell_37_id-1']/div/div/div/div/div[3]/span[1]")).isDisplayed();
				} catch (Throwable tr)
				{
					logInfo(logMessage);;
				}
			}
			System.out.println("FiancelastnameValidation is completed");


		} catch (Throwable flnv) {
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	
	public static void EmailMeValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			logMessage = "ERROR :EmailMe text is not displayed.\n ";
			driver.findElement(By.xpath("//*[@class='skr_isEmailTxt']")).isDisplayed();
			System.out.println("EmailMe text is  displayed.");

			logMessage = "ERROR :EmailMe CheckBox is not displayed.\n ";
			driver.findElement(By.xpath("//*[@class='skr_isEmailCheckboxVisible']")).isDisplayed();
			System.out.println("EmailMe CheckBox is  displayed.");

			logMessage = "ERROR :EmailMe CheckBox is not clicked.\n ";
			driver.findElement(By.xpath("//*[@class='skr_isEmailCheckboxVisible']")).click();
			System.out.println("EmailMe CheckBox is  clicked.");
		}
		catch(Throwable emailme)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	public static void ErrorMsgEntryValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			if (driver.findElements(By.xpath("//*[@class ='jcp_skr_inputErrCon']")).size() != 0)
			{
				System.out.println("Alert displayed successfully.");

			}
			else 
			{
				System.out.println("Alert not displayed.");
			}
			logMessage = "ERROR : Next Button is not clicked.\n ";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[1]/div/div[4]/div[2]")).click();
			System.out.println("Next Button is  clicked.");
			Thread.sleep(8000);
		}
		catch(Throwable error)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	public static void ShippingPagemoved() throws IOException {
		// TODO Auto-generated method stub
		try

		{
			// case147
			System.out.println("ShippingPagemoved is started to validate");
			logMessage = "ERROR :Shipping Information page is  not moved when NEXT is clicked with valid entries.\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[2]/div/div[2]/div[2]")).isDisplayed();
			System.out.println("Shipping Information page is moved.");
			logMessage = "ERROR : Next Button is not clicked.\n ";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[1]/div/div[4]/div[2]")).click();
			Thread.sleep(8000);
			if (driver.findElements(By.xpath("//*[@class ='jcp_skr_inputErrCon']")).size() != 0)
			{
				System.out.println("Alert displayed successfully.");
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[1]/div/div[4]/div[2]")).click();
			}
			else 
			{
				System.out.println("Alert not displayed.");
			}
			System.out.println("ShippingPagemoved is completed");
		} catch (Throwable spv)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	public static void ShippingPageFieldDisplayed() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out
			.println("ShippingPageFieldDisplayed is started to validate");
			logMessage = "ERROR Event information step should  not be ticked at right panel as the indication of completed step.\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[2]/div/div[1]/div[1]")).isDisplayed();

			logMessage = "ERROR Input Address  is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_addr']")).isDisplayed();
			logMessage = "ERROR Input Apt  is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_apt']")).isDisplayed();
			logMessage = "ERROR Input City  is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_city']")).isDisplayed();
			logMessage = "ERROR Input State   is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_state']")).isDisplayed();
			logMessage = "ERROR Input Zipcode  is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_zipcode']")).isDisplayed();
			logMessage = "ERROR Input Phone  is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_phone']")).isDisplayed();
			System.out.println("ShippingPageFieldDisplayed is completed");
		} catch (Throwable sfd)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	public static void StreetAddressValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out
			.println("StreetAddressValidation is started to validate");
			driver.findElement(By.xpath("//*[@id='skr_input_addr']")).sendKeys(
					"119 town hall");
			String checkGoogleOverlay = driver.findElement(
					By.xpath("//html/body/div[7]")).getAttribute("style");
			if (checkGoogleOverlay.contains("display: none;")) {
				System.out.println("Google Overlay is not present");
			} else {
				System.out.println("Google Overlay is present");
			}
			System.out.println("StreetAddressValidation is completeed");
		} catch (Throwable sav) {
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	public static void FieldAutoFilledValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out
			.println("FieldAutoFilledValidation is started to validate");
			logMessage = "ERROR CityName is not entered automatically.\n";
			String cityname = driver.findElement(By.xpath("//*[@id='skr_input_city']")).getText();
			System.out.println("City Name : " + cityname);
			logMessage = "ERROR StateName is not entered automatically.\n";
			String statename = driver.findElement(By.xpath("//*[@id='skr_input_state']")).getText();
			System.out.println("State Name : " + statename);
			logMessage = "ERROR ZipcodeName is not entered automatically.\n";
			String zipcodename = driver.findElement(By.xpath("//*[@id='skr_input_zipcode']")).getText();
			System.out.println("Zipcode Name : " + zipcodename);
			System.out.println("FieldAutoFilledValidation is completed");
		} catch (Throwable fafv)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	public static void PhoneNumberLimitValidation() throws IOException 
	{
		// TODO Auto-generated method stub
		try 
		{
			System.out.println("PhoneNumberLimitValidation is started to validate");
			String[] phonenumber = { "gfderg", "!@#$%", "78945612307","7894561230" };
			for (int i = 0; i < phonenumber.length; i++)
			{
				String input = phonenumber[i];
				System.out.println("PhoneNumber : " + input);
				try 
				{
					logMessage = "ERROR : ERROR icon is not displayed. PhoneNumber : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_phone']")).clear();
					driver.findElement(By.xpath("//*[@id='skr_input_phone']")).sendKeys(input);
					driver.findElement(By.xpath("//*[@id='skr_input_phone']")).click();
					Thread.sleep(4000);
					// driver.findElement(By.xpath("//*[@id='skPageLayoutCell_37_id-1']/div/div/div/div/div[3]/span[1]")).isDisplayed();
				} catch (Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
			}
			System.out.println("PhoneNumberLimitValidation is completed");
		} catch (Throwable plv)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	public static void AptTextBoxValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out.println("AptTextBoxValidation is started to validate");
			logMessage = "ERROR Inputapt is not entered .\n";
			driver.findElement(By.xpath("//*[@id='skr_input_apt']")).sendKeys("dfgt6785");
			if (driver.findElements(By.xpath("//*[@class ='jcp_skr_inputErrCon']")).size() != 0)
			{
				System.out.println("Alert displayed successfully.");

			}
			else 
			{
				System.out.println("Alert not displayed.");
			}
			System.out.println("AptTextBoxValidation is completed");
		} catch (Throwable apt)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	public static void RemainderDateValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//REMAINDER DATE VALIDATION IS STARTED.//");


			logMessage = "ERROR Remainder CheckBox is not displayed .\n";
			driver.findElement(By.xpath("	//*[@class ='skr_isRemainderCheckboxVisible']")).isDisplayed();
			System.out.println("Remainder CheckBox  is displayed.");


			logMessage = "ERROR Remainder text is not displayed .\n";
			driver.findElement(By.xpath("//*[@class ='skr_isremainder_date']")).isDisplayed();
			System.out.println("Remainder text  is displayed.");

			logMessage = "ERROR Remainder CheckBox is not clicked .\n";
			driver.findElement(By.xpath("	//*[@class ='skr_isRemainderCheckboxVisible']")).click();
			System.out.println("Remainder CheckBox  is clicked.");
			Thread.sleep(5000);

			logMessage = "ERROR Next Button  is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='skr_regNextButton")).click();
			System.out.println("Next Button is clicked.");
			Thread.sleep(2000);

			if(driver.findElements(By.xpath("(//*[@class ='jcp_skr_inputErrCon skr_desktopDate'])[2]")).size()!= 0)
			{
				System.out.println("Error msg is displayed.");
			}
			else
			{
				System.out.println("Error msg is not displayed.");
			}
			logMessage = "ERROR Remainder Date is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skr_input_remainderDate']")).isDisplayed();
			System.out.println("Remainder Date  is displayed.");

			logMessage = "ERROR Remainder Date is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skr_input_remainderDate']")).click();
			System.out.println("Remainder Date  is clicked.");
			Thread.sleep(3000);

			logMessage = "ERROR Remainder Date arrow is not clicked .\n";
			driver.findElement(By.xpath("//*[@title='Next']")).click();
			System.out.println("Remainder Date arrow is clicked.");
			Thread.sleep(2000);

			logMessage = "ERROR Remainder particular Date  is not clicked .\n";
			driver.findElement(By.xpath("(//*[@class='ui-state-default'])[30]")).click();
			System.out.println("Remainder  particular Date is clicked.");
			Thread.sleep(2000);

			/*logMessage = "ERROR Next Button  is not clicked .\n";
				driver.findElement(By.xpath("//*[@class='skr_regNextButton")).click();
				System.out.println("Next Button is clicked.");
				Thread.sleep(2000);*/

			System.out.println("//REMAINDER DATE VALIDATION IS COMPLETED.//");
		}
		catch(Throwable remainderdate)
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}

	}
	
	
	public static void ShippingBackNextValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out.println("ShippingBackNextValidation is started to validate");
			logMessage = "ERROR Back Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[1]/div/div[4]/div[1]")).click();
			System.out.println("Back Button is clicked.");
			Thread.sleep(4000);
			logMessage = "ERROR Input eventDate is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_eventDate']")).isDisplayed();
			System.out.println("Input eventDate is  displayed.");
			Thread.sleep(4000);
			logMessage = "ERROR Next Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[1]/div/div[4]/div[2]")).click();
			System.out.println("Next Button is  clicked.");
			Thread.sleep(4000);
			logMessage = "ERROR  InputCity is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='skr_input_city']")).isDisplayed();
			System.out.println("InputCity is  displayed.");
			logMessage = "ERROR Next Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[1]/div/div[4]/div[2]")).click();
			System.out.println("Next Button is  clicked .");
			Thread.sleep(4000);
			try
			{
				logMessage = "ERROR Personalize Registry Tick Mark is not shown.\n";
				driver.findElement(By.xpath("//*[@class='skr_regTitleBullet']")).isDisplayed();
				System.out.println("ShippingBackNextValidation is completed");
			}catch(Throwable tickmark)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}

		} catch (Throwable sbnv) 
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}
	
	
	public static void BesideSpecialCharFieldNameValidation() throws IOException {
		// TODO Auto-generated method stub
		try {
			System.out.println("BesideSpecialCharFieldNameValidation is started to validate");
			String[] requiredfields = { "*autotest", "autotest*", "  ", "@#$%","pennytest" };
			for (int i = 0; i < requiredfields.length; i++) 
			{
				String input = requiredfields[i];
				System.out.println("RequiredFields : " + input);
				try {
					logMessage = "ERROR : ERROR icon is not displayed. requiredfields : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_regName']")).clear();
					logMessage = "ERROR Input RegName is not entered .\n";
					driver.findElement(By.xpath("//*[@id='skr_input_regName']")).sendKeys(input);
					Thread.sleep(4000);
					perregistryname =driver.findElement(By.xpath("//*[@id='skr_input_regName']")).getText();
					System.out.println("Personalize Registry Name :"+perregistryname);
				} catch (Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);;
				}
			}
			try
			{
				System.out.println("BesideSpecialCharFieldNameValidation is completed");
				System.out.println("GreetingTextValidation is started.");
				logMessage = "ERROR Input_MsgToGuest  paragraph is not entered .\n";
				String[] greetingtext = { "*autotest", "autotest*", "  ", "@#$%","autotest\ntestmodule\n" };
				for (int i = 0; i < greetingtext.length; i++) 
				{
					String input = greetingtext[i];
					System.out.println("GreetingText : " + input);
					try {
						logMessage = "ERROR : ERROR icon is not displayed. greetingtext : "+ input + ".\n";
						driver.findElement(By.xpath("//*[@id='skr_input_msgToGuest']")).clear();
						logMessage = "ERROR Input RegName is not entered .\n";
						driver.findElement(By.xpath("//*[@id='skr_input_msgToGuest']")).sendKeys(input);
						Thread.sleep(4000);
					} catch (Throwable tr)
					{
						System.out.println(logMessage);
						logInfo(logMessage);;
					}
				}
			}
			catch(Throwable greetig)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;
			}
			// VerifyUploadImage();

			logMessage = "ERROR Create Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[1]/div/div[4]/div[2]")).click();
			System.out.println("Created my account");
			Thread.sleep(25000);
			/*try{
				logMessage = "ERROR the input fields are filled with tick mark if the input values are not passed..\n";
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_10_id-1']/div/div/div[1]/div[1]/div[2]/div/div[3]/div[1]")).isDisplayed();
				Thread.sleep(3000);
			}
			catch(Throwable tickmark)
			{
				System.out.println(logMessage);
				logInfo(logMessage);;	
			}*/

		} catch (Throwable bscv) 
		{
			System.out.println(logMessage);
			logInfo(logMessage);;
		}
	}


}
