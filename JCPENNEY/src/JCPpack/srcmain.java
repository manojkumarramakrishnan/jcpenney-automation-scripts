package JCPpack;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class srcmain {
	
	public static WebDriver driver = null;
	public static String LogFile = "E:\\Logs\\JCPPennyTestCasesERRORLOG.log";
	public static FileOutputStream fileWrite = null;
	public static String eventdate ="";
	public static String perregistryname ="";
	public static String dashboarditemcount ="";
	public static String registryitemcount="";
	public static String viewregistryname ="";
	public static String viewregistrydate ="";
	public static String viewregistrytype ="";
	public static String eventfirstname = "";
	public static String eventlastname ="";
	public static String myaccountfirstname ="";
	public static String myaccountlastname ="";
	public static String foundregname ="";
	public static String foundregdate ="";
	public static String foundregnumber ="";
	public static String logMessage = "";
	public static int rvPdt;
	public static SimpleDateFormat sdf =  new SimpleDateFormat("DD/mm/YYYY hh:MM:ss");
	public static void  logInfo(String stLogMessage)
	{
		try
		{	
			String currentTime  = sdf.format(new Date());
			stLogMessage = currentTime + " " +stLogMessage;  
			fileWrite.write(stLogMessage.getBytes());
			
		}catch(Throwable tr)
		{
			
		}
	}
	public static void scriptStart()
	{

		try {

			fileWrite = new FileOutputStream(LogFile);

			System.setProperty("webdriver.chrome.driver","E:\\Jars\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			//driver = new FirefoxDriver();
			driver.get("http://dt-test4.jcpenney.com/index.jsp?dpAkamaiOverride=1");
			Thread.sleep(10000);
			driver.get("http://dt-test4.jcpenney.com/jsp/giftregistry/templates/landingV2.jsp?containerId=JCP|pg4005400007&cmJCP_T=M2&cmJCP_C=Feature&eventRootCatId=cat100630110&eventType=W&grView=public");
			
			System.out.println("Jcp page opened.");
			Thread.sleep(10000);

		} catch (Throwable tr) {
			System.out.println("ERROR ;" + tr.toString());
		}
	}
	

}
