package JCPpack;

import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.browsepagevalid;
import pages.clickandredirectvalid;
import pages.dashboardvalid;
import pages.favoritepgpdpcheckvalid;
import pages.infomationpage;
import pages.inspirationgallery;
import pages.settingsvalid;
import pages.tipsfaqsvalid;
import pages.viewregistryaddprod;
import pages.viewregistryproductdetails;
import pages.viewregistrysortvalid;
import pages.viewregpagevalid;

public  class createregistry extends srcmain {
	
	@Before()
	public void BeforeTest()
	{
		scriptStart();
	}
	
	
	@Test()
	public void MethodTest() throws IOException
	{
		try
		{
			CreateRegistryValidation();
			infomationpage.Informationpgvalidation();
			viewregpagevalid.ViewRegistryValidation();
			viewregistryaddprod.ViewRegistryAddProductValidation();
			viewregistrysortvalid.ViewRegistrySortItemCountValidation();
			viewregistryproductdetails.ViewRegistryProductDetails();
			inspirationgallery.InspirationGallerypdpaddfavorite();
			browsepagevalid.BrowsePageValidation();
			favoritepgpdpcheckvalid.FavoritePgPdpCheck();
			dashboardvalid.DasBoardValidation();
			settingsvalid.SettingsValidation();
			clickandredirectvalid.ClickAndRedirectionValidation();
			tipsfaqsvalid.TipsFaqValitation();
		}
		catch(Throwable method)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
	}
	
	
	
	public static void CreateRegistryValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{

			logMessage = "ERROR :Create a Registry is not clicked.\n";
			driver.findElement(By.xpath("//*[@title='Create a registry']")).click();
			System.out.println("Create a Registry is  clicked.");
			Thread.sleep(7000);

			CreateRegistryPresentValidation();
			
			CreateMyAccount();
			/*FirstNameValidation();
			LastNameValidation();
			CreateEmailAddressValidation();
			CreatePasswordValidation();
			CreateConfirmPasswordValidation();*/
			
			//CreateRistry.test();

		}
		catch(Throwable createregistry)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}
	
	
	public static void CreateRegistryPresentValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//CREATE REGISTRY PRESENT VALIDATION IS STARTED.//");

			System.out.println("Create My Account  page is opened.");

			logMessage = "ERROR :JCPenney homepage is not displayed.\n";
			driver.findElement(By.xpath("//*[@title='go to JCPenney homepage']")).isDisplayed();
			System.out.println("JCPenney homepage is  displayed.");

			logMessage = "ERROR :Search textBox is not displayed.\n";
			driver.findElement(By.xpath("//*[@name='searchTerm']")).isDisplayed();
			System.out.println("Search textBox is  displayed.");

			logMessage = "ERROR :Search Icon is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='searchbutton']")).isDisplayed();
			System.out.println("Search Icon is  displayed.");

			logMessage = "ERROR :Wedding Registry and Gift Image  is not displayed.\n";
			driver.findElement(By.xpath("//*[@class='gift_header flt_lft gr_landing_slot_s1']")).isDisplayed();
			System.out.println("Wedding Registry and Gift Image is  displayed.");

			logMessage = "ERROR :CreateRegistryHeading is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryHeading']")).isDisplayed();
			System.out.println("CreateRegistryHeading is  displayed.");

			logMessage = "ERROR :FirstName is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='firstName']")).isDisplayed();
			System.out.println("FirstName is  displayed.");

			logMessage = "ERROR :LastName is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='lastName']")).isDisplayed();
			System.out.println("LastName is  displayed.");

			logMessage = "ERROR :EmailAddr is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='emailAddr']")).isDisplayed();
			System.out.println("EmailAddr is  displayed.");

			logMessage = "ERROR :CreatePwd is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='createPwd']")).isDisplayed();
			System.out.println("CreatePwd is  displayed.");

			logMessage = "ERROR :Show_inst is not displayed.\n";
			driver.findElement(By.xpath("//*[@class='show_inst']")).isDisplayed();
			System.out.println("Show_inst is  displayed.");

			logMessage = "ERROR :ConfirmPwd is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='confirmPwd']")).isDisplayed();
			System.out.println("ConfirmPwd is  displayed.");

			logMessage = "ERROR :CreateRegistryButton is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryButton']")).isDisplayed();
			System.out.println("CreateRegistryButton is  displayed.");

			logMessage = "ERROR :Alreaady have create my account Text is not displayed.\n";
			driver.findElement(By.xpath("//*[@title='create registry']")).isDisplayed();
			System.out.println("Alreaady have create my account Text is  displayed.");

			logMessage = "ERROR :SignUp is not displayed.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryForm']/fieldset/div[5]/a/strong")).isDisplayed();
			System.out.println("SignUp is  displayed.");

			System.out.println("//CREATE REGISTRY PRESENT VALIDATION IS COMPLETED.//");
		}
		catch(Throwable createregistrypresent)
		{
			System.out.println(logMessage);
			logInfo(logMessage);	
		}

	}
	
	
	
	public static void CreateMyAccount() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//CREATE MY ACCOUNT IS STARTED.//");
			
			//driver.findElement(By.xpath("//*[@id='firstName']")).sendKeys("bivi");
			logMessage = "ERROR First Name is not entered .\n";
			myaccountfirstname = "fritestingjcp";
			driver.findElement(By.xpath("//*[@id='firstName']")).sendKeys(myaccountfirstname);
			System.out.println("First Name is  entered:"+myaccountfirstname);
			Thread.sleep(2000);


			logMessage = "ERROR Last Name is not entered .\n";
			//driver.findElement(By.xpath("//*[@id='lastName']")).sendKeys("achu");
			myaccountlastname = "fritestjcp";
			driver.findElement(By.xpath("//*[@id='lastName']")).sendKeys(myaccountlastname);
			System.out.println("Last Name is  entered:"+myaccountlastname);
			Thread.sleep(2000);


			logMessage = "ERROR Email Address is not entered .\n";
			driver.findElement(By.xpath("//*[@id='emailAddr']")).sendKeys("fritestingjcpenney@skava.com");
			System.out.println("Email Address is  entered .");
			Thread.sleep(5000);

			logMessage = "ERROR Password is not entered .\n";
			driver.findElement(By.xpath("//*[@id='createPwd']")).sendKeys("Skava@1234");
			System.out.println("Password is  entered .");
			Thread.sleep(6000);

			logMessage = "ERROR Confirm Password is not entered .\n";
			driver.findElement(By.xpath("//*[@id='confirmPwd']")).sendKeys("Skava@1234");
			System.out.println(" Confirm Password is  entered .");
			Thread.sleep(5000);


			logMessage = "ERROR CreateRegistryButton is not clicked .\n";
			driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
			System.out.println(" CreateRegistryButton is  clicked .");
			Thread.sleep(20000);

			System.out.println("//CREATE MY ACCOUNT IS COMPLETED.//");
		}
		catch(Throwable createaccount)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}
	
	
	
	public static void FirstNameValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//FIRST NAME VALIDATION STARTED.//");

			logMessage = "ERROR :Create Account Button is not clicked.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
			System.out.println("Create Account Button is  clicked.");
			Thread.sleep(4000);



			logMessage = "ERROR :Error msg is not displayed.\n";
			driver.findElement(By.xpath("//*[@class='dynamic_error_msgs mrgb10 hide_display']")).isDisplayed();
			System.out.println("Common Error msg is  displayed.");

			String []listOffirstNamesForCheck = {"autotest1","23424243","!@#$%^","tests@123"};

			for(int i =0; i < listOffirstNamesForCheck.length;i++)
			{
				String input = listOffirstNamesForCheck[i];
				System.out.println("First Name : " + input);
				try
				{
					logMessage = "ERROR :First Name is not Entered.\n";
					driver.findElement(By.xpath("//*[@id='firstName']")).clear();
					driver.findElement(By.xpath("//*[@id='firstName']")).sendKeys(input);
					System.out.println("First Name is  Entered");

					logMessage = "ERROR :Create Account Button is not clicked.\n";
					driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
					System.out.println("Create Account Button is  clicked.");
					Thread.sleep(4000);

					logMessage = "ERROR :Error msg is not displayed.\n";
					if(driver.findElements(By.xpath("//*[@id='errorHolder']")).size() != 0)
					{
						String errorMsg = driver.findElement(By.xpath("//*[@id='errorHolder']")).getText();

						if(errorMsg.contains("enter a valid first name"))
						{
							System.out.println("Error messages shown correctly for firstname");
						}else
						{
							System.out.println("Error messages not displayed for firstname.");
						}

						System.out.println("Common Error msg is  displayed Firstname.");
					}else
					{
						System.out.println("Common Error messages is  not displayed for firstname.");
					}
				}
				catch(Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);
				}


			}

			logMessage = "ERROR :Valid First Name is not Entered.\n";
			driver.findElement(By.xpath("//*[@id='firstName']")).clear();
			driver.findElement(By.xpath("//*[@id='firstName']")).sendKeys("autotest");
			System.out.println("First Name is  Entered");

			logMessage = "ERROR :Create Account Button is not clicked.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
			System.out.println("Create Account Button is  clicked.");
			Thread.sleep(4000);

			logMessage = "ERROR :Error msg is not displayed.\n";
			if(driver.findElements(By.xpath("//*[@id='errorHolder']")).size() != 0)
			{
				String errorMsg = driver.findElement(By.xpath("//*[@id='errorHolder']")).getText();

				if(errorMsg.contains("enter a valid first name"))
				{
					System.out.println("Error messages displayed wrongly for  Valid firstname");
				}else
				{
					System.out.println("Error messages not displayed for Valid firstname.");
				}

				System.out.println("Common Error msg is  displayed for valid firstname.");
			}else
			{
				System.out.println("common Error messages not displayed for  valid firstname.");
			}


			System.out.println("FIRST NAME VALIDATION COMPLETED.");
		}
		catch(Throwable firstname)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}
	
	
	public static void LastNameValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//LAST NAME VALIDATION STARTED.//");
			String []listOflastNamesForCheck = {"autotest2","23424243","!@#$%^","tests@123"};

			for(int i =0; i < listOflastNamesForCheck.length;i++)
			{
				String input = listOflastNamesForCheck[i];
				System.out.println("Last Name : " + input);
				try
				{
					logMessage = "ERROR :Last Name is not Entered.\n";
					driver.findElement(By.xpath("//*[@id='lastName']")).clear();
					driver.findElement(By.xpath("//*[@id='lastName']")).sendKeys(input);
					System.out.println("Last Name is  Entered");

					logMessage = "ERROR :Create Account Button is not clicked.\n";
					driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
					System.out.println("Create Account Button is  clicked.");
					Thread.sleep(4000);

					logMessage = "ERROR :Error msg is not displayed.\n";
					if(driver.findElements(By.xpath("//*[@id='errorHolder']")).size() != 0)
					{
						String errorMsg = driver.findElement(By.xpath("//*[@id='errorHolder']")).getText();

						if(errorMsg.contains("enter a valid last name."))
						{
							System.out.println("Error messages shown correctly for Lastname");
						}else
						{
							System.out.println("Error messages not displayed for Lastname.");
						}

						System.out.println("Common Error msg is  displayed Lastname.");
					}else
					{
						System.out.println("Common Error messages is  not displayed for Lastname.");
					}
				}
				catch(Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);
				}


			}

			logMessage = "ERROR :Valid Last Name is not Entered.\n";
			driver.findElement(By.xpath("//*[@id='lastName']")).clear();
			driver.findElement(By.xpath("//*[@id='lastName']")).sendKeys("manualtest");
			System.out.println("Last Name is  Entered");

			logMessage = "ERROR :Create Account Button is not clicked.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
			System.out.println("Create Account Button is  clicked.");
			Thread.sleep(4000);

			logMessage = "ERROR :Error msg is not displayed.\n";
			if(driver.findElements(By.xpath("//*[@id='errorHolder']")).size() != 0)
			{
				String errorMsg = driver.findElement(By.xpath("//*[@id='errorHolder']")).getText();

				if(errorMsg.contains("enter a valid last name."))
				{
					System.out.println("Error messages displayed wrongly for  Valid lastname");
				}else
				{
					System.out.println("Error messages not displayed for Valid lastname.");
				}

				System.out.println("Common Error msg is  displayed for valid lastname.");
			}else
			{
				System.out.println("common Error messages not displayed for  valid lastname.");
			}


			System.out.println("//LAST NAME VALIDATION COMPLETED.//");
		}
		catch(Throwable secondname)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}
	
	
	
	public static void CreateEmailAddressValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//CREATE EMAIL ADDRESS VALIDATION IS STARTED.//");
			String[] createemailIDs = { "@skavatest@gmail.com", "skavatest","123@gmail.com", "skavatestgmail", "skavatest@gm@ail","s1234@gmail.com" };

			for (int i = 0; i < createemailIDs.length; i++)
			{
				String input = createemailIDs[i];
				System.out.println("Email ID : " + input);
				try {
					logMessage = "ERROR :ErrorIcon is not displayed.CreateEmailID : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@name='cr_email_addr']")).clear();
					driver.findElement(By.xpath("//*[@name='cr_email_addr']")).sendKeys(input);
					driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
					Thread.sleep(5000);

					logMessage = "ERROR :Error msg is not displayed.\n";
					if(driver.findElements(By.xpath("//*[@id='errorContainer']")).size() != 0)
					{
						String errorMsg = driver.findElement(By.xpath("//*[@id='errorContainer']")).getText();

						if(errorMsg.contains("enter a valid  create email address"))
						{
							System.out.println("Error messages shows correctly for create email address.");
						}else
						{
							System.out.println("Error messages not displayed for create email address.");
						}

						System.out.println("Common Error msg is  displayed for create email address.");
					}else
					{
						System.out.println("common Error messages not displayed for create email address.");
					}


				} catch (Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);
				}
			}


			logMessage = "ERROR :ValidCreate Email Address is not Entered.\n";
			driver.findElement(By.xpath("//*[@name='cr_email_addr']")).clear();
			driver.findElement(By.xpath("//*[@name='cr_email_addr']")).sendKeys("mailtojcp@gmail.com");
			System.out.println("Valid CreateEmail Address is  Entered");

			logMessage = "ERROR :CreateRegistry Button is not clicked.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
			System.out.println("CreateRegistry Button is  clicked.");
			Thread.sleep(4000);

			logMessage = "ERROR :Error msg is not displayed for create emailaddress.\n";
			if(driver.findElements(By.xpath("//*[@id='errorContainer']")).size() != 0)
			{
				String errorMsg = driver.findElement(By.xpath("//*[@id='errorContainer']")).getText();

				if(errorMsg.contains("enter a valid email address"))
				{
					System.out.println("Error messages displayed wrongly for create  email address");
				}else
				{
					System.out.println("Error messages not displayed for Valid  create Email Address.");
				}

				System.out.println("Common Error msg is  displayed for valid create email address.");
			}else
			{
				System.out.println("common Error messages not displayed for valid  create email address.");
			}

			System.out.println("//CREATE EMAIL ADDRESS VALIDATION IS COMPLETED.//");
		}
		catch(Throwable createemailaddress)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}

	
	public static void CreatePasswordValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//CREATE PASSWORD VALIDATION IS STARTED.//");
			String[] createpassword = { "skava@123","jcpenny","@@#$%","485694","skava@#" };

			for (int i = 0; i < createpassword.length; i++)
			{
				String input = createpassword[i];
				System.out.println("Create Password : " + input);
				try {
					logMessage = "ERROR :ErrorIcon is not displayed.Create Password : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@name='cr_crt_pwd']")).clear();
					driver.findElement(By.xpath("//*[@name='cr_crt_pwd']")).sendKeys(input);
					driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
					Thread.sleep(5000);

					logMessage = "ERROR :Error msg is not displayed.\n";
					if(driver.findElements(By.xpath("//*[@id='errorContainer']")).size() != 0)
					{
						String errorMsg = driver.findElement(By.xpath("//*[@id='errorContainer']")).getText();

						if(errorMsg.contains("enter a valid create password"))
						{
							System.out.println("Error messages shows correctly for create password.");
						}else
						{
							System.out.println("Error messages not displayed for create password.");
						}

						System.out.println("Common Error msg is  displayed for create password.");
					}else
					{
						System.out.println("common Error messages not displayed for create password.");
					}


				} catch (Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);
				}
			}


			logMessage = "ERROR :Valid create Password is not Entered.\n";
			driver.findElement(By.xpath("//*[@name='cr_crt_pwd']")).clear();
			driver.findElement(By.xpath("//*[@name='cr_crt_pwd']")).sendKeys("Skava@1234");
			System.out.println("Valid create Password is  Entered");

			logMessage = "ERROR :Create Registry Button is not clicked.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
			System.out.println("Create Registry Button is  clicked.");
			Thread.sleep(9000);

			logMessage = "ERROR :Error msg is not displayed for password.\n";
			if(driver.findElements(By.xpath("//*[@id='errorContainer']")).size() != 0)
			{
				String errorMsg = driver.findElement(By.xpath("//*[@id='errorContainer']")).getText();

				if(errorMsg.contains("enter a valid create password"))
				{
					System.out.println("Error messages displayed wrongly for  Valid create password");
				}else
				{
					System.out.println("Error messages not displayed for Valid create password.");
				}

				System.out.println("Common Error msg is  displayed for valid  create password.");
			}else
			{
				System.out.println("common Error messages not displayed for  valid create password.");
			}

			System.out.println("//CREATE PASSWORD VALIDATION IS COMPLETED.//");
		}
		catch(Throwable createpassword)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}
	
	
	public static void CreateConfirmPasswordValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//CREATE CONFIRM PASSWORD VALIDATION IS STARTED.//");
			String[] createconfirmpassword = { "skava@123","jcpenny","@@#$%","485694","skava@#" };

			for (int i = 0; i < createconfirmpassword.length; i++)
			{
				String input = createconfirmpassword[i];
				System.out.println("Create  Confirm Password : " + input);
				try {
					logMessage = "ERROR :ErrorIcon is not displayed.Create Confirm Password : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='confirmPwd']")).clear();
					driver.findElement(By.xpath("//*[@id='confirmPwd']")).sendKeys(input);
					driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
					Thread.sleep(9000);

					logMessage = "ERROR :Error msg is not displayed.\n";
					if(driver.findElements(By.xpath("//*[@id='errorContainer']")).size() != 0)
					{
						String errorMsg = driver.findElement(By.xpath("//*[@id='errorContainer']")).getText();

						if(errorMsg.contains("enter a valid create confirm password"))
						{
							System.out.println("Error messages shows correctly for create confirm password.");
						}else
						{
							System.out.println("Error messages not displayed for create confirm password.");
						}

						System.out.println("Common Error msg is  displayed for create confirm password.");
					}else
					{
						System.out.println("common Error messages not displayed for create confirm password.");
					}


				} catch (Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);
				}
			}


			logMessage = "ERROR :Valid create confirm Password is not Entered.\n";
			driver.findElement(By.xpath("//*[@id='confirmPwd']")).clear();
			driver.findElement(By.xpath("//*[@id='confirmPwd']")).sendKeys("Skava@1234");
			System.out.println("Valid create Confirm Password is  Entered");

			logMessage = "ERROR :Create Registry Button is not clicked.\n";
			driver.findElement(By.xpath("//*[@id='createRegistryButton']")).click();
			System.out.println("Create Registry Button is  clicked.");
			Thread.sleep(10000);

			logMessage = "ERROR :Error msg is not displayed for Create Confirm password.\n";
			if(driver.findElements(By.xpath("//*[@id='errorContainer']")).size() != 0)
			{
				String errorMsg = driver.findElement(By.xpath("//*[@id='errorContainer']")).getText();

				if(errorMsg.contains("enter a valid create confirm password"))
				{
					System.out.println("Error messages displayed wrongly for  Valid create confirm password");
				}else
				{
					System.out.println("Error messages not displayed for Valid create confirm password.");
				}

				System.out.println("Common Error msg is  displayed for valid  create confirm password.");
			}else
			{
				System.out.println("common Error messages not displayed for  valid create confirm password.");
			}

			System.out.println("//CREATE CONFIRM PASSWORD VALIDATION IS COMPLETED.//");
		}
		catch(Throwable createconfirmpassword)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}
	
	
	


}
