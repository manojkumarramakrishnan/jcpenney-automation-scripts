package JCPpack;

import guestpage.findregfooterlinkvalid;
import guestpage.findregiconpresent;
import guestpage.findreglinkvalid;
import guestpage.findregnamedatepresent;
import guestpage.findregnameregnamedatecompare;
import guestpage.findregnumbervalid;
import guestpage.findregtextboxvalid;
import guestpage.findregviewregaddbagrandom;
import guestpage.findregviewregbrandAZ;
import guestpage.findregviewregbrandZA;
import guestpage.findregviewregcategoryAZ;
import guestpage.findregviewregcategoryZA;
import guestpage.findregviewreghightolow;
import guestpage.findregviewregitemcountvalid;
import guestpage.findregviewreglistvalid;
import guestpage.findregviewreglowtohigh;
import guestpage.headermovevalidation;

import java.io.IOException;
import java.util.Random;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import pages.browsepagevalid;
import pages.inspirationgallery;
import pages.tipsfaqsvalid;
import JCPpack.srcmain;

public class viewasguest extends srcmain {
	@Before
	public void beforeTest()
	{
		scriptStart();
	}
	@Test()
	public void TestMethod() throws IOException
	{
	
		try
		{
			ViewAsGuest();
			/*findregiconpresent.FindRegistryPageIconPresent();
			findreglinkvalid.FindRegistryPageLinkValidation();
			findregfooterlinkvalid.FindRegistryPageFooterLinkValidation();
			findregtextboxvalid.FindRegistryEventDetailTextBoxValidation();*/
			findregnumbervalid.FindRegistryRegNumberValidation();
			findregnamedatepresent.FoundRegistryNameDatePresent();
			findregnameregnamedatecompare.FindRegistryViewRegistryPgValidation();
			findregviewreglistvalid.GridListValidation();
			findregviewreglowtohigh.FounderRegistrySortByLowToHigh();
			findregviewreghightolow.FounderRegistrySortByHighToLow();
			findregviewregcategoryAZ.FindRegViewRegSortByCategoryAZ();
			findregviewregcategoryZA.FindRegViewRegSortByCategoryZA();
			findregviewregbrandAZ.FindRegViewRegSortByBrandAZ();
			findregviewregbrandZA.FindRegViewRegSortByBrandZA();
			findregviewregitemcountvalid.ItemscountInRegistry();
			findregviewregaddbagrandom.AddToBagInRandom();
			inspirationgallery.InspirationGallerypdpaddfavorite();
			browsepagevalid.BrowsePageValidation();
			headermovevalidation.HeaderMoveValidation();
			tipsfaqsvalid.TipsFaqValitation();
			
		}
		catch(Throwable method)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
	}
	
	
	public static void ViewAsGuest() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			
			logMessage = "ERROR:Give a gift:banner is not displayed .\n";
			driver.findElement(By.xpath("//*[@title='Give a gift']")).isDisplayed();
			System.out.println("Give a gift:banner is  displayed .");
			
			
			logMessage = "ERROR:Give a gift:banner is not clicked .\n";
			driver.findElement(By.xpath("//*[@title='Give a gift']")).click();
			System.out.println("Give a gift:banner is  clicked .");
			Thread.sleep(7000);
			
			
			

		}
		catch(Throwable viewasguest)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}
	
	
	
	
	


}
