package JCPpack;

import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.browsepagevalid;
import pages.clickandredirectvalid;
import pages.dashboardvalid;
import pages.inspirationgallery;
import pages.settingsvalid;
import pages.tipsfaqsvalid;
import pages.viewregistryaddprod;
import pages.viewregistryproductdetails;
import pages.viewregistrysortvalid;
import pages.viewregpagevalid;
import JCPpack.srcmain;

public class manageregistry  extends srcmain{
	
	@Before()
	public void BeforeTest()
	{
		scriptStart();

	}
	
	@Test()
	public void TestMethod() throws IOException
	{
		try
		{
			ManageRegistrySignInPageValidation();
			viewregpagevalid.ViewRegistryValidation();
			viewregistryaddprod.ViewRegistryAddProductValidation();
			viewregistrysortvalid.ViewRegistrySortItemCountValidation();
			viewregistryproductdetails.ViewRegistryProductDetails();
			inspirationgallery.InspirationGallerypdpaddfavorite();
			browsepagevalid.BrowsePageValidation();
			dashboardvalid.DasBoardValidation();
			settingsvalid.SettingsValidation();
			clickandredirectvalid.ClickAndRedirectionValidation();
			tipsfaqsvalid.TipsFaqValitation();
		}
		catch(Throwable manage)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
	}
	
	
	public static void ManageRegistrySignInPageValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
					
			System.out.println("//MANAGE REGISTRY SIGNIN PAGE VALIDATION IS STARTED.// ");

			logMessage = "ERROR :Manage Registry is not clicked.\n";
			driver.findElement(By.xpath("//*[@title='Manage a registry']")).click();
			System.out.println("Manage Registry is  clicked..");
			Thread.sleep(7000);

			//SignInPagePresentValidation();
			//SignInPageEmailPassValidation();

			logMessage = "ERROR :Email  is not entered.\n";
			driver.findElement(By.xpath("//*[@id='emailidLogin']")).sendKeys("saran@gmail.com");
			System.out.println("Email is  entered..");
			Thread.sleep(2000);

			logMessage = "ERROR :Password  is not entered.\n";
			driver.findElement(By.xpath("//*[@id='mypasswdLogin']")).sendKeys("Skava@1234");
			System.out.println("Password is  entered..");
			Thread.sleep(2000);

			logMessage = "ERROR :SignIn Button is not clicked.\n";
			driver.findElement(By.xpath("//*[@class='signin_button']")).click();
			System.out.println("SignIn Button is  clicked..");
			Thread.sleep(10000);

			System.out.println("//MANAGE REGISTRY SIGNIN PAGE VALIDATION IS COMPLETED.// ");
		}
		catch(Throwable signin)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}

}
