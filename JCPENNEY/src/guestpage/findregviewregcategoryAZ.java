package guestpage;

import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregviewregcategoryAZ extends srcmain{
	
	public static void FindRegViewRegSortByCategoryAZ()
	{
		try
		{
			System.out.println("//SORT BY CATEGORY A-Z VALIDATION STARTED.//");
			
			logMessage = "ERROR: SortBy DropDown Box Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@aria-label='Sort By']")).click();
			System.out.println("SortBy DropDown Box Is  clicked.");
			Thread.sleep(3000);
			
			
			logMessage = "ERROR: SortBy DropDown Box:Category A-Z Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@sortlabel='Category A - Z']")).click();
			System.out.println("SortBy DropDown Box:Category A-Z Is  clicked.");
			Thread.sleep(3000);
			
			
			logMessage = "ERROR: SortBy DropDown Box:Category A-Z:Apply Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@class='skr_sortDone']")).click();
			System.out.println("SortBy DropDown Box:Category A-Z:Apply Is  clicked.");
			Thread.sleep(3000);
			
			
			/*int sizeofcategoryatoz =driver.findElements(By.xpath("//*[contains(@class,'skr_registryTitleCont')]")).size();
			System.out.println("Number of Category A-Z:"+sizeofcategoryatoz);*/
			
			ArrayList<String> StoredAllCategoryList = new ArrayList<String>(); 
			for( int i=0 ; ; i++)
			{
				try
				{
					String sortbycategoryatozname = driver.findElement(By.xpath("//*[@id='id_skr_registryItemsDeptCont_"+i+"']/div/div[1]/div[1]/div")).getText();
					StoredAllCategoryList.add(sortbycategoryatozname);				
					System.out.println("Sort By Category A-Z Name:"+sortbycategoryatozname);
				}
				catch(Throwable tr)
				{
					break;
				}
			}
			 ArrayList<String> UnsortedList = StoredAllCategoryList;
			 
			 Collections.sort(StoredAllCategoryList); 
			 boolean SortFlag = true;
			 for(int i = 0; i < StoredAllCategoryList.size();i++)
			 {
				 if(!UnsortedList.get(i).equals(StoredAllCategoryList.get(i)))
				 {
					 System.out.println("ERROR :Category Sorting is Wrong By A-Z BY Index : " + i );
					 SortFlag = false;
					 break;
				 }
			 }
			if(SortFlag)
			{
				System.out.println("PASS :Category Sorting A-Z is Correct.");
			}
			
			
			System.out.println("//SORT BY CATEGORY A-Z VALIDATION COMPLETED.//");
		}
		catch(Throwable sortbycategoryAZ)
		{
			System.out.println(logMessage);
			logInfo(logMessage);	
		}
		
	}

}
