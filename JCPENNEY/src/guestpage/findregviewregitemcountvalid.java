package guestpage;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregviewregitemcountvalid extends srcmain {
	
	public static void ItemscountInRegistry() {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//ITEM COUNT INREGISTRY PAGE VALIDATION STARTED.//");
			
			logMessage = "Item Count In Registry Page is not displayed.\n";
			driver.findElement(By.xpath("(//*[contains(@class,'skr_registryItemCountInRegContainer')])")).isDisplayed();
			System.out.println("Item Count In Registry Page is  displayed.");
			
			String itemsinregistrypage =driver.findElement(By.xpath("(//*[contains(@class,'skr_registryItemCountInRegContainer')])")).getText();
			System.out.println("Items count in registry page:"+itemsinregistrypage);
			
			System.out.println("//ITEM COUNT INREGISTRY PAGE VALIDATION COMPLETED.//");
			
		}
		catch(Throwable itemsinregistry)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
	}

}
