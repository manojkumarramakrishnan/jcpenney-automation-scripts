package guestpage;

import java.io.IOException;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregnameregnamedatecompare extends srcmain{
	public static void FindRegistryViewRegistryPgValidation() throws IOException 
	{
		// TODO Auto-generated method stub
		try
		{
			String viewregname = driver.findElement(By.xpath("//*[@class='skr_registryDetailName']")).getText();
			System.out.println("View Reg Name :"+viewregname);
			
			String viewregdate = driver.findElement(By.xpath("//*[@class='skr_registryDetailDate']")).getText();
			System.out.println("View Reg Date :"+viewregdate);
			
			String viewregnum = driver.findElement(By.xpath("//*[@class='skr_registryRegId']")).getText();
			System.out.println("View Reg Number :"+viewregnum);
			
			
			if(foundregname.equals(viewregname))
			{
				System.out.println("Find Registry(Reg Name) : "+foundregname +" is Matched in both side.");
			}
			else
			{
				System.out.println("Find Registry(Reg Name)  is Mismatch. In Founder Reg Name : "+foundregname +"  and In View Reg Name : " + viewregname );
			}
			
			
			if(foundregdate.equals(viewregdate))
			{
				System.out.println("Find Registry(Reg Date) : "+foundregdate +" is Matched in both side.");
			}
			else
			{
				System.out.println("Find Registry(Reg Date)  is Mismatch. In Founder Reg Date : "+foundregdate +"  and In View Reg Date : " + viewregdate );
			}
			
			if(foundregnumber.equals(viewregnum))
			{
				System.out.println("Find Registry(Reg Number) : "+foundregnumber +" is Matched in both side.");
			}
			else
			{
				System.out.println("Find Registry(Reg Number)  is Mismatch. In Founder Reg Number : "+foundregnumber +"  and In View Reg Number : " + viewregnum );
			}
			
		}
		catch(Throwable findregviewregistry)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
	}

}
