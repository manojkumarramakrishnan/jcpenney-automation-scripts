package guestpage;

import java.io.IOException;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregiconpresent extends srcmain{
	public static void FindRegistryPageIconPresent() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//FIND REGISTRY PAGE ICON PRESENT VALIDATION IS STARTED.//");

			logMessage = "ERROR:Find Registry page:Wedding Registry logo  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='logo_icon']")).isDisplayed();
			System.out.println("Find Registry page:Wedding Registry logo  is  displayed .");

			logMessage = "ERROR:Find Registry page:Bag Icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='bag_icon']")).isDisplayed();
			System.out.println("Find Registry page:Bag Icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:Home Icon  is not displayed .\n";
			driver.findElement(By.xpath("(//*[@pagename='home'])[1]")).isDisplayed();
			System.out.println("Find Registry page:Home Icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:View Registry Icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@pagename='viewRegistry']")).isDisplayed();
			System.out.println("Find Registry page:View Registry Icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:Browse icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@pagename='browsecat']")).isDisplayed();
			System.out.println("Find Registry page:Browse icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:Tips icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@pagename='tips']")).isDisplayed();
			System.out.println("Find Registry page:Tips icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:JCPenney icon  is not displayed .\n";
			driver.findElement(By.xpath("(//*[@pagename='home'])[2]")).isDisplayed();
			System.out.println("Find Registry page:JCPenney icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:SignIn  is not displayed .\n";
			driver.findElement(By.xpath("(//*[@class='skr_headerWelcomeTxt guest'])[1]")).isDisplayed();
			System.out.println("Find Registry page:SignIn  is  displayed .");

			logMessage = "ERROR:Find Registry page:Find Registry Text  is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_findRegistryLabel']")).isDisplayed();
			System.out.println("Find Registry page:Find Registry Text  is  displayed .");

			logMessage = "ERROR:Find Registry page:Required Fields Text is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_findRegRequiredField']")).isDisplayed();
			System.out.println("Find Registry page:Required Fields Text  is  displayed .");

			logMessage = "ERROR:Find Registry page:Event details  is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_eventDetails']")).isDisplayed();
			System.out.println("Find Registry page:Event details  is  displayed .");

			logMessage = "ERROR:Find Registry page:Input Registry Type  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skr_input_regType']")).isDisplayed();
			System.out.println("Find Registry page:Input Registry Type  is  displayed .");

			logMessage = "ERROR:Find Registry page:Registry Location  is not displayed .\n";
			driver.findElement(By.xpath("//*[@name='registryLocation']")).isDisplayed();
			System.out.println("Find Registry page:Registry Location  is  displayed .");

			logMessage = "ERROR:Find Registry page:Search Registry Lable  is not displayed .\n";
			driver.findElement(By.xpath("//*[@aria-label='search registry label']")).isDisplayed();
			System.out.println("Find Registry page:Search Registry Lable  is  displayed .");

			logMessage = "ERROR:Find Registry page:OR Text  is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_orCont']")).isDisplayed();
			System.out.println("Find Registry page:OR Text  is  displayed .");

			logMessage = "ERROR:Find Registry page:Registry Number  is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_registryNum']")).isDisplayed();
			System.out.println("Find Registry page:Registry Number  is  displayed .");

			logMessage = "ERROR:Find Registry page:Registry Cancel Button  is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_registryCancelButton']")).isDisplayed();
			System.out.println("Find Registry page:Registry Cancel Button  is  displayed .");

			logMessage = "ERROR:Find Registry page:Registry Search Button  is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_registrySearchButton']")).isDisplayed();
			System.out.println("Find Registry page:Registry Search Button  is  displayed .");

			logMessage = "ERROR:Find Registry page:FaceBook icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skr_shareFB_id']")).isDisplayed();
			System.out.println("Find Registry page:FaceBook icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:Twitter Icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skr_shareTwitter_id']")).isDisplayed();
			System.out.println("Find Registry page:Twitter Icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:Share Printer Icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skr_sharePinterset_id']")).isDisplayed();
			System.out.println("Find Registry page:Share Printer Icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:Share You Tube icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skr_shareYoutube_id']")).isDisplayed();
			System.out.println("Find Registry page:Share You Tube icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:ShareAtJcpenney icon  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='skr_shareAtJcpenney_id']")).isDisplayed();
			System.out.println("Find Registry page:ShareAtJcpenney icon  is  displayed .");

			logMessage = "ERROR:Find Registry page:Footer Font  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div/div[3]/span")).isDisplayed();
			System.out.println("Find Registry page:Footer Font  is  displayed .");

			logMessage = "ERROR:Find Registry page:Customer Service  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div/div[4]/div[1]/div[1]/a")).isDisplayed();
			System.out.println("Find Registry page:Customer Service  is  displayed .");

			logMessage = "ERROR:Find Registry page:Store Service  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div/div[4]/div[1]/div[2]/a")).isDisplayed();
			System.out.println("Find Registry page:Store Service  is  displayed .");



			logMessage = "ERROR:Find Registry page:More Way To Shop  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div/div[4]/div[2]/div[1]/a")).isDisplayed();
			System.out.println("Find Registry page:More Way To Shop  is  displayed .");

			logMessage = "ERROR:Find Registry page:About Us  is not displayed .\n";
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div/div[4]/div[2]/div[2]/a")).isDisplayed();
			System.out.println("Find Registry page:About Us  is  displayed .");


			logMessage = "ERROR:Find Registry page:Copywrite text  is not displayed .\n";
			driver.findElement(By.xpath("//*[@class='skr_text_1']")).isDisplayed();
			System.out.println("Find Registry page:Copywrite text  is  displayed .");

			System.out.println("//FIND REGISTRY PAGE ICON PRESENT VALIDATION IS COMPLETED.//");

		}
		catch(Throwable findregistrypageiconpresent)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}

}
