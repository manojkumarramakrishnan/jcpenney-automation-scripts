package guestpage;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class headermovevalidation extends srcmain {
	
	public static void HeaderMoveValidation()
	{
		try
		{
			System.out.println("//HEADER MOVER VALIDATION STARTED.//");
			try
			{

				logMessage = "ERROR :View As A Guest:ViewRegistry is not clicked.\n";
				driver.findElement(By.xpath("//*[@pagename='viewRegistry']")).click();
				System.out.println("View As A Guest:View Registry  is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("Signin"))
				{
					System.out.println("Successfully moved to Signin Page .");
				} 
				else 
				{
					System.out.println("Failed moved to Signin Page.");
				}
				Thread.sleep(3000);
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			
			
			
			
			
			try
			{

				logMessage = "ERROR :View As A Guest:Home  is not clicked.\n";
				driver.findElement(By.xpath("(//*[@pagename='home'])[1]")).click();
				System.out.println("View As A Guest:Home  is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("landing"))
				{
					System.out.println("Successfully moved to Landing Page .");
				} 
				else 
				{
					System.out.println("Failed moved to Landing Page.");
				}
				Thread.sleep(3000);
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			
			
			try
			{

				logMessage = "ERROR :View As A Guest:JCPenney Logo  is not clicked.\n";
				driver.findElement(By.xpath("(//*[@pagename='home'])[2]")).click();
				System.out.println("View As A Guest:JCPenney Logo  is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("jcpenney"))
				{
					System.out.println("Successfully moved to jcpenney Page .");
				} 
				else 
				{
					System.out.println("Failed moved to jcpenney Page.");
				}
				Thread.sleep(3000);
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			
			
			try
			{

				logMessage = "ERROR :View As A Guest:Signin  is not clicked.\n";
				driver.findElement(By.xpath("(//*[contains(@class,'skr_headerWelcomeTxt')])[1]")).click();
				System.out.println("View As A Guest:Signin   is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("Signin"))
				{
					System.out.println("Successfully moved to Signin Page .");
				} 
				else 
				{
					System.out.println("Failed moved to Signin Page.");
				}
				Thread.sleep(3000);
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			
			
			
			try
			{

				logMessage = "ERROR :View As A Guest:Wedding Registry Logo  is not clicked.\n";
				driver.findElement(By.xpath("(//*[contains(@class,'skr_headerWelcomeTxt')])[1]")).click();
				System.out.println("View As A Guest:Wedding Registry Logo   is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("landing"))
				{
					System.out.println("Successfully moved to Landing Page .");
				} 
				else 
				{
					System.out.println("Failed moved to Landing Page.");
				}
				Thread.sleep(3000);
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			
			
			try
			{

				logMessage = "ERROR :View As A Guest:SignIn  is not clicked.\n";
				driver.findElement(By.xpath("//*[@id='bag_icon']")).click();
				System.out.println("View As A Guest:SignIn   is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("ShoppingBag"))
				{
					System.out.println("Successfully moved to ShoppingBag Page .");
				} 
				else 
				{
					System.out.println("Failed moved to ShoppingBag Page.");
				}
				Thread.sleep(3000);
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			System.out.println("//HEADER MOVER VALIDATION COMPLETED.//");
		}
		catch(Throwable headermove)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
		
		
		
	}

}
