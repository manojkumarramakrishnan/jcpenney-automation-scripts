package guestpage;

import java.io.IOException;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregviewreglowtohigh extends srcmain {
	public static void FounderRegistrySortByLowToHigh() throws IOException {
		// TODO Auto-generated method stub
		/*String sortby ="//*[@aria-label='Sort By']";
		String lowtohigh="//*[@sortlabel='Price Low to High']";*/
		try
		{
			try{
				//logMessage = "ERROR:ShortBy Low To High  is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[contains(@aria-label,'Sort By')])"))).selectByIndex(8);
				
				logMessage = "SortBy DropDown Box Is Not Displayed.\n";
				driver.findElement(By.xpath("//*[@aria-label='Sort By']")).isDisplayed();
				System.out.println("SortBy DropDown Box Is  Displayed.");
				
				logMessage = "SortBy DropDown Box Is Not Clicked.\n";
				driver.findElement(By.xpath("//*[@aria-label='Sort By']")).click();
				logMessage = "SortBy DropDown Box Is  Clicked.\n";
				Thread.sleep(3000);
				
				//new Select(driver.findElement(By.xpath("(//*[contains(@class,'skr_sortByOptions')])"))).selectByVisibleText(" Price Low to High ");
				
				//*[@sortlabel='Price Low to High']
				logMessage = "SortBy DropDown Box:Price Low To High Is Not Clicked.\n";
				driver.findElement(By.xpath("//*[@sortlabel='Price Low to High']")).click();
				System.out.println("SortBy DropDown Box:Price Low To High Is  Clicked.");
				Thread.sleep(3000);
				
				logMessage = "SortBy DropDown Box:Apply Is Not Clicked.\n";
				driver.findElement(By.xpath("//*[contains(@class,'skr_sortDone')]")).click();
				System.out.println("SortBy DropDown Box:Apply Is  Clicked.");
				Thread.sleep(3000);
				
				
				
				System.out.println("ShortBy Low To High is  Selected .");

			}
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			//(//*[@itemprop='price'])[2]
			try
			{
				int sizeoflowtohighprice= driver.findElements(By.xpath("//*[contains(@class,'skr_registryItemsRegPrice')]")).size();
				System.out.println("Number of Low To High Price:"+sizeoflowtohighprice);

				//*[@id="skr_id_regItemsPrice_78811680018"]
				Float prev= 0.0f;
				Float next = 0.0f;


				for(int i=1;i<=sizeoflowtohighprice ; i++)
				{
					logMessage = "ERROR:Registry Item Low To High Price  is not displayed .\n";
					String registryitemlowtohigh = driver.findElement(By.xpath("(//*[contains(@class,'skr_registryItemsRegPrice')])["+i+"]")).getText();

					//$29.99 \\+ \\. \\\ -

					registryitemlowtohigh = registryitemlowtohigh.replaceAll(" Original", "").replaceAll("\\$", "");


					prev = Float.parseFloat(registryitemlowtohigh);

					if(i != 1)
					{

						if(prev.compareTo(next) < - 1)
						{
							System.out.println("Sorting order is wrong...");
							break;
						}
						else
						{
							//System.out.println("Sorting Order Reg Price Low To High are:");
							System.out.println("Product Order is correct.. Prev : " + prev +" Current Value : " + next+" for Product : " + i);
						}
					}
					next = prev;

				}
				
			
			}
			catch(Throwable founderlowtohigh)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
		}
		catch(Throwable foundreglowtohigh)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
		
	}

}
