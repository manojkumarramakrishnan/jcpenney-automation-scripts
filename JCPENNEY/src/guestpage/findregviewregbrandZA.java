package guestpage;

import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregviewregbrandZA extends srcmain {
	
	public static void FindRegViewRegSortByBrandZA()
	{
		try
		{
			
System.out.println("//SORT BY BRAND Z-A VALIDATION STARTED.//");
			
			logMessage = "ERROR: SortBy DropDown Box Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@aria-label='Sort By']")).click();
			System.out.println("SortBy DropDown Box Is  clicked.");
			Thread.sleep(3000);
			
			
			logMessage = "ERROR: SortBy DropDown Box:Brand Z-A Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@sortlabel='Brand Z - A']")).click();
			System.out.println("SortBy DropDown Box:Brand Z-A Is  clicked.");
			Thread.sleep(3000);
			
			
			logMessage = "ERROR: SortBy DropDown Box:Brand Z-A:Apply Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@class='skr_sortDone']")).click();
			System.out.println("SortBy DropDown Box:Brand Z-A:Apply Is  clicked.");
			Thread.sleep(3000);
			
			
			/*int sizeofcategoryatoz =driver.findElements(By.xpath("//*[contains(@class,'skr_registryTitleCont')]")).size();
			System.out.println("Number of Category A-Z:"+sizeofcategoryatoz);*/
			
			ArrayList<String> StoredAllBrandList = new ArrayList<String>(); 
			for( int i=0 ; ; i++)
			{
				try
				{
					String sortbybrandztoaname = driver.findElement(By.xpath("(//*[contains(@itemprop,'name')])["+i+"]")).getText();
					StoredAllBrandList.add(sortbybrandztoaname);				
					System.out.println("Sort By Category A-Z Name:"+sortbybrandztoaname);
				}
				catch(Throwable tr)
				{
					break;
				}
			}
			 ArrayList<String> UnsortedList = StoredAllBrandList;
			 
			 Collections.sort(StoredAllBrandList); 
			 boolean SortFlag = true;
			 for(int i = 0; i < StoredAllBrandList.size();i++)
			 {
				 if(!UnsortedList.get(i).equals(StoredAllBrandList.get(i)))
				 {
					 System.out.println("ERROR :Brand Sorting is Wrong By Z-A BY Index : " + i );
					 SortFlag = false;
					 break;
				 }
			 }
			if(SortFlag)
			{
				System.out.println("PASS :Brand Sorting Z-A is Correct.");
			}
			
			
			System.out.println("//SORT BY BRAND Z-A VALIDATION COMPLETED.//");
		}
		catch(Throwable brandZA)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
	}

}
