package guestpage;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregviewreghightolow extends srcmain{
	
	public static void FounderRegistrySortByHighToLow() {
		// TODO Auto-generated method stub
		try 
		{
			try{
				//logMessage = "ERROR:ShortBy Low To High  is not Selected .\n";
				//new Select(driver.findElement(By.xpath("(//*[contains(@aria-label,'Sort By')])"))).selectByIndex(8);
				
				logMessage = "SortBy DropDown Box Is Not Displayed.\n";
				driver.findElement(By.xpath("//*[@aria-label='Sort By']")).isDisplayed();
				System.out.println("SortBy DropDown Box Is  Displayed.");
				
				logMessage = "SortBy DropDown Box Is Not Clicked.\n";
				driver.findElement(By.xpath("//*[@aria-label='Sort By']")).click();
				logMessage = "SortBy DropDown Box Is  Clicked.\n";
				Thread.sleep(3000);
				
				//new Select(driver.findElement(By.xpath("(//*[contains(@class,'skr_sortByOptions')])"))).selectByVisibleText(" Price Low to High ");
				
				//*[@sortlabel='Price Low to High']
				logMessage = "SortBy DropDown Box:Price High To Low Is Not Clicked.\n";
				driver.findElement(By.xpath("//*[@sortlabel='Price High to Low']")).click();
				System.out.println("SortBy DropDown Box:Price High To Low Is  Clicked.");
				Thread.sleep(3000);
				
				logMessage = "SortBy DropDown Box:Apply Is Not Clicked.\n";
				driver.findElement(By.xpath("//*[contains(@class,'skr_sortDone')]")).click();
				System.out.println("SortBy DropDown Box:Apply Is  Clicked.");
				Thread.sleep(3000);
				
			}
			catch(Throwable st)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			//(//*[@itemprop='price'])[2]
			try
			{
				int sizeofhightolowprice= driver.findElements(By.xpath("//*[contains(@class,'skr_registryItemsRegPrice')]")).size();
				System.out.println("Number of High To Low Price:"+sizeofhightolowprice);

				//*[@id="skr_id_regItemsPrice_78811680018"]
				Float prev= 0.0f;
				Float next = 0.0f;


				for(int i=1;i<=sizeofhightolowprice ; i++)
				{
					logMessage = "ERROR:Registry Item High To Low Price  is not displayed .\n";
					String registryitemhightolow = driver.findElement(By.xpath("(//*[contains(@class,'skr_registryItemsRegPrice')])["+i+"]")).getText();

					//$29.99 \\+ \\. \\\ -

					registryitemhightolow = registryitemhightolow.replaceAll(" Original", "").replaceAll("\\$", "");


					prev = Float.parseFloat(registryitemhightolow);

					if(i != 1)
					{

						if(prev.compareTo(next) > - 1)
						{
							System.out.println("Sorting order is wrong...");
							break;
						}
						else
						{
							System.out.println("Product Order is correct.. Prev : " + prev +" Current Value : " + next+" for Product : " + i);
						}
					}
					next = prev;

				}
			
			}
			catch(Throwable founderhightolow)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			
		}
		catch(Throwable hightolow)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
	}

}
