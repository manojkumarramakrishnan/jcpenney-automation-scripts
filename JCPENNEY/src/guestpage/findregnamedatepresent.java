package guestpage;

import java.io.IOException;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregnamedatepresent extends srcmain{
	public static void FoundRegistryNameDatePresent() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			String foundregname = driver.findElement(By.xpath("//*[@class='skr_registriesNameWrap']")).getText();
			System.out.println("Found Reg Name :"+foundregname);
			
			String foundregdate = driver.findElement(By.xpath("//*[@class='skr_registriesDate']")).getText();
			System.out.println("Found Reg Date:"+foundregdate);
			
			String foundregnumber = driver.findElement(By.xpath("//*[@class='skr_registryNumber_tabView']")).getText();
			System.out.println("Found Reg Number:"+foundregnumber);
			
			logMessage = "ERROR:Found Registry:View Button is not clicked.\n";
			driver.findElement(By.xpath("//*[@class='skr_viewButton']")).click();
			System.out.println("Found Registry:View Button is  clicked.");
			Thread.sleep(5000);
			
		}
		catch(Throwable foundregistrynamedatepresent)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
	}

}
