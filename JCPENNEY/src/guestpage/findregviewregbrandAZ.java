package guestpage;

import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregviewregbrandAZ extends srcmain{
	
	public static void FindRegViewRegSortByBrandAZ()
	{
		try
		{
System.out.println("//SORT BY BRAND A-Z VALIDATION STARTED.//");
			
			logMessage = "ERROR:SortBy DropDown Box Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@aria-label='Sort By']")).click();
			System.out.println("SortBy DropDown Box Is  clicked.");
			Thread.sleep(3000);
			
			
			logMessage = "ERROR: SortBy DropDown Box:Brand A-Z Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@sortlabel='Brand A - Z']")).click();
			System.out.println("SortBy DropDown Box:Brand A-Z Is  clicked.");
			Thread.sleep(3000);
			
			
			logMessage = "ERROR: SortBy DropDown Box:Brand A-Z:Apply Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@class='skr_sortDone']")).click();
			System.out.println("SortBy DropDown Box:Brand A-Z:Apply Is  clicked.");
			Thread.sleep(3000);
			
			
			/*int sizeofbrandatoz =driver.findElements(By.xpath("//*[contains(@itemprop,'name')]")).size();
			System.out.println("Number of Category A-Z:"+sizeofbrandatoz);*/
			
			ArrayList<String> StoredAllBrandList = new ArrayList<String>(); 
			for( int i=0 ;; i++)
			{
				try{
					String sortbybrandatozname = driver.findElement(By.xpath("(//*[contains(@itemprop,'name')])["+i+"]")).getText();
					StoredAllBrandList.add(sortbybrandatozname);				
					System.out.println("Sort By Category A-Z Name:"+sortbybrandatozname);
				}
				catch(Throwable tr)
				{
					break;
				}
			}
			 ArrayList<String> UnsortedList = StoredAllBrandList;
			 
			 Collections.sort(StoredAllBrandList); 
			 boolean SortFlag = true;
			 for(int i = 0; i < StoredAllBrandList.size();i++)
			 {
				 if(!UnsortedList.get(i).equals(StoredAllBrandList.get(i)))
				 {
					 System.out.println("ERROR :Brand Sorting is Wrong By A-Z BY Index : " + i );
					 SortFlag = false;
					 break;
				 }
			 }
			if(SortFlag)
			{
				System.out.println("PASS :Brand Sorting A-Z is Correct.");
			}
			
			
			System.out.println("//SORT BY BRAND A-Z VALIDATION COMPLETED.//");
		}
		catch(Throwable brandAZ)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
	}

}
