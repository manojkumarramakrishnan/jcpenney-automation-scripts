package guestpage;

import java.util.ArrayList;
import java.util.Collections;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregviewregcategoryZA extends srcmain {
	
	public static void FindRegViewRegSortByCategoryZA()
	{
		try
		{
        System.out.println("//SORT BY CATEGORY Z-A VALIDATION STARTED.//");
			
			logMessage = "ERROR: SortBy DropDown Box Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@aria-label='Sort By']")).click();
			System.out.println("SortBy DropDown Box Is  clicked.");
			Thread.sleep(3000);
			
			
			logMessage = "ERROR: SortBy DropDown Box:Category Z-A Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@sortlabel='Category Z - A']")).click();
			System.out.println("SortBy DropDown Box:Category Z-A Is  clicked.");
			Thread.sleep(3000);
			
			
			logMessage = "ERROR: SortBy DropDown Box:Category Z-A:Apply Is Not clicked.\n";
			driver.findElement(By.xpath("//*[@class='skr_sortDone']")).click();
			System.out.println("SortBy DropDown Box:Category Z-A:Apply Is  clicked.");
			Thread.sleep(3000);
			
			
			/*int sizeofcategoryatoz =driver.findElements(By.xpath("//*[contains(@class,'skr_registryTitleCont')]")).size();
			System.out.println("Number of Category A-Z:"+sizeofcategoryatoz);*/
			
			ArrayList<String> StoredAllCategoryList = new ArrayList<String>(); 
			for( int i=0 ; ; i++)
			{
				try
				{
					String sortbycategoryztoaname = driver.findElement(By.xpath("//*[@id='id_skr_registryItemsDeptCont_"+i+"']/div/div[1]/div[1]/div")).getText();
					StoredAllCategoryList.add(sortbycategoryztoaname);				
					System.out.println("Sort By Category Z-A Name:"+sortbycategoryztoaname);
				}
				catch(Throwable tr)
				{
					break;
				}
			}
			 ArrayList<String> UnsortedList = StoredAllCategoryList;
			 
			 Collections.sort(StoredAllCategoryList); 
			 boolean SortFlag = true;
			 for(int i = 0; i < StoredAllCategoryList.size();i++)
			 {
				 if(!UnsortedList.get(i).equals(StoredAllCategoryList.get(i)))
				 {
					 System.out.println("ERROR :Category Sorting is Wrong By Z-A BY Index : " + i );
					 SortFlag = false;
					 break;
				 }
			 }
			if(SortFlag)
			{
				System.out.println("PASS :Category Sorting Z-A is Correct.");
			}
			
			
			System.out.println("//SORT BY CATEGORY Z-A VALIDATION COMPLETED.//");
		}
		catch(Throwable categoryZA)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
	}

}
