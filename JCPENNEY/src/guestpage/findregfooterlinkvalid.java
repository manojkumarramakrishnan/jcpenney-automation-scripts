package guestpage;

import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregfooterlinkvalid extends srcmain{
	
	public static void FindRegistryPageFooterLinkValidation() {
		// TODO Auto-generated method stub
		try
		{

			try
			{
				System.out.println("FooterLinkValidation is started to validate");

				NewWindowValidation("shareFB", "//*[@id='skr_shareFB_id']","www.facebook.com/jcp", "facebook");
				System.out.println("successfully moved to FaceBook");
				Thread.sleep(5000);
				NewWindowValidation("shareTwitter","//*[@id='skr_shareTwitter_id']","twitter.com/jcpenney", "twitter.com");
				System.out.println("successfully moved to shareTwitter");
				Thread.sleep(5000);
				NewWindowValidation("sharePinterset","//*[@id='skr_sharePinterset_id']","www.pinterest.com/jcpenney/", "pinterest.com");
				System.out.println("successfully moved to sharePinterset");
				Thread.sleep(5000);
				NewWindowValidation("shareYoutube","//*[@id='skr_shareYoutube_id']","www.youtube.com/user/jcpenney", "youtube.com");
				System.out.println("successfully moved to shareYoutube");
				Thread.sleep(5000);
				NewWindowValidation("shareAtJcpenney","//*[@id='skr_shareAtJcpenney_id']","www.jcpenney.com/index.jsp", "jcpenney.com");
				System.out.println("successfully moved to shareAtJcpenney");
				Thread.sleep(5000);
				System.out.println("FooterLinkValidation is completed.");
			}
			catch(Throwable footerlink)
			{
				System.out.println(logMessage);
				logInfo(logMessage);


			}
		}
		catch(Throwable findregistryfooterlinkvalidation)
		{

		}

	}
	
	
	
	public static void NewWindowValidation(String logValue, String xpath,String value, String input) throws IOException 
	// TODO Auto-generated method stub
	{
		try
		{
			logValue = "ERROR :" + logValue + " is not displayed.\n";
			driver.findElement(By.xpath(xpath)).isDisplayed();

			driver.findElement(By.xpath(xpath)).click();
			Thread.sleep(5000);

			String currentWindowName = driver.getWindowHandle();
			// currentWindowName = "firstWindow"

			Set<String> allWindowName = driver.getWindowHandles();
			// allWindowName = {"firstWindow","SecondWindow"};
			/*
			 * windowName =firstWindow windowName =SecondWindow
			 */

			for (String windowName : allWindowName)
			{
				if (!windowName.equals(currentWindowName))
				{
					driver.switchTo().window(windowName);
					String fbURL = driver.getCurrentUrl();
					if (fbURL.contains(value))
					{
						String shareiconurl = driver.getCurrentUrl();
						if (shareiconurl.contains("input"))

							System.out.println("succesfully  Share icon moved to new tap");

					}
					else 
					{
						System.out.println("Failed  Share icon moved to new tap");
					}
					driver.close();
					break;

				}
			}
			driver.switchTo().window(currentWindowName);
		}
		catch(Throwable newlinkvalidation)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}

}
