package guestpage;

import java.io.IOException;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findregtextboxvalid extends srcmain{
	
	public static void FindRegistryEventDetailTextBoxValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			logMessage = "ERROR:Find A Registry:Search Button is not clicked .\n";
			driver.findElement(By.xpath("//*[@class='skr_registrySearchButton']")).click();
			System.out.println("Find A Registry:Search Button is  clicked .");
			Thread.sleep(4000);


			if(driver.findElements(By.xpath("//*[@class='jcp_skr_inputErrCon']")).size() !=0)
			{
				System.out.println("Error Msg is dispalyed.");
			}
			else
			{
				System.out.println("Error Msg is not dispalyed.");
			}


		}
		catch(Throwable findregistrytextbox)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

		try {
			System.out.println("//EVENT NAME TEXT BOX VALIDATION STARTED.//");
			String[] eventname = { "wedding1", "  ", "@#$", "wedding" };
			for (int i = 0; i < eventname.length; i++)
			{
				String input = eventname[i];
				System.out.println("Event Name : " + input);
				try {
					logMessage = "ERROR : ERROR icon is not displayed. EventName : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_regType']")).clear();
					driver.findElement(By.xpath("//*[@id='skr_input_regType']")).sendKeys(input);
					logMessage = "ERROR :Search Button is not clicked.\n";
					driver.findElement(By.xpath("//*[@class='skr_registrySearchButton']")).click();
					System.out.println("Search Button is  clicked.");
					Thread.sleep(4000);
					logMessage = "ERROR :Error text is not displayed.\n";
					driver.findElement(By.xpath("//*[@class='jcp_skr_inputErrCon']")).isDisplayed();
					System.out.println("Error text is  displayed.");
				} catch (Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);
				}
			}
			System.out.println("//EVENT NAME TEXT BOX VALIDATION COMPLETED.//");

		} catch (Throwable fnv) 
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
		
		
		try {
			System.out.println("//LOCATION TEXT BOX VALIDATION STARTED.//");
			String[] location = { "12548","coimbatore1 " ," @coimbatore"," ", "@#$", "coimbatore" };
			for (int i = 0; i < location.length; i++)
			{
				String input = location[i];
				System.out.println("Location : " + input);
				try {
					logMessage = "ERROR : ERROR icon is not displayed. Location : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_location']")).clear();
					driver.findElement(By.xpath("//*[@id='skr_input_location']")).sendKeys(input);
					logMessage = "ERROR :Search Button is not clicked.\n";
					driver.findElement(By.xpath("//*[@class='skr_registrySearchButton']")).click();
					System.out.println("Search Button is  clicked.");
					Thread.sleep(4000);
					logMessage = "ERROR :Error text is not displayed.\n";
					driver.findElement(By.xpath("//*[@class='jcp_skr_inputErrCon']")).isDisplayed();
					System.out.println("Error text is  displayed.");
				} catch (Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);
				}
			}
			System.out.println("//LOCATION TEXT BOX VALIDATION COMPLETED.//");

		} catch (Throwable fnv) 
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}
		
		
		
		try {
			System.out.println("//REGISTRY NAME TEXT BOX VALIDATION STARTED.//");
			String[] location = { "12548","jcp " ," @jcp"," ", "@#$", "tuesdayjcptest" };
			for (int i = 0; i < location.length; i++)
			{
				String input = location[i];
				System.out.println("Location : " + input);
				try {
					logMessage = "ERROR : ERROR icon is not displayed. Location : "+ input + ".\n";
					driver.findElement(By.xpath("//*[@id='skr_input_searchRegistry']")).clear();
					driver.findElement(By.xpath("//*[@id='skr_input_searchRegistry']")).sendKeys(input);
					logMessage = "ERROR :Search Button is not clicked.\n";
					if(driver.findElements(By.xpath("//*[@class='jcp_skr_inputErrCon']")).size() !=0)
					{
						System.out.println("Error text is  displayed.");
					}
					else
					{
						System.out.println("Error text is not displayed.");
					}
					driver.findElement(By.xpath("//*[@class='skr_registrySearchButton']")).click();
					System.out.println("Search Button is  clicked.");
					Thread.sleep(4000);
					
					if(driver.findElements(By.xpath("//*[@id='skPageLayoutCell_28_id-1']/div/div/div[2]")).size() !=0)
					{
						System.out.println("Searched registry is either public static registry or does not exist.");
					}
					
					
				} catch (Throwable tr)
				{
					System.out.println(logMessage);
					logInfo(logMessage);
				}
			}
			System.out.println("//REGISTRY NAME TEXT BOX VALIDATION COMPLETED.//");

		} catch (Throwable fnv) 
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}

}
