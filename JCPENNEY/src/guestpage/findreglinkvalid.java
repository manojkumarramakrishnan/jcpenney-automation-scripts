package guestpage;

import java.io.IOException;

import org.openqa.selenium.By;

import JCPpack.srcmain;

public class findreglinkvalid extends srcmain{
	
	public static void FindRegistryPageLinkValidation() throws IOException {
		// TODO Auto-generated method stub
		try
		{
			System.out.println("//FIND REGISTRYPAGE LINK VALIDATION IS STARTED.//");
			try
			{

				logMessage = "ERROR :Find Registry Page:Wedding Registry Page is not clicked.\n";
				driver.findElement(By.xpath("//*[@class='skr_logo_icon_container']")).click();
				System.out.println("Wedding Registry Page is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("JCP_Registry"))
				{
					System.out.println("Successfully moved to Jcp Page .");
				} 
				else 
				{
					System.out.println("Failed moved to Jcp Page.");
				}
				Thread.sleep(3000);

				/*logMessage = "ERROR :Home Icon is not clicked.\n";
				driver.findElement(By.xpath("//*[@name='homepage']")).click();
				System.out.println("Home Icon is  clicked.");*/
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}


			try
			{

				logMessage = "ERROR :Find Registry Page:Home Page Icon is not clicked.\n";
				driver.findElement(By.xpath("(//*[@pagename='home'])[1]")).click();
				System.out.println("Home Page Icon is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("home"))
				{
					System.out.println("Successfully moved to home Page .");
				} 
				else 
				{
					System.out.println("Failed moved to home Page.");
				}
				Thread.sleep(3000);

				/*logMessage = "ERROR :Home Icon is not clicked.\n";
				driver.findElement(By.xpath("//*[@name='homepage']")).click();
				System.out.println("Home Icon is  clicked.");*/
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}


			try
			{

				logMessage = "ERROR :Find Registry Page:View Registry Page icon is not clicked.\n";
				driver.findElement(By.xpath("//*[@pagename='viewRegistry']")).click();
				System.out.println("View Registry Page icon is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("manageGiftRegistrySignin"))
				{
					System.out.println("Successfully moved to View Registry Page .");
				} 
				else 
				{
					System.out.println("Failed moved to View Registry Page.");
				}
				Thread.sleep(3000);

				/*logMessage = "ERROR :Home Icon is not clicked.\n";
				driver.findElement(By.xpath("//*[@name='homepage']")).click();
				System.out.println("Home Icon is  clicked.");*/
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}

			try
			{

				logMessage = "ERROR :Find Registry Page:Browse Item Page icon is not clicked.\n";
				driver.findElement(By.xpath("//*[@pagename='browsecat']")).click();
				System.out.println("Browse Item Page icon is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("browsecat"))
				{
					System.out.println("Successfully moved to Browse Item Page .");
				} 
				else 
				{
					System.out.println("Failed moved to Browse Item Page.");
				}
				Thread.sleep(3000);

				/*logMessage = "ERROR :Home Icon is not clicked.\n";
				driver.findElement(By.xpath("//*[@name='homepage']")).click();
				System.out.println("Home Icon is  clicked.");*/
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}



			try
			{

				logMessage = "ERROR :Find Registry Page:Tips Page icon is not clicked.\n";
				driver.findElement(By.xpath("//*[@pagename='tips']")).click();
				System.out.println("Tips Page icon is  clicked.");
				Thread.sleep(8000);
				String url2 = driver.getCurrentUrl();
				if (url2.contains("tips"))
				{
					System.out.println("Successfully moved to Tips Page .");
				} 
				else 
				{
					System.out.println("Failed moved to Tips Page.");
				}
				Thread.sleep(3000);

				/*logMessage = "ERROR :Home Icon is not clicked.\n";
				driver.findElement(By.xpath("//*[@name='homepage']")).click();
				System.out.println("Home Icon is  clicked.");*/
				driver.navigate().back();
				Thread.sleep(8000);
			}
			catch(Throwable browse)
			{
				System.out.println(logMessage);
				logInfo(logMessage);
			}
			System.out.println("//FIND REGISTRYPAGE LINK VALIDATION IS COMPLETED.//");
		}
		catch(Throwable findregistrylinkvalidation)
		{
			System.out.println(logMessage);
			logInfo(logMessage);
		}

	}

}
